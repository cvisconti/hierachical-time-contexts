#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Collection of functions that produce the figures that are used in the paper

"""

import logging

import numpy as np
import scipy.stats as stats
import pandas as pd
import seaborn as sns
from pymc3.parallel_sampling import ParallelSamplingError
from matplotlib import pyplot as plt
import statsmodels.formula.api as smf
import pickle
import itertools as it

import import_data
import analysis as anal
import plotting as poo
import agents_play_game as ass
import inference as infy

def figure_1(data=None, trans_types=((0,1,2,3),(4,5,6,7)), group_labels=None,
                        axis=None, add_condition=False,
                        condition=0, kwargs_barplot=None, fignum=13):
    
    ''' The function formerly known as diff_av_risk_trans
  
    Plots the difference in frequency of choices between the
    transition types in trans_type.

    Parameters
    ----------
    data : DataFrame
    Behavioral data. Output of import_data.import_data()

    trans_types : array-like
    Transitions to use. If an array of two ints, the first element is
    compared against the second one. If any of the elements of the array
    is an array itself, all its elements are groupped together. For example,
    [[1, 2], [3, 4]] will plot two bars, one for all the trials that are
    trans_type either 1 or 2, and one for 3 and 4.

    add_condition : bool
    If True, the condition 0.75 is also considered, by setting the trials
    of that condition to a transition type 8 or 9, depending on whether
    the miniblock was risky or safe (regardless of the next one or of its
    color). These new transition types can then be plotted alongside
    the existing 0-7 ones. '''
    
    if data is None:
        data = import_data.import_all(data_sets=['behavioural'],no_timeouts=False)
    
    if kwargs_barplot is None:
        kwargs_barplot = {}
    if axis is None:
        fig, axis = plt.subplots(1, 1, num=fignum, clear=True,
                                 squeeze=True)

    if add_condition:
        cond_cond = data['condition'] == 0.75
        cond_safe = np.zeros(len(cond_cond), dtype=bool)
        cond_risk = np.zeros(len(cond_cond), dtype=bool)
        for ix_trial, mblock_type in enumerate(data['mblock_type']):
            cond_safe[ix_trial] = mblock_type.startswith('safe')
            cond_risk[ix_trial] = mblock_type.startswith('risky')
        data.loc[np.logical_and(cond_cond, cond_safe), 'trans_type'] = 9
        data.loc[np.logical_and(cond_cond, cond_risk), 'trans_type'] = 8
    else:
        data = data.query('condition == @condition')
    new_trans = []
    for ix_trans, one_group in enumerate(trans_types):
        try:
            one_new = 10 + ix_trans
            for one_trans in one_group:
                data.loc[data['trans_type'] == one_trans,
                         'trans_type'] = one_new
            new_trans.append(one_new)
        except TypeError:
            new_trans.append(one_group)
    data = data.query('trans_type in @new_trans')
    sns.barplot(data=data, x='trans_type', y='choice',
                ax=axis, **kwargs_barplot)
    if group_labels:
        axis.set_xticklabels(group_labels)
    axis.set_title('Behavior across transitions')
    axis.set_ylabel('Frequency of safe choice')
    axis.set_xlabel('Transition type')
    
def figure_2():
    ''' Figure formerly known as plot_choices_by_risk
    
    Makes a plot for all participants averaged and each conditions 
    (future of no future) of average choice for miniblocks that start with 
    risky, or those that start with safe'''
    data = import_data.import_all(data_sets=['behavioural'])
    pal = sns.cubehelix_palette(10, rot=-.25, light=.7)
    
    data_2 = data.groupby(by=['participant','condition','trans_type']).mean()
    
    grouped_data = data_2.reset_index()
    
    risky_trans_cond1  = grouped_data.query('trans_type in [0,1,4,5] and condition==0').mean()['choice']
    safe_trans_cond1 = grouped_data.query('trans_type in [2,3,6,7] and condition==0').mean()['choice']
    
    risky_trans_cond2  = grouped_data.query('trans_type in [0,1,4,5] and condition==0.75').mean()['choice']
    safe_trans_cond2 = grouped_data.query('trans_type in [2,3,6,7] and condition==0.75').mean()['choice']
    
    colours = ['cyan', 'lightblue', 'lightgreen', 'cadetblue','tan']
    
    ticks = ['risky, cond1','safe, cond1', 'risky cond2', 'safe cond2']
    plot = plt.bar([1,2,3,4],[safe_trans_cond1,risky_trans_cond1,safe_trans_cond2,risky_trans_cond2], tick_label=ticks, color=colours)
    plt.ylabel('Choices by Risk')
    
    return plot
    
def calc_subplots(num_plots):
    """ Calculates a good arrangement for the subplots given the number of
    subjects.
    """
    if num_plots == 2 or num_plots == 3:
        return num_plots, 1

    sqrtns = np.sqrt(num_plots)
    if abs(sqrtns - np.ceil(sqrtns)) < 0.001:
        a1 = a2 = np.ceil(sqrtns)
    else:
        divs = num_plots % np.arange(2, num_plots)
        divs = np.arange(2, num_plots)[divs == 0]
        if divs.size == 0:
            return calc_subplots(num_plots + 1)
        else:
            a1 = divs[np.ceil(len(divs) / 2).astype(int)]
            a2 = num_plots / a1
    return np.array((int(a1), int(a2)))
