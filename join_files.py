#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Sep 27 10:35:11 2019

@author: visconti
"""

import os
import pickle
import numpy as np


import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt

from typing import Any


def join_files(agent_name):
    first = {}  # Where to store this
    second = {}
    third = {}
    fourth = {}
    fifth = {}
    sixth = {}

    base_directory = './parallel_results/'

    part_names = np.array(['_first', '_second', '_third', '_fourth', '_fifth', '_sixth'])

    for ix, onepart in enumerate(part_names):
        part_names[ix] = agent_name + onepart

    dick_names = [first, second, third, fourth, fifth, sixth]

    for index, part_name in enumerate(part_names):
        count = 0
        for file_name in os.listdir(base_directory):
            if file_name.startswith(part_name):
                open_this = base_directory + file_name
                with open(open_this, 'rb') as part:
                    curr_dick = pickle.load(part)
                if count == 0:
                    dick_names[index].update(curr_dick)

                    count = 1
                else:
                    dick_names[index]['decisions'] = np.vstack((dick_names[index]['decisions'], curr_dick['decisions']))
                    dick_names[index]['unmatched'] = np.vstack((dick_names[index]['unmatched'], curr_dick['unmatched']))
                    dick_names[index]['final_m_pts'] = np.concatenate(
                        [np.array(dick_names[index]['final_m_pts'], ndmin=1),
                         np.array(curr_dick['final_m_pts'], ndmin=1)])

    return first, second, third, fourth, fifth, sixth


def calculate_points_mean(dicks):
    ''' Reads into the dictionaries and calculates the mean'''

    num_dicks = len(dicks)
    final_mean = np.zeros(num_dicks)
    final_std  = np.zeros(num_dicks)

    for dick_number in range(6):
        final_mean[dick_number] = dicks[dick_number]['final_m_pts'].mean()
        final_std[dick_number] = dicks[dick_number]['final_m_pts'].std()



    return final_mean, final_std

def make_points_pretty():
    rs = np.random.RandomState(1979)
    x = rs.randn(500)
    g = np.tile(list("ABCDEFGHIJ"), 50)
    df = pd.DataFrame(dict(x=x, g=g))
    m = df.g.map(ord)
    df["x"] += m

    # Initialize the FacetGrid object
    pal = sns.cubehelix_palette(10, rot=-.25, light=.7)
    g = sns.FacetGrid(df, row="g", hue="g", aspect=15, height=.5, palette=pal)

    # Draw the densities in a few steps
    g.map(sns.kdeplot, "x", clip_on=False, shade=True, alpha=1, lw=1.5, bw=.2)
    g.map(sns.kdeplot, "x", clip_on=False, color="w", lw=2, bw=.2)
    g.map(plt.axhline, y=0, lw=2, clip_on=False)


  # Define and use a simple function to label the plot in axes coordinates
def label(x, color, label):
    ax = plt.gca()
    ax.text(0, .2, label, fontweight="bold", color=color,
            ha="left", va="center", transform=ax.transAxes)


    g.map(label, "x")

    # Set the subplots to overlap
    g.fig.subplots_adjust(hspace=-.25)

    # Remove axes details that don't play well with overlap
    g.set_titles("")
    g.set(yticks=[])
    g.despine(bottom=True, left=True)
