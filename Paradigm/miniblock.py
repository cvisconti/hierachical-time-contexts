""" This is the function that runs one miniblock of the Point Monster Task """
import time

import numpy as np
from psychopy import core, visual, event
# import time

imagefolder = '../Images/'

# Uncomment to eliminate pauses.
# def my_wait(x):
#     pass
# core.wait = my_wait


def pars(key=None):
    '''This makes a dictionary of the parameters because it is good
    practice.'''

    dicktionary = {'maximum_bar_size': 1.0,
                   'bar_scaling': 40,
                   'risky_offers': [9, ]}
    if key is None:
        return dicktionary
    return dicktionary[key]


def define_mblock_types():
    """ Seperately shows the list of miniblock types """

    all_mblock_types = ['risky_red', 'risky_blue', 'safe_red',
                        'safe_blue', 'trick']

    return all_mblock_types


def one(curr_mblock, next_mblock, points, stars, window=None,
        num_trials=6, fmri=False, fast=False):
    """Runs one miniblock
    Inputs
    ------
    points : list
    the types of point that the sub has (red,blue)

    curr_mblock : str
    what type of miniblock the subject currently is in
    'risky_red''safe_red''risky_blue''safe_blue'

    next_mblock : str
    what the next miniblock will be
    used for drawing a nice little indication of this"""

    # visual.Rect(window, lineColor='black').draw()
    # visual.TextStim(window, 'yo').draw()
    # window.flip()
    # core.wait(3)

    window_flag = False
    if fast:
        waits = {'pre_mblock': 0.1,
                 'pre_monster': 0.1,
                 'post_monster': 0.1,
                 'monster': 0.1,
                 'star_feedback': 0.1,
                 'point_feedback': 0.1,
                 'inter_trial': 0.1,
                 'decision_timeout': 4}
    elif fmri:
        waits = {'pre_mblock': 1.4,
                 'pre_monster': 0.3,
                 'post_monster': 0.5,
                 'monster': 1.35,
                 'star_feedback': 0.2,
                 'point_feedback': 0.35555,
                 'inter_trial': 4,
                 'decision_timeout': 4}
    else:
        waits = {'pre_mblock': 1.4,
                 'pre_monster': 0.3,
                 'post_monster': 0.5,
                 'monster': 1.35,
                 'star_feedback': 0.2,
                 'point_feedback': 0.35555,
                 'inter_trial': 1,
                 'decision_timeout': 4}

    if window is None:
        window = visual.Window()
        window_flag = True

    screen_offer, screen_feedback, screen_mblock_switch = \
        create_stimuli(window, points)
    screen_offer.update_curr_mblock(curr_mblock)
    screen_offer.update_next_mblock(next_mblock)
    screen_feedback.update_curr_mblock(curr_mblock)
    screen_feedback.update_next_mblock(next_mblock)
    screen_mblock_switch.update_curr_mblock(curr_mblock)
    screen_mblock_switch.update_next_mblock(next_mblock)

    # Set up containers for logging all relevant stuffs

    log_points_red = -np.ones((1, num_trials))
    log_points_blue = -np.ones((1, num_trials))
    log_choice = -np.ones((1, num_trials))
    log_monster = -np.ones((1, num_trials))
    log_monster_belly = -np.ones((1, num_trials, 2))
    log_num_trial = -np.ones((1, num_trials))
    log_offered_points_red = -np.ones((1, num_trials))
    log_offered_points_blue = -np.ones((1, num_trials))
    log_stars = -np.ones((1, num_trials))
    log_rts = -np.ones((1, num_trials))
    log_decision_onset = np.array(['HHMMSS'] * num_trials)[None, :]
    log_decision_onset_epoch = np.zeros((1, num_trials))

    results = dict(points_red=log_points_red,
                   points_blue=log_points_blue,
                   choice=log_choice,
                   monster_appearance=log_monster,
                   monster_belly=log_monster_belly,
                   trial_number=log_num_trial,
                   offered_points_red=log_offered_points_red,
                   offered_points_blue=log_offered_points_blue,
                   miniblock_type=np.array(curr_mblock, ndmin=2),
                   stars=log_stars, rts=log_rts,
                   decision_onset=log_decision_onset,
                   decision_onset_epoch=log_decision_onset_epoch)

    screen_mblock_switch.draw()
    window.flip()
    core.wait(waits['pre_mblock'])

    screen_feedback.display_stars(stars)
    for trial in range(num_trials):
        # print('Points before choice: ', points)
        log_num_trial[0, trial] = trial  # log
        offered_points = generate_risky_offer(curr_mblock)
        if offered_points[0] == 0:
            colour = 'blue'
            amt_offered = offered_points[1]
            log_offered_points_blue[0, trial] = amt_offered  # log
            log_offered_points_red[0, trial] = 0
        else:
            colour = 'red'
            amt_offered = offered_points[0]
            log_offered_points_red[0, trial] = amt_offered  # log
            log_offered_points_blue[0, trial] = 0
        log_points_red[0, trial] = points[0]  # log
        log_points_blue[0, trial] = points[1]  # log
        log_stars[0, trial] = stars  # log

        screen_offer.update_risky_offer(colour, amt_offered)
        screen_offer.draw()
        window.flip()
        log_decision_onset[0, trial] = when_is_it()
        log_decision_onset_epoch[0, trial] = time.time()
        t_ini = time.time()
        choice, reaction_time = process_keypress(t_ini,
                                                 fmri=fmri,
                                                 window=window,
                                                 waits=waits)
        log_choice[0, trial] = choice  # log
        log_rts[0, trial] = reaction_time

        do_monster = sample_monster(choice, curr_mblock)
        screen_feedback.invisible_monster(0)
        points, tmp_points, eaten_points, earned_points =\
            sample_next_points(choice, points, offered_points, do_monster)
#        print('Trial: ', trial, 'Points after choice: ', points,
#              'Earned_points', earned_points, ' \n Monstie:',
#              do_monster, '\n\n', 'tmp_points', tmp_points)
        log_monster_belly[0, trial, :] = eaten_points  # log
        log_monster[0, trial] = bool(eaten_points)  # log
        does_eat = choice == -1 or (choice == 0 and do_monster)
        screen_feedback.update_points(window, tmp_points, waits,
                                      does_eat)

        if min(points)/pars('bar_scaling') >= pars('maximum_bar_size'):
            stars = stars+1
            points = [points[0]-min(points), points[1]-min(points)]
            print('\nNew Puppy Points', points)
            screen_feedback.display_stars(stars)
            screen_feedback.update_points(window, points, waits)
            screen_feedback.draw()
            window.flip()
            core.wait(waits['star_feedback'])
        core.wait(waits['inter_trial'])
    if window_flag is True:
        window.close()

    return results, points, stars, screen_mblock_switch


def test_see_monster():
    window = visual.Window()
    screen_monster = create_stimuli(window)[2]
    screen_monster.draw()
    window.flip()
    event.waitKeys()

    eaten_points = monster_eats_points(1, [9, 6])
    screen_monster.update_monster_screen(window, eaten_points)
    screen_monster.draw()
    window.flip()
    event.waitKeys()
    window.close()


def when_is_it():
    '''Sets some stuff up so I can later figure out when things happened'''
    
    t = time.localtime()
    it_is_now = time.strftime("%H%M%S", t)
    
    return it_is_now


def generate_risky_offer(curr_mblock):
    '''Outputs a tuple (red,blue) of risky points.'''

    points_on_offer = pars('risky_offers')
    p_distribution = 1 / len(points_on_offer) * np.ones(len(points_on_offer))

    points_generated = np.random.choice(points_on_offer, p=p_distribution)

    if curr_mblock[-3:] == 'red':
        offered_points = (points_generated, 0)
    elif curr_mblock[-4:] == 'blue':
        offered_points = (0, points_generated)
    else:
        raise ValueError('I can''t tell if it is a red or blue miniblock :(')

    return offered_points


# def show_offers(window,picture_mblock,picture_money,picture_save,
#     picture_unsaved,picture_saved,picture_vault,label_money,label_save):
#    """Displays the offered points / save points option to the participant"""
#
#    label_money.draw()
#    label_save.draw()
#    picture_mblock.draw()
#
#    picture_vault.draw()
#    picture_money.draw()
#    picture_save.draw()
#
#    picture_unsaved.draw()
#
#    picture_saved.draw()
#
#    window.flip()
#

def process_keypress(t_ini, waits, window, fmri=False):
    """Transforms participants keypress given whatever PsychoPy gives as
    keyboard into the choice

    0 = Get Risky Points    1 = Get Safe Points

    """
    if fmri is False:
        key_press = event.waitKeys(keyList=('c', 'm'),
                                   maxWait=waits['decision_timeout'])
        reaction_time = time.time() - t_ini
        if key_press is None:
            timeout_taser(window)
            choice = -1
        else:
            # key_press = np.random.choice(['c', 'm'], p=(0.1, 0.9))
            if key_press[0] == 'c':
                choice = 0
            elif key_press[0] == 'm':
                choice = 1
            
    elif fmri is True:
        '''Keypresses for fmri setup. Triggers on O-Rings are as follows :
            Right Thumb : d
            Right Index : c
            Left Index  : b
            Left Thumb  : a '''

        key_press = event.waitKeys(keyList=('b', 'c'),
                                   maxWait=waits['decision_timeout'])
        reaction_time = time.time() - t_ini
        if key_press is None:
            timeout_taser(window)
            choice = -1
        else:
        # key_press = np.random.choice(['c', 'm'], p=(0.1, 0.9))
            if key_press[0] == 'b':
                choice = 0
            elif key_press[0] == 'c':
                choice = 1

    return choice, reaction_time


def timeout_taser(window):
    """If they don't respond quick enough, they see this screen.
    
    Beil dich, Schlampe!
    """
    visual.TextStim(window, text='Du warst zu langsam.\nBitte antworte das nächte Mal schneller').draw()
    window.flip()
    core.wait(1)


def sample_next_points(choice, points, offered_points, do_monster):
    """Given participant choice, calculates new points, and if necessary
    takes away points that the monster has eaten
    Inputs
    ------
    choice : int
    0 (risky) 1 (safe)

    points : list
    points subject currently has (red,blue)

    offered_points : tuple
    [red,blue] number of points offered given risky choice

    do_monster :bool
    if monster comes [no, yes], comes from sample_monster

    """

    eaten_points = [0, 0]
    if choice == 1:
        earned_points = (1, 1)
        points = [points[0]+earned_points[0], points[1]+earned_points[1]]
        tmp_points = points
    elif choice == 0 and do_monster == 0:
        new_points = []
        for x, y in zip(points, offered_points):
            new_points.append(x+y)
        earned_points = offered_points
        points = new_points
        tmp_points = points
    elif choice == 0 and do_monster == 1:
        pre_safe = min(points)
        tmp_points = [offered_points[0] + points[0],
                      offered_points[1] + points[1]]
        eaten_points = [tmp_points[0] - pre_safe,
                        tmp_points[1] - pre_safe]
        points = [pre_safe, pre_safe]
#        uneaten_points = min(points)
#        eaten_points = max(points) - uneaten_points
        # [uneaten_points,uneaten_points]
        earned_points = offered_points
    elif choice == -1:
        pre_safe = min(points)
        eaten_points = [points[0] - pre_safe,
                        points[1] - pre_safe]
        points = [pre_safe, pre_safe]
        tmp_points = points
        earned_points = [0, 0]

    return points, tmp_points, eaten_points, earned_points

# def monster_eats_points(do_monster,points):
#
#    if do_monster == 1:
#        uneaten_points = np.amin(points)
#        eaten_points = np.amax(points) - uneaten_points
#        points = [uneaten_points,uneaten_points]
#    elif do_monster == 0:
#        points = points
#        eaten_points = 0
#
#    return eaten_points


def sample_monster(choice, curr_mblock):
    """Determines if the Monster comes this trial
    Takes input of choice of participant and type of miniblock
    and decides if monster will come or not. Monster can only come
    if participants chose RISKY.

    Returns
    -------
    do_monster : boolean
    0 no monster , 1 monster"""

    p_monster_safe = [0.85, 0.15]
    p_monster_risky = [0.65, 0.35]
    #do_monster       = -1

    if choice == 0: 
        if curr_mblock[:4] == 'safe':
            do_monster = np.random.choice([0, 1], p=p_monster_safe)
        elif curr_mblock[:5] == 'risky':
            do_monster = np.random.choice([0, 1], p=p_monster_risky)
    elif choice == 1:
        do_monster = 1
    else:
        do_monster = 1

    return do_monster


def calculate_monster_probability(trial_number, mblock_type, last_monster_trial,
                                  fixed_prob=0.35, ramp_slope=1/12):
    """Calculates monster probabiliy given the trial number and miniblock type
    according to the rules outlined below ...
    fixed       = probability of monster is constant on any given trial
    oscillating = sine wave independent of actual monster appearance
    reset       = increasing p dependent on monst recent monster appearance

    Parameters
    ----------
    trial_number : int

    mblock_type  : string
    Determines the type of miniblock. Must be 'fixed', 'oscillating' or 'reset.' 

    Returns 
    --------
    p_monster : float
    The probability of the monster appearing on this trial """
    if mblock_type == 'reset' and last_monster_trial is None:
        raise ValueError(
            'Miniblock type was reset but no last monster was provided !')

    if mblock_type == 'fixed':
        p_monster = fixed_prob
    elif mblock_type == 'oscillating':
        p_monster = (-np.cos(trial_number)+1)/2 * 0.8
    elif mblock_type == 'reset':
        how_many_ago = trial_number - last_monster_trial
        p_monster = (how_many_ago-1) * ramp_slope
    else:
        raise ValueError('mBlock type %s was not recognised !' % (mblock_type))

    return p_monster


def create_stimuli(window, points):

    picture_monster = visual.ImageStim(
        window, image=imagefolder+'pointmonster.png', size=(0.6, 0.5), pos=(0, 0.5))
    risky_offer = create_stimuli_risky(window)
    safe_offer = create_stimuli_safe(window)

    point_bars = create_stimuli_points(window, points)

    context_size = 0.3
    context_y = 0.8
    dict_contexts_c = {'risky_red': visual.ImageStim(window,
                                                     image=imagefolder+'risky_red.png',
                                                     size=(context_size, context_size),
                                                     pos=(-0.8, context_y)),
                       'safe_red': visual.ImageStim(window, image=imagefolder+'safe_red.png',
                                                    size=(context_size, context_size),
                                                    pos=(-0.8, context_y)),
                       'risky_blue': visual.ImageStim(window,
                                                      image=imagefolder+'risky_blue.png',
                                                      size=(context_size, context_size),
                                                      pos=(-0.8, context_y)),
                       'safe_blue': visual.ImageStim(window, image=imagefolder+'safe_blue.png',
                                                     size=(context_size, context_size),
                                                     pos=(-0.8, context_y)), }

    # 'Inner' dictionary of the context labels, same thing as in
    # "create_stimuli_points" par ex., but just in this function
    dict_contexts_n = {'risky_red': visual.ImageStim(window,
                                                     image=imagefolder+'risky_red.png',
                                                     size=(context_size, context_size),
                                                     pos=(0.8, context_y)),
                       'safe_red': visual.ImageStim(window, image=imagefolder+'safe_red.png',
                                                    size=(context_size, context_size),
                                                    pos=(0.8, context_y)),
                       'risky_blue': visual.ImageStim(window,
                                                      image=imagefolder+'risky_blue.png',
                                                      size=(context_size, context_size),
                                                      pos=(0.8, context_y)),
                       'safe_blue': visual.ImageStim(window,
                                                     image=imagefolder+'safe_blue.png',
                                                     size=(context_size, context_size),
                                                     pos=(0.8, context_y)),
                       'trick' : visual.ImageStim(window,
                                                  image=imagefolder + 'trick.png',
                                                  size=(context_size, context_size),
                                                  pos=(0.8, context_y))}
    # 'Inner' dictionary of the context labels, same thing as in
    # "create_stimuli_points" par ex., but just in this function

    curr_contexts = drawObjects(dict_contexts_c)
    next_contexts = drawObjects(dict_contexts_n)

    containment_rect = visual.Rect(window, pos=(0, 0), size=(
        pars('maximum_bar_size'), 0.2), lineColor='DimGrey')

    list_stars = []

    star_size = 0.1
    star_y = context_y-0.5*(-context_size+star_size)

    for idx in range(20):
        x_pos = (idx*star_size)-star_size
        list_stars.append(visual.ImageStim(window, image=imagefolder+'star.png',
                                           size=(star_size, star_size),
                                           pos=(x_pos, star_y), opacity=0))

    list_stars = drawObjects(list_stars)

    dict_offers = {'risky': risky_offer, 'safe': safe_offer,
                   'curr_mblock': curr_contexts,
                   'next_mblock': next_contexts,
                   'point_bars': point_bars,
                   'containment_rect': containment_rect,
                   'stars': list_stars}

    dict_feedback = {'curr_mblock': curr_contexts,
                     'next_mblock': next_contexts,
                     'point_bars': point_bars,
                     'containment_rect': containment_rect,
                     'stars': list_stars,
                     'monster': picture_monster}

    dict_mblock_switch = {'curr_mblock': curr_contexts,
                          'next_mblock': next_contexts}

    screen_offer = drawObjects(dict_offers)
    screen_feedback = drawObjects(dict_feedback)
    screen_mblock_switch = drawObjects(dict_mblock_switch)

    return screen_offer, screen_feedback, screen_mblock_switch


def test_create_stimuli():
    window = visual.Window()

    stimuli = create_stimuli(window)

    for screen in stimuli:
        screen.draw()
        window.flip()
        event.waitKeys()

    window.close()


def test_update_stimuli():
    window = visual.Window()

    stimuli = create_stimuli(window)[0]
    stimuli.draw()
    window.flip()
    event.waitKeys()

    stimuli.update_next_mblock('risky_blue')
#    stimuli.update_curr_mblock('risky_red')
    stimuli.draw()
    window.flip()
    event.waitKeys()

    stimuli.update_curr_mblock('risky_red')
    stimuli.draw()
    window.flip()
    event.waitKeys()
#
    window.close()


def test_create_stimuli_risky():
    """Little collection of what we need to fix create_stimuli_risky"""

    window = visual.Window()
    rect_pos = (-0.45, -0.75)
    rect_size = (0.95, 0.3)

    damn_rectangle = visual.Rect(window, pos=rect_pos, width=rect_size[0],
                                 height=rect_size[1], color='white')
    damn_rectangle.draw()

    dict_stimuli_risky = create_stimuli_risky(window, rect_pos, rect_size)

    for stimulus in dict_stimuli_risky['blue']:
        print(stimulus.pos)
        print(stimulus.size)

    for thing in dict_stimuli_risky['blue']:
        thing.draw()
    window.flip()

    event.waitKeys()
    window.close()


def create_stimuli_safe(window, rect_pos=None, rect_size=None):
    """Creates and draws a little box that presents the safe option"""

    if rect_pos is None:
        rect_pos = (0.45, -0.75)

    if rect_size is None:
        rect_size = (0.85, 0.3)

    cord = -np.ones(2)
    max_offer_safe = 2
    dict_stimuli_safe = {}
    coin_size = 0.8*rect_size[1]
    screen_res = window.size
    marg = 0.001

    for ix_ix in range(2):
        cord[ix_ix] = 0.5*rect_size[ix_ix] + rect_pos[ix_ix]

    delta_c = (rect_size[0]-2*marg-coin_size)/(max_offer_safe-1)

    coin_pos = np.zeros(max_offer_safe)
    coin_pos[0] = rect_pos[0]-0.5*rect_size[0] + \
        (0.5*coin_size)  # Do the first coin manuallly

    for ix_coin in range(1, max_offer_safe):
        coin_pos[ix_coin] = coin_pos[ix_coin-1]+delta_c

    stimuli_safe = []

    for ix_ix, colour in enumerate(['blue', 'red']):

        stimuli_safe.append(visual.ImageStim(window, image=imagefolder+'{}circle.png'.format(colour),
                                             size=(
                                                 coin_size*screen_res[1]/screen_res[0], coin_size),
                                             pos=(coin_pos[ix_ix], rect_pos[1])))

    stimuli_safe.insert(0, visual.Rect(window,
                                       pos=rect_pos,
                                       width=rect_size[0],
                                       height=rect_size[1],
                                       lineColor='white'))
   # add the nice little container rectangle to the list

    return drawObjects(stimuli_safe)


def create_stimuli_risky(window, rect_pos=None, rect_size=None):
    """Creates X amount of red or blue circles for later drawing"""

    if rect_pos is None:
        rect_pos = (-0.45, -0.75)

    if rect_size is None:
        rect_size = (0.85, 0.3)

    cord = -np.ones(2)
    marg = 0.001  # Little margin on the left and right inside the rectangle
    coin_size = 0.7*rect_size[1]  # "c" of equation; %of the "risky" box
    max_offer_risky = 9  # How many max coins can be offered, keep flexible for later
    dict_stimuli_risky = {}
    screen_res = window.size

    for ix_ix in range(2):
        cord[ix_ix] = 0.5*rect_size[ix_ix] + rect_pos[ix_ix]

    delta_c = (rect_size[0]-2*marg-coin_size)/(max_offer_risky-1)

    coin_pos = np.zeros(max_offer_risky)
    coin_pos[0] = rect_pos[0]-0.5*rect_size[0] + \
        (0.5*coin_size)  # Do the first coin manuallly

    for ix_coin in range(1, max_offer_risky):
        coin_pos[ix_coin] = coin_pos[ix_coin-1]+delta_c

    stimuli_risky = []

    for colour in ['blue', 'red']:

        for ix_ix in range(max_offer_risky):
            stimuli_risky.append(visual.ImageStim(window,
                                                  image=imagefolder +
                                                  '{}circle.png'.format(
                                                      colour),
                                                  size=(
                                                      coin_size*screen_res[1]/screen_res[0], coin_size),
                                                  pos=(coin_pos[ix_ix], rect_pos[1])))

#        dict_stimuli_risky[colour] = stimuli_risky

    stimuli_risky.insert(0, visual.Rect(
        window, pos=rect_pos, width=rect_size[0], height=rect_size[1], lineColor='white'))
   # add the nice little container rectangle to the list

    return drawObjects(stimuli_risky)


def create_stimuli_points(window, points):
    bar_sizes = np.array([0, 0.1])
    height = bar_sizes[-1] / 2
    dict_points = {"stim_risky_red": visual.Rect(window, pos=(0, -height),
                                                 size=bar_sizes, fillColor='lightpink',
                                                 lineColor='lightpink'),
                   "stim_risky_blue": visual.Rect(window, pos=(0.0, height),
                                                  size=bar_sizes, fillColor='lightsteelblue',
                                                  lineColor='lightsteelblue'),
                   "stim_safe_red": visual.Rect(window, pos=(0.0, -height),
                                                size=bar_sizes, fillColor='tomato',
                                                lineColor='tomato'),
                   "stim_safe_blue": visual.Rect(window, pos=(0.0, height),
                                                 size=bar_sizes, fillColor='steelblue',
                                                 lineColor='steelblue')}

    # Draws the initial distribution at the beginning of the miniblock

    points_initial = np.array(points)/pars('bar_scaling')
    safe_points = points_initial.min()
    extra_growth = points_initial.max() - points_initial.min()

    for bar in dict_points.values():
        current_size = bar.size
        new_size = (current_size[0]+safe_points, current_size[1])
        bar.size = new_size

    if points[0] > points[1]:
        curr_red_size = dict_points['stim_risky_red'].size
        new_red_size = (curr_red_size[0] + extra_growth, curr_red_size[1])
        dict_points['stim_risky_red'].size = new_red_size

    elif points[0] < points[1]:
        curr_blue_size = dict_points['stim_risky_blue'].size
        new_blue_size = (curr_blue_size[0] + extra_growth, curr_blue_size[1])
        dict_points['stim_risky_blue'].size = new_blue_size

    screen_points = drawObjects(dict_points)

    return screen_points


def mblock_structure():
    """ Stupidly picks a miniblock type for feeding into the run miniblock bit of the code"""

    ix_block = np.random.randint(3)
    mblock_types = {"blocks": ["fixed", "oscillating", "reset"]}

    mblock_type = mblock_types["blocks"][ix_block]

    return mblock_type

#
# class drawScreens():
#    window = visual.Window()
#
#    def __init__(self,total_objects,screen_type):
#        self.total_objects = []
#        self.screen_type   = ''
##
# def screen_offer(self,5,'offer'):
# pass
# def screen_feedback(self,3,'feedback'):
# pass
# def screen_monster(self,1,'monster'):
# pass
##


class drawObjects(object):
    def __init__(self, objects_to_draw):
        self.objects_to_draw = objects_to_draw

        if 'curr_mblock' in self.objects_to_draw:
            self.all_mblock_types_c = objects_to_draw['curr_mblock']
        if 'next_mblock' in self.objects_to_draw:
            self.all_mblock_types_n = objects_to_draw['next_mblock']
        if 'risky' in self.objects_to_draw:
            self.all_risky = objects_to_draw['risky'].objects_to_draw
        if 'stars' in self.objects_to_draw:
            self.all_stars = objects_to_draw['stars'].objects_to_draw

    def draw(self,):
        #        for object_key in self.objects_to_draw:
        #            objects_to_draw[object_key].draw()
        #        for object_key,object_value in self.objects_to_draw.items():
        #            object_value.draw()
        if isinstance(self.objects_to_draw, dict):
            for object_value in self.objects_to_draw.values():
                object_value.draw()

        elif isinstance(self.objects_to_draw, list):
            for object_value in self.objects_to_draw:
                object_value.draw()

        else:
            raise ValueError('Input should either be a dick or list')

    def update_points(self, window, points, waits, eat=False):
        """Increases the point bar sizes to the offer taken and, if the monster
        came and there were points to eat, the monster is shown and takes
        the points away.

        """

        points = np.array(points) / pars('bar_scaling')
        safe_points = points.min()
        all_point_bars = self.objects_to_draw['point_bars'].objects_to_draw

        current_height = all_point_bars['stim_risky_blue'].size[1]

        all_point_bars['stim_risky_red'].size = (points[0], current_height)
        all_point_bars['stim_risky_blue'].size = (points[1], current_height)
        self.draw()
        window.flip()
        core.wait(waits['pre_monster'])
        self.invisible_monster(eat)
        self.draw()
        window.flip()
        core.wait(waits['monster'])
        if eat:
            self.eat_points()
        else:
            all_point_bars['stim_safe_red'].size = (
                safe_points, current_height)
            all_point_bars['stim_safe_blue'].size = (
                safe_points, current_height)
        self.invisible_monster(0)
        self.draw()
        window.flip()
        core.wait(waits['post_monster'])

    def eat_points(self):
        '''If points are eaten, updates screen to reflect this'''

        bar_size = []

        all_point_bars = self.objects_to_draw['point_bars'].objects_to_draw
        for bar in all_point_bars.values():
            bar_size.append(bar.size[0])
        bar_min = min(bar_size)

        for bar in all_point_bars.values():
            bar.size = [bar_min, bar.size[1]]

    def update_curr_mblock(self, curr_mblock):
        if not isinstance(curr_mblock, str):
            all_dictionary_keys = list(
                self.all_mblock_types_c.objects_to_draw.keys())
            where_look = all_dictionary_keys[curr_mblock]
        else:
            where_look = curr_mblock

        self.objects_to_draw['curr_mblock'] = \
            self.all_mblock_types_c.objects_to_draw[where_look]

    def update_next_mblock(self, next_mblock):
        if not isinstance(next_mblock, str):
            all_dictionary_keys = define_mblock_types()
            where_look = all_dictionary_keys[next_mblock]
        else:
            where_look = next_mblock
        self.objects_to_draw['next_mblock'] = \
            self.all_mblock_types_n.objects_to_draw[where_look]

    def update_risky_offer(self, colour, amt_offered):
        if colour == 'blue':
            add_place = 1

        elif colour == 'red':
            add_place = int(((len(self.all_risky)-1)/2)+1)
            # here be monsters

        self.objects_to_draw['risky'].objects_to_draw = \
            self.all_risky[add_place:(add_place + amt_offered)]
        self.objects_to_draw['risky'].objects_to_draw.insert(
            0, self.all_risky[0])

    def invisible_monster(self, do_monster):
        self.objects_to_draw['monster'].opacity = float(do_monster)

    def display_stars(self, stars):

        m = np.arange(0, stars*2, 2)

        star_size = self.all_stars[0].size[0]
        star_pos_y = self.all_stars[0].pos[1]
        star_pos_x = (-(stars-1-m)/2)*star_size

        for idx in range(stars):
            self.all_stars[idx].opacity = 1

            self.all_stars[idx].pos = (star_pos_x[idx], star_pos_y)
