#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
This is the file to run when you want to run the full experiment
"""
import itertools as it
import argparse
import os
import copy
import pickle
import time

import numpy as np
from psychopy import core, visual, event

import miniblock as mb
import shady_business as shade

# Directory to save the experimental data:
DIR = os.path.dirname(os.path.abspath(__file__))
DATA_FOLDER = os.path.abspath(DIR + '/../exp_data/')
DEBUG = False

# FMRI trigger key
FMRI_TRIGGER = 't'

# Dictionary to save the time of each slice of fmri:
TIMINGS = {'slice_time': [], 'slice_epoch': []}


def get_pars(key=None, args=None, phase='fmri'):
    """Return the parameter called --key--. If None, returns all parameters
    in a dictionary.

    The number of mini-blocks depends on whether it's for fmri or not, which
    is controlled with the command-line argument --fmri.

    Parameters:
    -----------

    args : argparse.ArgumentParser
    Command-line arguments passed.

    phase : string
    If --args-- is not provided, this can determine the phase to use.


    Raises
    ------

    ValueError : If more than one flag (--fmri, --behavioural,
    --prefmri, --debug) was provided

    """
    if args is not None:
        count = 0
        if args.behavioural:
            phase = 'behavioural'
            count += 1
        if args.fmri:
            phase = 'fmri'
            count += 1
        if args.prefmri:
            phase = 'prefmri'
            count += 1
        if DEBUG:
            phase = 'debug'
            count += 1
        if count != 1:
            raise ValueError('Too many options (--fmri, --behavioural, '
                             '--prefmri, --debug) '
                             'specified.')

    if phase == 'behavioural':
        num_trials = 6
        num_trials_training = 6
        num_mblocks_training = 16
        conditions = [0, 0.75]
        num_mblocks = [80, 32]
        dick = dict(num_trials=num_trials,
                    num_trials_training=num_trials_training,
                    num_mblocks_training=num_mblocks_training,
                    conditions=conditions,
                    num_mblocks=num_mblocks)
    elif phase == 'fmri':
        num_trials = 6
        conditions = [0, 0.75]
        num_mblocks = [20, 20]
        dick = dict(num_trials=num_trials,
                    conditions=conditions,
                    num_mblocks=num_mblocks,
                    fmri_min_pause=60,
                    fmri_max_pause=180)
    elif phase == 'prefmri':
        num_trials = 6
        num_trials_training = 3
        num_mblocks_training = 4
        conditions = [0, 0.75]
        num_mblocks = [16, 4]
        dick = dict(num_trials=num_trials,
                    conditions=conditions,
                    num_mblocks=num_mblocks)
        
    elif phase == 'debug':
        num_trials = 3
        conditions = [0, 0.25, 0.75]
        num_mblocks = [2, 2, 2]
        num_trials_training = 3
        num_mblocks_training = 2
        dick = dict(num_trials=num_trials,
                    num_trials_training=num_trials_training,
                    num_mblocks_training=num_mblocks_training,
                    conditions=conditions,
                    num_mblocks=num_mblocks)
    else:
        raise ValueError('--phase-- not recognized. Is: {}, should be in '
                         '("behavioural", "fmri", "prefmri, '
                         '"debug"")'.format(phase))
    if key is None:
        return dick
    return dick[key]


def pump_weights():
    weights = np.array([1, 3, 2, 4, 3, 1, 4, 2, 1, 4, 1, 3, 4, 1, 3, 1])

    return weights


def which_experiment(args):
    '''stupidy picks which version of the experiment to run'''

    if args.fmri is False:
        behavioural_experiment(args)

    if args.fmri is True:
        fmri_experiment(args)


def fmri_experiment(args):

    pars = get_pars(args=args)

    if args.small is False:
        window = visual.Window(fullscr=True)
    elif args.small is True:
        window = visual.Window()
    global_keys(window)
    if args.subject_id is None:
        raise ValueError('You did not input a subject number! SHAME!')

    if args.notrain is True:
        raise ValueError('There is no training in the fMRI, '
                         'are you confused ?')

    if args.noexp is False:
        event.waitKeys(keyList=(FMRI_TRIGGER, ))
        first_slice_fmri_epoch = time.time()
        first_slice_fmri = time.strftime("%H%M%S", time.localtime())
        exp_results = conditions(window, pars, fmri=True)
        exp_results['onsets'].update({'first_sequence_onset_epoch': first_slice_fmri_epoch,
                                      'first_sequence_onset': first_slice_fmri})
    else:
        exp_results = {'No Experiment': 'No Experiment'}

    if not args.noshade:
        first_slice_shade_epoch = time.time()
        first_slice_shade = time.strftime("%H%M%S", time.localtime())
        intro_shady(window)
        shade_results = shade.run_shady(window)
    else:
        shade_results = {'No shade': 'Its always sunny in Philadelphia'}

    all_results = {'experiment': exp_results,
                   'shade': shade_results,
                   'slices': TIMINGS}  # Times of all fmri slices

    fmri_outro(window)

    with open(DATA_FOLDER + '/data_fmri_{}.pi'.format(args.subject_id),
              'wb') as dumpy:
        pickle.dump(all_results, dumpy)
    window.close()


def behavioural_experiment(args):
    """Runs the entire behavioral experiment, with all its phases."""
    pars = get_pars(args=args)
    

    if args.small is False:
        window = visual.Window(fullscr=True)
    elif args.small is True:
        window = visual.Window()
    if args.subject_id is None:
        raise ValueError('You did not input a subject number! SHAME!')
    global_keys(window)

    if args.notrain is False:
        training_results = run_training(window)
    else:
        print('no train, sucka')
        training_results = {'No Training': 'No Training'}

    if args.noexp is False:
        exp_results = conditions(window, pars, prefmri=args.prefmri)
    else:
        exp_results = {'No Experiment': 'No Experiment'}

    all_results = {'training': training_results,
                   'experiment': exp_results
                   }
    if args.prefmri:
        filename = '/data_prefmri_{}.pi'
    else:
        filename = '/data_behavioural_{}.pi'
    with open((DATA_FOLDER + filename).format(args.subject_id),
              'wb') as dumpy:
        pickle.dump(all_results, dumpy)
    outro_text(window, fmri=False)

    window.close()


def run_training(window):
    """Runs the training for the behavioral part of the experiment."""
    phase = 'debug' if DEBUG else 'behavioural'
    num_mblocks = get_pars('num_mblocks_training', phase=phase)
    num_trials = get_pars('num_trials_training', phase=phase)
    break_interval = 4
    all_mblock_types = define_mblock_types()

    when_breaks = set(np.arange(break_interval, num_mblocks, break_interval))

    weights = np.ones([num_mblocks], dtype=np.int64)

    points = [0, 0]  # starting number of points in training
    stars = 0     # starting number of stars in training
    training_results = {}  # results of training

    intro_train(window)  # run the intro text before training
    flag_con = True
    while flag_con:
        contexts, _, _ = get_transitions(num_mblocks, weights)
        if len(contexts) == num_mblocks:
            flag_con = False

    for ix_mblock, curr_type in enumerate(contexts):
        if ix_mblock in when_breaks:
            show_break(window)
        curr_mblock = all_mblock_types[curr_type]
        ix_mblock_next = ix_mblock + 1
        next_type = contexts[ix_mblock_next %
                             num_mblocks]  # makes sure last
        # miniblock wont break everything, using the modulus thingy
        next_mblock = all_mblock_types[next_type]

        new_results, points, stars, screen_mblock_switch = mb.one(
            curr_mblock, next_mblock, points, stars, window,
            num_trials=num_trials, fast=FAST)
        # core.wait(1.5)
        training_results = concatenate_results(new_results, training_results)
        animate_mblock_switch(window, screen_mblock_switch, next_type,
                              next_type)

    return training_results


def conditions(window, pars, prefmri=False, fmri=False):
    """Separates the experimental run into the three blocks with different
    probabilities of the next context being a lie
    """
    global_keys(window)
    imagefolder = '../Images/'
    if fmri:
        with open('./contexts_fmri.pi', 'rb') as dumpy:
            contexts_real = pickle.load(dumpy)
    else:
        with open('contexts_behavioural.pi', 'rb') as dumpy:
            contexts_real = pickle.load(dumpy)

    all_contexts_trick = []
    conditions = pars['conditions']
    num_mblocks = pars['num_mblocks']
    num_trials = pars['num_trials']
    # Probabilities of it being a HORRIBLE LIE

    all_results = {}

    intro_exp(window, prefmri=prefmri, fmri=fmri)  # show the introductary before experiment

    for prob, c_num_mblocks in zip(conditions, num_mblocks):
        contexts_real = contexts_real[:c_num_mblocks]
        if prob == 0:
            # TODO: Get rid of contexts_trick when prob is zero.
            with open('contexts_trick_{}.pi'.format(int(100 * prob)),
                      'rb') as dumpy:
                contexts_trick = pickle.load(dumpy)
            contexts_trick = contexts_trick[:c_num_mblocks]
            all_contexts_trick.append(contexts_trick)
            all_results[prob] = many_mblocks(window,
                                             contexts_real,
                                             contexts_real, # TODO: remove this horrible hack (see previous one)
                                             num_trials,
                                             fmri=fmri)
            pause_end_epoch, pause_end = intercondition_pause(window, fmri=fmri)
            all_results['onsets'] = {'second_sequence_onset_epoch': pause_end_epoch,
                                     'second_sequence_onset': pause_end}
        elif prob == 0.75:
            no_future_instructions(window, imagefolder, fmri=fmri)
            if fmri:
                contexts_75 = contexts_real
            else:
                with open('./contexts_75.pi', 'rb') as mafi:
                    contexts_75 = pickle.load(mafi)[:c_num_mblocks]
            contexts_trick = 4 * np.ones(c_num_mblocks).astype(int)
            all_contexts_trick.append(contexts_trick)
            all_results[prob] = many_mblocks(window, contexts_75,
                                             contexts_trick, num_trials,
                                             fmri=fmri)
        else:
            # This creates the instructions for any reliability
            condition_instructions = trick_condition_instructions(window,
                                                                  prob,
                                                                  imagefolder)
            #This draws those instructions in the order that they are
            for c_text in condition_instructions:
                c_text.draw()
                window.flip()
                event.waitKeys()
            core.wait(2)
            with open('contexts_trick_{}.pi'.format(int(100 * prob)),
                      'rb') as dumpy:
                contexts_trick = pickle.load(dumpy)
                contexts_trick = contexts_trick[:c_num_mblocks]
            all_contexts_trick.append(contexts_trick)
            all_results[prob] = many_mblocks(window, contexts_real,
                                             contexts_trick, num_trials,
                                             fmri=fmri)

    return all_results


def many_mblocks(window, contexts_real, contexts_trick,
                 num_trials, fmri=False):
    """Runs an entire block of many mini-blocks with the same condition."""
    global_keys(window)
    num_mblocks = len(contexts_real)
    break_interval = 8
    all_mblock_types = define_mblock_types()
    # print('Mblocks: {}, trials: {}'.format(num_mblocks, num_trials))

    if fmri:
        when_breaks = []
    else:
        when_breaks = set(np.arange(break_interval,
                                    num_mblocks,
                                    break_interval))

    # The hand-picked weights of how important it is to display each
    # type of miniblock, ints to be fed into get_transitions
    # weights = pump_weights()

    # num_mblock_types = np.arange(len(all_mblock_types))

    points = [0, 0]  # starting number of points
    stars = 0  # starting number of stars

    all_results = {}  # initialising

    for ix_mblock, curr_type in enumerate(contexts_real):
        if ix_mblock in when_breaks:
            show_break(window)
        curr_mblock = all_mblock_types[curr_type]
        ix_mblock_next = ix_mblock + 1
        next_type = contexts_trick[ix_mblock_next %
                                   num_mblocks]  # makes sure last
        # miniblock wont break everything, using the modulus thingy
        next_mblock = all_mblock_types[next_type]

        new_results, points, stars, screen_mblock_switch = mb.one(
            curr_mblock, next_mblock, points, stars, window, num_trials,
            fmri=fmri, fast=FAST)
        core.wait(1.5)
        all_results = concatenate_results(new_results, all_results)
        next_type_realsies = contexts_real[ix_mblock_next % num_mblocks]
        animate_mblock_switch(window, screen_mblock_switch, next_type,
                              next_type_realsies)

    return all_results


def no_future_instructions(window, imagefolder, fmri=False):
    """THERE IS NO FUTURE.

    """

    ss_size = (1.5, 1.5)
    ss_pos = (0, 0.3)

    if fmri is True:
        no_future_fmri = ('Genau so, wie es war, als Sie vorher gespielt haben, (nicht im Scanner...)'
                          'ab jetzt werden wir Ihnen nicht mehr sagen, '
                           'welcher Kontext als nächstes kommt. '
                           '\n\n\n\nDrücke eine beliebige Taste, um fortzufahren...')
        visual.TextStim(window,text=no_future_fmri).draw()
        window.flip()
        event.waitKeys()
        
    else:
        visual.TextStim(window, ('Ab jetzt werden wir Ihnen nicht mehr sagen, '
                                 'welcher Kontext als nächstes kommt. ')).draw()
        window.flip()
        event.waitKeys()
    
        visual.TextStim(window,
                        'Mit anderen Worten: es wird keine Vorhersage mehr geben.',
                        pos=(0.0, -0.7)).draw()
        no_future_1 = visual.ImageStim(window,
                                       image=imagefolder + 'no_future.png',
                                       size=ss_size, pos=ss_pos)
        no_future_1.draw()
        window.flip()
        event.waitKeys()
    
        visual.TextStim(window,
                        ('Der nächste Kontext könnte jeder der vier '
                         '(rot / blau; gefährlich / sicher) sein.'),
                        pos=(0.0, -0.7)).draw()
        no_future_2 = visual.ImageStim(window,
                                       image=imagefolder + 'nofuture_any.jpeg',
                                       size=ss_size, pos=ss_pos)
        no_future_2.draw()
        window.flip()
        event.waitKeys()


def trick_condition_instructions(window, probability, imagefolder):
    """Creates instructions that state that the probability of the next
    miniblock is accurate is --probability--, for any value of
    --probability-- that you give it.

    """
    ss_size = (1.5, 1.5)
    ss_pos = (0, 0.3)

    text = ('Ab jetzt ist die Wahrscheinlichkeit {:2.0f}, dass die vorhersage '
            'darüber welcher Kontext als nächstes kommt, falsch '
            'ist.'.format(100 * probability))
    stim_text_1 = visual.TextStim(window, text=text)
    text = ('Vergessen Sie nicht, dass eine falsche Vorhersage des nächsten '
            'Kontextes durch einen weissen Rahmen angezeigt wird, bevor '
            'Sie sehen welcher Kontext wirklich der nächste ist.')
    stim_text_2 = visual.TextStim(window, text=text,
                                  pos=(0.0, -0.7), wrapWidth=1.85,)

    ss_con = visual.ImageStim(window, image=imagefolder+'context_lie_2.png',
                              size=ss_size, pos=ss_pos)

    ss_con_2 = visual.ImageStim(window, image=imagefolder+'context_lie_3.png',
                                size=ss_size, pos=ss_pos)

    ss_con_3 = visual.ImageStim(window, image=imagefolder+'context_lie_4.png',
                                size=ss_size, pos=ss_pos)
    return [stim_text_1, stim_text_2, ss_con, ss_con_2, ss_con_3]


def intercondition_pause(window, fmri=False):
    """Draws a pause screen between the conditions during fmri. The screen
    lasts at least XXX minutes, at most ZZZ minutes, and can end
    between those two times if the participant presses anything.

    After it ends, it waits for the crazy lady behind the glass to press the
    key w to continue.

    CHANGED THIS KEY TO W AS IT WAS DONKING UP THE PAUSE THINGY !!!!!!!!

    """
    
    if fmri is True:
        pars = get_pars(phase='fmri')
        visual.TextStim(window, text='Du hast eine Pause verdient.').draw()
        window.flip()
        core.wait(pars['fmri_min_pause'])
        text = 'Drücke eine beliebige Taste, wenn Du bereits bist.'
        visual.TextStim(window, text=text).draw()
        window.flip()
        max_wait = pars['fmri_max_pause'] - pars['fmri_min_pause']
        event.waitKeys(maxWait=max_wait)
        visual.TextStim(window, text='Es geht gleich weiter.').draw()
        window.flip()
        event.waitKeys(keyList=[FMRI_TRIGGER])
        second_sequence_onset_epoch = time.time()
        second_sequence_onset = time.strftime("%H%M%S", time.localtime())
        event.waitKeys(keyList=['w'])
    else:
        text = 'Du hast eine Pause verdient ! :)'
        '\n\nDrücke eine beliebige Taste, wenn Du bereits bist.'
        visual.TextStim(window, text=text).draw()
        window.flip()
        event.waitKeys()
        second_sequence_onset_epoch = 0  # Dummy value
        second_sequence_onset = 0  # Dummy value
    return second_sequence_onset_epoch, second_sequence_onset


def fuck_with_contexts(probabilities):
    ''' Messes up the contexts.pi files so that they display upcoming next contexts
    in accordance with the probabilities of being messed up.

    Loads the real contexts from the file ./contexts.pi, and the fucked-up
    contexts are saved to files with names contexts_trick_%%.pi, where %% is in
    --probabilities--.

    Inputs :
        probabilities : list of how likely it is to be A HORRIBLE LIE
    Outputs:
        fucked_up_contexts : some contexts.pi files that are all donked up'''

    with open('contexts.pi', 'rb') as dumpy:
        contexts = pickle.load(dumpy)

    num_contexts = len(contexts)

    for probability in probabilities:
        num_indices = int(np.ceil(num_contexts * probability))
        which_are_tricky = np.arange(num_contexts)
        np.random.shuffle(which_are_tricky)
        which_are_tricky = which_are_tricky[:num_indices]
        trick_contexts = copy.copy(contexts)
        for tricky_dicky in which_are_tricky:
            context_types = [0, 1, 2, 3]
            context_types.remove(contexts[tricky_dicky])
            trick_contexts[tricky_dicky] = np.random.choice(context_types, 1)
        with open('./contexts_trick_{}.pi'.format(int(100 * probability)),
                  'wb') as dumpy:
            pickle.dump(trick_contexts, dumpy)


def animate_mblock_switch(window, screen, next_type, next_type_realsies):
    """Animates the switch in miniblocks."""
    # print('animate', next_type, next_type_realsies)
    duration = 1.5
    fps = 30
    num_frames = int(duration * fps)
    # delta_t = 1 / fps
    draw_frame = not (next_type == next_type_realsies)
    c_pos = screen.objects_to_draw['next_mblock'].pos
    c_size = np.array(screen.objects_to_draw['next_mblock'].size) * 1.2
    if draw_frame:
        frame = visual.Rect(window, pos=c_pos, width=c_size[0],
                            height=c_size[1],
                            fillColor='white',
                            lineWidth=0)

    if draw_frame:
        frame.draw()
    screen.draw()
    window.flip()
    core.wait(1)

    screen.update_next_mblock(next_type_realsies)
    if draw_frame:
        frame.draw()
    screen.draw()
    window.flip()
    core.wait(0.5)

    positions = np.linspace(screen.objects_to_draw['next_mblock'].pos[0],
                            screen.objects_to_draw['curr_mblock'].pos[0],
                            num_frames)
    for pos in positions:
        old_pos_y = screen.objects_to_draw['next_mblock'].pos[1]
        screen.objects_to_draw['next_mblock'].pos = [pos, old_pos_y]
        screen.draw()
        window.flip()
        # core.wait(delta_t)


def define_mblock_types():
    """ Seperately shows the list of miniblock types """
    return mb.define_mblock_types()


def concatenate_results(new_results, old_results):
    """Concatenates the results from different runs of miniblock.one()."""
    # Check to see if this is the first time going through
    if len(old_results) == 0:
        old_results = new_results
        return old_results

    all_results = dict()

    for keys in new_results:
        all_results[keys] = np.concatenate(
            [old_results[keys], new_results[keys]], axis=0)

    return all_results


def show_break(window):
    ''' This makes a break.'''

    text_of_break = visual.TextStim(window,
                                    text='Sie haben eine Pause verdient ! ',
                                    pos=(0.0, 0.4))
    text_of_break.draw()
    window.flip()
    core.wait(5)
    text_of_break.draw()
    text = 'Drücke eine beliebige Taste, wenn Du bereits bist.'
    visual.TextStim(window, text=text, pos=(0.0, -0.7)).draw()
    window.flip()
    event.waitKeys(maxWait=90)


def intro_train(window):
    '''shows the introductary text of the experiement'''

    font_height = 0.09

    imagefolder = '../Images/'
    ss_size = (1.5, 1.5)
    ss_pos = (0, 0.3)
    font = 'Arial'

    intro_text_1 = ('Willkommen! Danke, dass Sie sich die Zeit genommen haben!'
                    '\n\nZiel des Experiments ist es, so viele Punkte wie '
                    'moeglich zu sammeln. Sie koennen immer zwischen zwei '
                    'Optionen waehlen:\n\nSie koennen einen Punkt von jeder '
                    'der zwei Farben bekommen (einen roten Punkt und einen blauen '
                    'Punkt) oder 9 Punkte von nur einer Farbe.\n\n'
                    '                   ... Aber passen Sie auf!\nWenn '
                    'Sie sich fuer die Option mit den 9 Punkten entscheiden,'
                    ' besteht die Moeglichkeit, dass das Punktemonster kommt '
                    'und ihre ungesicherten Punkte frisst.'
                    ' Sie sichern Ihre Punkte, indem sie die entsprechenden '
                    'Punkte der anderen Farbe sammeln.\n\n'
                    'Schauen wir uns im Folgenden Schritt fuer Schritt an, '
                    'wie dies im Experiment aussieht.'
                    '\n\nDruecken Sie eine beliebige Taste um fortzufahren...')

    intro_1 = visual.TextStim(window, text=intro_text_1, height = font_height,
                              wrapWidth=1.95, alignHoriz='center')
    intro_1.draw()
    window.flip()
    event.waitKeys()

    # Show and explain the difference between the two choices
    ss_2 = visual.ImageStim(window, image=imagefolder +
                            'risky_or_safe_1.png', size=ss_size, pos=ss_pos)
    intro_text_2 = ('Sie koennen sich immer zwischen der risikobehafteten '
                    'Option, bei der Sie 9 Punkte einer Farbe bekommen, oder '
                    'der sicheren Option, bei der sie einen Punkt von '
                    'jeder Farbe bekommen, entscheiden.')

    intro_2 = visual.TextStim(window, text=intro_text_2, pos=(
        0.0, -0.7), wrapWidth=1.85, font=font, height = font_height)
    ss_2.draw()
    intro_2.draw()
    window.flip()
    event.waitKeys()

    ss_3 = visual.ImageStim(window, image=imagefolder +
                            'risky_or_safe_2.png', size=ss_size, pos=ss_pos)
    intro_text_3 = ('Druecken Sie "C", um 9 Punkte zu bekommen und "M" fuer '
                    'die gepaarten Punkte. \nSeien Sie jedoch vorsichtig! Wenn '
                    'sie sich fuer die 9 Punkte entscheiden, besteht'
                    ' die Chance, dass das Punktemonster Ihre nicht '
                    'gesicherten Punkte frisst.')
    intro_3 = visual.TextStim(window, text=intro_text_3, pos=(
        0.0, -0.7), wrapWidth=1.85, font=font, height = font_height)
    ss_3.draw()
    intro_3.draw()
    window.flip()
    event.waitKeys()

    # Show and explain where the points are displayed and what that means
    ss_4 = visual.ImageStim(window, image=imagefolder +
                            'show_points_1.png', size=ss_size, pos=ss_pos)
    intro_text_4 = ('Die Punkte, die Sie ueber das gesamte Experiment gesammelt haben, werden hier angezeigt.'
                    '\nFuer jedes komplette Fuellen der grauen Box bekommen Sie einen Stern.')

#    intro_text_4 = ('The points you have earned over the whole experiment are shown here.'
#                    '\nIf you fill up the grey box, you get a star to represent that.')
    intro_4 = visual.TextStim(window, text=intro_text_4, pos=(
        0.0, -0.7), wrapWidth=1.85, font=font, height = font_height)
    ss_4.draw()
    intro_4.draw()
    window.flip()
    event.waitKeys()

    ss_5 = visual.ImageStim(window, image=imagefolder +
                            'show_points_2.png', size=ss_size, pos=ss_pos)
    intro_text_5 = ('Gesicherte Punkte werden in der Mitte dunkel angezeigt.'
                    '\nPunkte werden gesichert, wenn gleich viele Punkte in beiden Farben vorhanden sind.'
                    '\nNoch nicht gesicherte Punkte sind heller. Diese Punkte werden vom Punktemonster gefressen.')

#    intro_text_5 = ('Matched points are shown in the middle, and are darker.'
#                    '\nUnmatched points are shown in a lighter colour.'
#                    '\nYou will lose these points if the monster comes.')
    intro_5 = visual.TextStim(window, text=intro_text_5, pos=(
        0.0, -0.7), wrapWidth=1.85, font=font, height = font_height)
    ss_5.draw()
    intro_5.draw()
    window.flip()
    event.waitKeys()

    # Show and explain the puppy star system which is kind of the point of the experiment now
    ss_5a = visual.ImageStim(
        window, image=imagefolder+'show_points_2a.png', size=ss_size, pos=ss_pos)
    intro_text_5a = ('Wenn Sie die graue Box mit gesicherten Punkten auffuellen, bekommen Sie einen Stern. '
                     'Sie behalten dabei alle Punkte die Sie ueber die Schwelle hinaus gesammelt haben.')

#    intro_text_5a = ('If you fill up a grey box with matched points, they turn into a star.'
#                     '\nDon"t worry, you keep all the extra points !')
    intro_5a = visual.TextStim(window, text=intro_text_5a, pos=(
        0.0, -0.7), wrapWidth=1.85, font=font, height = font_height)
    ss_5a.draw()
    intro_5a.draw()
    window.flip()
    event.waitKeys()

    # Show and explain the contexts
    ss_6 = visual.ImageStim(window, image=imagefolder +
                            'contexts.png', size=ss_size, pos=ss_pos)
    intro_text_6 = ('Außerdem werden Sie im Experiment mit verschiedenen Kontexten konfrontiert. '
                    '\nOben sehen Sie, in welchem Kontext Sie sich gerade befinden (links) und welcher Kontext als '
                    'naechstes folgt (rechts). '
                    'Der aktuelle Kontext (links) bestimmt, ob es wahrscheinlich oder unwahrscheinlich ist, dass das '
                    'Punktemonster bei der 9-Punkte-Option kommt und ihre ungesicherten Punkte frisst.')

#    intro_text_6 = ('At the top of your screen, you see what condition you are playing in currently,'
#                    ' and what condition will be next.'
#                    '\nYou can play in a condition that gives you red points or blue points'
#                    ' It is either a risky condition (monster is likely to come if you press C)'
#                    ' or a safe condition (monster is less likely to come if you press C)')
    intro_6 = visual.TextStim(window, text=intro_text_6, pos=(
        0.0, -0.7), wrapWidth=1.9, font=font, height = font_height)
    ss_6.draw()
    intro_6.draw()
    window.flip()
    event.waitKeys()
    

    ss_7 = visual.ImageStim(window, image=imagefolder +
                            'context_color.png', size=ss_size, pos=ss_pos)
    intro_text_7 = ('Das Bild im aktuellen Kontext bestimmt die Auftretenswahrscheinlichkeit des Punktemonsters: '
                    '\nSchweinchenbild = sicherer Kontext, also erscheint das Punktemonster selten.'
                    '\nMonsterbild = risikoreicher Kontext, also erscheint das Punktemonster haeufiger.'
                    '\nAußerdem bestimmt die Hintergrundfarbe des aktuellen Kontextes die Farbe der Punkte in der 9-Punkte-Option (Taste "C").' )


    intro_7 = visual.TextStim(window, text=intro_text_7, pos=(
        0.0, -0.7), wrapWidth=1.9, font=font, height = font_height)
    ss_7.draw()
    intro_7.draw()
    window.flip()
    event.waitKeys()

    ss_8 = visual.ImageStim(window, image=imagefolder +
                            'contexts_2.png', size=ss_size, pos=ss_pos)
    intro_text_8 = ('Nach einigen Runden ändert sich der aktuelle Kontext.'
                    '\nDabei verschiebt sich das rechte Bild nach links und wird somit zum aktuellen Kontext.'
                    '\nGleichzeitig erscheint rechts ein neuer Kontext, der wiederum den naechsten Kontext anzeigt.' )

#    intro_text_8 = ('When conditions change, you will see the ''next'' condition move to the left'
#                    '\nA new, upcoming condition will take its place.')
    intro_8 = visual.TextStim(window, text=intro_text_8, pos=(
        0.0, -0.7), wrapWidth=1.85, font=font, height = font_height)
    ss_8.draw()
    intro_8.draw()
    window.flip()
    event.waitKeys()

    # Start the training
    intro_text_9 = ('Zuerst koennen Sie die Aufgabe ein paar Runden ueben, um sich mit der Aufgabe vertraut zu machen.'
                    '\n\n\n\nDruecken Sie eine beliebige Taste, um mit dem Training zu starten!')

#    intro_text_8 = ('Let us first start with a few practice rounds to get the hang of it.'
#                    '\n\n\n\n\n\nPress any key when you are ready to give it a try!')
    intro_9 = visual.TextStim(
        window, text=intro_text_9, wrapWidth=1.85, font=font, height = font_height)
    intro_9.draw()
    window.flip()
    event.waitKeys()



def intro_exp(window, prefmri=False, fmri=False):
    '''Displays the between training-and-exp information'''

    imagefolder = '../Images/'
    ss_size = (1.5, 1.5)
    ss_pos = (0, 0.3)

    if prefmri is True:
        intro_text_1 = ('Bald werden wir mit dem MRT-Scan beginnen.'
                        '\n\nDa diese Daten fuer uns am wichtigsten sind, wollen wir sicher sein,'
                        'dass Sie sich an die Aufgabe erinnern, die Sie gestern gespielt haben.'
                        '\n\nDas Spiel wird gleich bleiben. Wichtig ist: Es gibt keine Tricks !'
                        '\n\nDenken Sie daran, dass das Monster nur dann eine Chance hat zu kommen,'
                        'wenn Sie die riskante 9-Punkte Option wählen. Der in den oberen Ecken gezeigte'
                        'Kontext (Schweinchen oder Monster) bestimmt die Wahrscheinlichkeit (niedrig oder hoch),'
                        'dass das Monster kommt. Es hängt von nichts anderem ab.'
                        '\n\n\nAlso zuerst ein paar Übungsrunden, und dann fangen wir mit dem Scannen an :)')
        
        intro_1 = visual.TextStim(window, text=intro_text_1, wrapWidth=1.85)
        intro_1.draw()
        window.flip()
        event.waitKeys()
        

    elif fmri is True:
        intro_text_1 = ('Ab jetzt beginnt das eigentliche Experiment!'
                        '\n\n\nEs dauert einige Zeit, bis wir Ihre Hirnsignale '
                        'mit unserem MRT messen können.'
                        '\n\nAlso bitte nicht überrascht sein, wenn diesmal '
                        'die Dinge etwas langsamer laufen!'
                        '\n\nAlles andere ist das Gleiche wie vorher. Keine Tricks !'
                        '\n\nBitte versuchen Sie, stillzuhalten.'
                        '\nWir sind sicher, dass Sie Ihr Bestes tun werden, '
                        'um so viele abgestimmte Punkte wie möglich zu '
                        'erhalten!')
        intro_1 = visual.TextStim(window, text=intro_text_1, wrapWidth=1.85)
        intro_1.draw()
        window.flip()
        event.waitKeys(keyList=('e'))
        
        
    else:
        intro_text_1 = ('Die Trainingsphase ist vorbei.'
                        '\n\n\nAb jetzt beginnt das eigentliche Experiment!')


        intro_1 = visual.TextStim(window, text=intro_text_1, wrapWidth=1.85)
        intro_1.draw()
        window.flip()
        event.waitKeys()
        
        intro_text_alpha = ('...Nur zur Erinnerung...'
                        '\n\n\nTaste C bedeutet 9 Punkte, mit einer Monsterwahrscheinlichkeit'
                        '\n\nTaste M bedeutet 1 Punkt von jeder Farbe, OHNE Monsterwahrscheinlichkeit')

        intro_alpha = visual.TextStim(window, text=intro_text_alpha, wrapWidth=1.85)
        intro_alpha.draw()
        window.flip()
        event.waitKeys()

    intro_text_2 = ('\nDrücken Sie eine beliebige Taste, um das Experiment zu '
                    'starten')

    intro_2 = visual.TextStim(window, text=intro_text_2, wrapWidth=1.85)
    intro_2.draw()
    window.flip()
    event.waitKeys()


def outro_text(window, fmri=False):
    '''This shows the outro text of the experiment'''
    if fmri is False:
        outro_text_1 = ('Sie haben es geschafft!'
                        '\n\nVielen Dank für Ihre Teilnahme!'
                        '\n\n\n\n\nBitte melden Sie sich bei der '
                        'Versuchsleiterin.')
    else:
        outro_text_1 = ('Großartig, Danke!'
                        '\n\nAls nächstes sehen Sie einige bekannte Kontexte. '
                        'Es gibt nichts zu tun, außer sie anzuschauen.'
                        '\n\nHalten Sie durch; das Experiment ist fast vorbei! '
                        ':)')

    outro = visual.TextStim(window, text=outro_text_1,
                            pos=(0.0, 0.4), wrapWidth=1.8)
    outro.draw()
    window.flip()
    event.waitKeys(keyList=('x'))


def intro_shady(window):
    """Intro to the shady business.

    """
    outro_text_1 = ('Großartig, Danke!'
                    '\n\nAls nächstes sehen Sie einige bekannte Kontexte. '
                    '\n\nEs gibt nichts zu tun, außer sie anzuschauen.'
                    '\n\nHalten Sie durch; das Experiment ist fast vorbei! '
                    ':) \n\nDrücke eine beliebige Taste, um fortzufahren.')

    outro = visual.TextStim(window, text=outro_text_1,
                            pos=(0.0, 0.4), wrapWidth=1.8)
    outro.draw()
    window.flip()
    event.waitKeys()


def fmri_outro(window):
    '''This shows the outro text of the fmri experiment'''
    outro_text_2 = ('Sie haben es geschafft!'
                    '\n\nVielen Dank für Ihre Teilnahme!'
                    '\n\n\nBitte bleiben Sie noch eine Minute ruhig liegen,'
                    'wir sind gleich fertig und holen Sie schnell aus dem MRT')
        

                    # '\n\n\n\n\nWarten Sie noch kurz, wir holen Sie so schnell '
                    # 'wie möglich da raus...')

    outro = visual.TextStim(window, text=outro_text_2,
                            pos=(0.0, 0.4), wrapWidth=1.8)
    outro.draw()
    window.flip()
    event.waitKeys(keyList=('x'))


def get_transitions(num_mblocks, weights, num_contexts=4):
    """Assumes that there are four contexts and returns --num_trials--
    indices which follow the --weights-- to determine which transitions
    are to be observed how often.

    --weights-- should have 4**2 entries, each referring to a possible
    transition between contexts. The order should be that given by
    itertools.product(contexts, contexts).

    Parameters
    ----------
    num_mblocks : int
    Number of miniblocks in the experiment. Conversely, number of indices
    to return.

    weights : np.1darray of ints
    Weights to be given to the different transitions. They need not sum to 1.
    The order of the transitions is [[1, 1], [1, 2], ..., [2, 1], [2, 2], ...].
    If the number of elements is less than 16, it will be padded with zeros
    at the end.

    num_contexts : int
    Number of contexts in the experiment. Defaults to the current 4.

    Returns
    -------
    contexts : np.1darray
    Vector with as many elements as --num_mblocks-- with the indices of the
    contexts.

    deviation : int
    Total deviation of the observed transitions in --contexts-- from the
    desired transition distribution in --weights--, calculated as the
    sum of the difference between desired and observed for all possible
    transitions.

    obs_transitions : dictionary
    Dictionary whose keys are all the possible transitions and whose values
    are the times that said transition was observed in --contexts--.
    """
    # Build all 16 transitions:
    possible_trans = list(it.product(np.arange(num_contexts),
                                     np.arange(num_contexts)))

    # Repeat each transition as many tines as --weights-- says
    if len(weights) < 16:
        pad = 16 - len(weights)
        weights = np.pad(weights, (0, pad), mode='constant', constant_values=0)
    if sum(weights) < num_mblocks:
        multiply_by = np.ceil(num_mblocks/sum(weights))
        weights = (weights * multiply_by).astype(int)

    all_transitions = np.zeros((sum(weights), 2))
    cum_index = 0

    for ix_weight, weight in enumerate(weights):
        all_transitions[cum_index:(cum_index + weight),
                        :] = possible_trans[ix_weight]
        cum_index += weight

    # Now randomly build a sequence of contexts by happily sampling
    # from all_transitions and hoping for the best because the best
    # always happens to good people and we're the best of them. Well,
    # at least not the worst.
    flag_failed = 0

    init_index = np.random.choice(np.arange(len(all_transitions)))
    contexts = [*all_transitions[init_index, :]]
    all_transitions[init_index, 0] = -1
    while sum(all_transitions[:, 0] > 0) > 0:
        next_context = contexts[-1]
        index_cand = (all_transitions[:, 0] == next_context).nonzero()[0]
        if len(index_cand) == 0:
            flag_failed += 1
            index_cand = (all_transitions[:, 0] >= 0).nonzero()[0]
        candidates = all_transitions[index_cand, :]
        rng_index = np.random.choice(np.arange(candidates.shape[0]))
        contexts.append(candidates[rng_index][-1])
        all_transitions[index_cand[rng_index]] = -1
    contexts = np.array(contexts, dtype=np.int64)
    contexts = contexts[:num_mblocks]  # Only take the first num_mblocks
    deviation, obs_transitions = evaluate_contexts(contexts,
                                                   possible_trans,
                                                   weights)

    return contexts, deviation, obs_transitions


def evaluate_contexts(contexts, possible_trans, weights):
    """Evaluates whether the transitions in --contexts-- are close
    enought to --weights--. Returns the deviation of the observed
    transitions in --contexts-- to the desired transitions in
    --weights--.
    """
    observed_transitions = {tuple(one_trans): 0 for one_trans in
                            possible_trans}
    desired_transitions = {tuple(one_trans): weight for one_trans,
                           weight in zip(possible_trans, weights)}
    # Calculate observed transitions
    for idx in range(len(contexts) - 1):
        observed_transitions[(contexts[idx], contexts[idx + 1])] += 1
    total_deviation = 0
    for key in observed_transitions:
        total_deviation += abs(observed_transitions[key] -
                               desired_transitions[key])
    return total_deviation, observed_transitions


def close_fun(window):
    window.close()
    core.quit()


def pause_fun(window):
    fix_cross = shade.get_fixation_stim(window)

    print('Pause!')
    fix_cross.draw()
    window.flip()
    event.waitKeys(keyList=['q'])
    print('Depausify!')


def save_slice_time(timings=TIMINGS):
    """Global catch for the fmri trigger. Saves when the trigger was
    sent by the fmri computer in the global dictionary TIMINGS.

    """
    c_epoch = time.time()
    c_time = time.strftime("%H%M%S", time.localtime())
    timings['slice_time'].append(c_time)
    timings['slice_epoch'].append(c_epoch)


def global_keys(window):
    """Sets up the global kill keys."""
    event.globalKeys.remove('all')
    event.globalKeys.add(key='escape', func=close_fun, func_args=[window],
                         name='shutdown', modifiers=['shift'])
    event.globalKeys.add(key='p', func=pause_fun, func_args=[window],
                         name='pause')
    event.globalKeys.add(key=FMRI_TRIGGER, func=save_slice_time,
                         name='slice times')


if __name__ == '__main__':
    PARSER = argparse.ArgumentParser(
        description='Start the experiment and input the subject number')
    PARSER.add_argument('-s', '--subject', dest='subject_id', default=None,
                        help='Subject Identifier. Consistency is paramount!')
    PARSER.add_argument('--notrain', dest='notrain', action='store_true',
                        help='Pass this argument to skip training')
    PARSER.add_argument('--noexp', dest='noexp', action='store_true',
                        help='Pass this argument to skip the experiment')
    PARSER.add_argument('--small', dest='small', action='store_true',
                        help='Pass this argument to run in a small screen')
    PARSER.add_argument('--debug', dest='debug', action='store_true',
                        help='Enables debug mode, with fewer and shorter mblocks')
    PARSER.add_argument('--fast', dest='fast', action='store_true',
                        help='Enables fast mode, with shorter pauses')
    PARSER.add_argument('--fmri', dest='fmri', action='store_true',
                        help='Runs the fmri version of the experiment')
    PARSER.add_argument('--behavioural', dest='behavioural', action='store_true',
                        help='Behavioural-only version')
    PARSER.add_argument('--prefmri', dest='prefmri', action='store_true',
                        help='Short behavioural right before fmri')
    PARSER.add_argument('--noshade', dest='noshade', action='store_true',
                        help='Skips shaddy business')
    ARGS = PARSER.parse_args()
    FAST = ARGS.fast
    DEBUG = ARGS.debug
    if ARGS.prefmri:
        ARGS.notrain = True
    which_experiment(ARGS)
