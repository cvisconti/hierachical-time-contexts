
"""
This is the shady file that will simply show all the different pairs of sequences in order
for the fmri version of the experiment. So that i can maybe do some RSA. And save my ass.

Shady.

"""
import itertools as it
import time 

import numpy as np
from psychopy import core, visual

from miniblock import drawObjects

img_folder = '../Images/'



def when_is_it():
    '''Sets some stuff up so I can later figure out when things happened'''
    
    t = time.localtime()
    it_is_now = time.strftime("%H%M%S", t)
    
    return it_is_now
    

def parameters():
    ''' Sets up some nice parameters for future fuck uppery

    '''

    dick = {'iti': 7,
            'stim_pres': 6,
            'num_pairs': 16,
            'context_size': 0.62,
            'fix_size':0.5,
            'context_y': 0.56,
            'random_order': False,  # Randomize order of trannies?
            }

    return dick

def run_shady(window=None):
    """Runs all screens, one for every type of transition observable,
    16 in total. The timings and other things are taken from
    parameters().

    """
    if window is None:
        window = visual.Window()

    dick = parameters()
    screens = image_dicks(dick, window)
    
    
    stimulus_onset = []
    
    fixation_onset = []

    num_mblock_types = 4
    mblock_types = np.arange(num_mblock_types)
    all_trannies = list(it.product(mblock_types, mblock_types))
    if dick['random_order']:
        all_trannies = np.random.permutation(all_trannies)
    else:
        # Use some preselected order
        order = order = np.array([14, 10,  5, 13,  3, 12,  0,  6,
                                  9,  1,  2,  8,  4, 11, 15,  7])
        all_trannies = np.array(all_trannies)[order, :]

    for trannie in all_trannies:
        screens[0].update_curr_mblock(trannie[0])
        screens[0].update_next_mblock(trannie[1])
        screens[0].draw()
        window.flip()
        stimulus_onset.append(when_is_it()) 
        core.wait(dick['stim_pres'])
        screens[1].draw()
        window.flip()
        fixation_onset.append(when_is_it()) 
        core.wait(dick['iti'])
        
    all_timings = {'stim_onset' : np.array(stimulus_onset),
                   'fix_onset'  : np.array(fixation_onset)}
    
    return all_timings

def image_dicks(dick, window):
    """Creates the images (next_mblock and curr_mblock) as psychopy
    objects. Transforms them into a drawObjects.

    """

    all_contexts = ['risky_red', 'safe_red', 'risky_blue', 'safe_blue']
    dict_contexts_n = {name: visual.ImageStim(window,
                                              image=img_folder + name + '.png',
                                              size=(dick['context_size'],
                                                    dick['context_size']),
                                              pos=(0.66, dick['context_y']))
                       for name in all_contexts}
    dict_contexts_c = {name: visual.ImageStim(window,
                                              image=img_folder + name + '.png',
                                              size=(dick['context_size'],
                                                    dick['context_size']),
                                              pos=(-0.66, dick['context_y']))
                       for name in all_contexts}
                                            
    screen_fixation = get_fixation_stim(window)

    curr_contexts = drawObjects(dict_contexts_c)
    next_contexts = drawObjects(dict_contexts_n)

    
    screen_contexts = drawObjects({'curr_mblock': curr_contexts,
                                   'next_mblock': next_contexts})
    
    return screen_contexts, screen_fixation


    
def get_fixation_stim(window, back_color=(0, 0, 0), stim_color=(1, 1, 1)):
    """Provide objects to represent a fixation stimulus as in [1]_.
    Parameters
    ----------
    win : psychopy.visual.Window
    The psychopy window on which to draw the fixation stimulus.
    back_color : tuple
    Color of the background (-1=black, 0=gray, 1=white)
    stim_color : tuple
    Color of the stimulus (-1=black, 0=gray, 1=white)
    Returns
    -------
    outer, inner, horz, vert : tuple of objects
    The objects that make up the fixation stimulus.
    References
    ----------
    .. [1] Thaler, L., Schütz, A. C., Goodale, M. A., & Gegenfurtner, K. R.
    (2013). What is the best fixation target? The effect of target shape on
    stability of fixational eye movements. Vision Research, 76, 31-42.
    https://www.doi.org/10.1016/j.visres.2012.10.012
    """
    # diameter outer circle = 0.6 degrees
    # diameter circle = 0.2 degrees
#    outer = visual.Circle(win=window, radius=30, fillColor=stim_color, lineColor=back_color, units='pix')
#    inner = visual.Circle(win=window,radius=15, fillColor=stim_color, lineColor=stim_color, units='pix')
#    horz = visual.Rect(win=window, width=0.6, height=0.2, fillColor=back_color, lineColor=back_color)
#    vert = visual.Rect(win=window, width=0.2, height=0.6, fillColor=back_color, lineColor=back_color)
#    
#    fixation_cross = drawObjects([outer,horz,vert, inner])
    win_size = window.size
    stim_size = (0.3, win_size[0] / win_size[1] * 0.3)
    fixation_cross = visual.ImageStim(window, image=img_folder + 'cross.png', size=stim_size)
    return fixation_cross

def set_fixstim_color(stim, color):
    """Set the fill and line color of a stim."""
    stim.setFillColor(color)
    stim.setLineColor(color)
    return stim    
    
if __name__ == '__main__':
    run_shady()
