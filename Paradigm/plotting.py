#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
These are some functions that plot and visualise aspects of the task
"""

import numpy as np
import matplotlib.pyplot as plt

import fullexperiment as fart


def draw_some_contexts(num_mblocks, repeats, weights=None,
                       color_repeat_cutoff=4, no_trans=False):
    '''Creates some versions of context ordering (miniblock structure) and
    outputs a nice visualisation of them, so you can easily choose
    which one to select for the experiment.

    NB : Plots go DOWN THE COLUMNS not accross the rows ! (I THINK THIS
    IS WRONG)

    '''
    if weights is None:
        weights = fart.pump_weights()

    list_contexts = []
    subplots = calc_subplots(repeats)
    good_repeats = 0
    while good_repeats < repeats:
        if no_trans:
            contexts = np.random.permutation(np.repeat(np.arange(4),
                                                       np.ceil(num_mblocks
                                                               / 4)))
        else:
            contexts = fart.get_transitions(num_mblocks, weights)[0]
        flag_continue = 0
        colors = contexts % 2
        rolled_colors = [np.roll(colors, - ix_roll)
                         for ix_roll in range(color_repeat_cutoff)]
        for matches in zip(*rolled_colors):
            if np.all(np.array(matches) == matches[0]):
                flag_continue = 1
                continue
        if flag_continue:
            continue
        contexts = np.array(contexts).reshape(calc_subplots(num_mblocks))
        list_contexts.append(contexts)

        ax = plt.subplot(*subplots, good_repeats + 1)
        plt.title(good_repeats)
        hinton(contexts, None, ax=ax)
        good_repeats += 1

    return list_contexts


def calc_subplots(num_plots):
    """ Calculates a good arrangement for the subplots given the number of
    subplots. Made by Dario.
    """
    if num_plots == 2 or num_plots == 3:
        return num_plots, 1

    sqrtns = np.sqrt(num_plots)
    if abs(sqrtns - np.ceil(sqrtns)) < 0.001:
        a1 = a2 = np.ceil(sqrtns)
    else:
        divs = num_plots % np.arange(2, num_plots)
        divs = np.arange(2, num_plots)[divs == 0]
        if divs.size == 0:
            return calc_subplots(num_plots + 1)
        else:
            a1 = divs[np.ceil(len(divs) / 2).astype(int)]
            a2 = num_plots / a1
    return int(a1), int(a2)


def hinton(contexts, max_weight=None, ax=None):
    """Draw Hinton diagram for visualizing a weight matrix."""

    ax = ax if ax is not None else plt.gca()

    if not max_weight:
        max_weight = 2 ** np.ceil(np.log(np.abs(contexts).max()) / np.log(2))

    ax.patch.set_facecolor('gray')
    ax.set_aspect('equal', 'box')
    ax.xaxis.set_major_locator(plt.NullLocator())
    ax.yaxis.set_major_locator(plt.NullLocator())

    for (y, x), w in np.ndenumerate(contexts):
        #        color = 'blue' if w > 0 else 'red'
        #        size = np.sqrt(np.abs(w) / max_weight)
        if w == 0:
            color = 'red'
            size = np.sqrt(1 / max_weight)
        elif w == 1:
            color = 'blue'
            size = np.sqrt(1 / max_weight)
        elif w == 2:
            color = 'red'
            size = np.sqrt(0.3 / max_weight)
        elif w == 3:
            color = 'blue'
            size = np.sqrt(0.3 / max_weight)
        rect = plt.Rectangle([x - size / 2, y - size / 2], size, size,
                             facecolor=color, edgecolor=color)
        ax.add_patch(rect)

    ax.autoscale_view()
    ax.invert_yaxis()


if __name__ == '__main__':
    # Fixing random state for reproducibility
    np.random.seed(19680801)

    hinton(np.random.rand(20, 20) - 0.5)
    plt.show()
