#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Sep 10 14:04:37 2019

@author: visconti
"""
import numpy as np
import pickle
import itertools as it

class theGame(object):
    
    def __init__(self,risky_offers=None,contexts=None,num_trials=None):
        
        if risky_offers is None:
            self.risky_offers = np.arange(5,9)
        else:
            self.risky_offers = risky_offers
            
        if contexts is None:
            with open('./Paradigm/contexts.pi','rb') as dumpy:
                self.contexts = pickle.load(dumpy)
        else:
            self.contexts = contexts
        
        if num_trials is None:
            self.num_trials = 6
        else:
            self.num_trials = num_trials 
        
        self.num_mblocks = len(self.contexts)
        
        self.safe_offer  = np.array([1,1])
        
        self.monster_prob_safe  = np.array([.35,.65])
        self.monster_prob_risky = np.array([.65,.35])
        self.num_colors         = 2
        self.max_offer          = self.risky_offers.max()
    
    def play_one_game(self):
        """Goes through all trials of every miniblock calling the method called
        make_decision() (defined in the subclasses) to make decisions
        
        Inputs
        ______"""
        
        points = np.array([0,0])
        log_points = np.zeros((self.num_mblocks,self.num_trials,2))
        
        for ix_mblock in range(self.num_mblocks):
            curr_context = self.contexts[ix_mblock]
            if ix_mblock+1 == self.num_mblocks:
                next_context = 0
            else:
                next_context = self.contexts[ix_mblock+1]
            
        
            for ix_trial in range(self.num_trials):
                risky_offer = self.sample_risky_offer(curr_context)
                decision    = self.make_decision(curr_context,next_context,risky_offer,self.safe_offer)
                monstie     = self.sample_monster(decision,curr_context)
                points      = self.sample_next_points(monstie,decision,risky_offer,points)
                
                log_points[ix_mblock,ix_trial,:]  = points
                
        return log_points
    
    def points_safe(self,points):
        """From unsafe or total points caluclates the amount of safe points that agent has"""
        
        safe_points = points.min()
        
        return safe_points
    
    def sample_risky_offer(self,curr_context):
        
        if curr_context%2 == 0:
            risky_offer = np.array((np.random.choice(self.risky_offers),0))
        else:
            risky_offer = np.array((0,np.random.choice(self.risky_offers)))
        
        return risky_offer

    def sample_monster(self,decision,curr_context):
        
        if decision == 1:
            monstie = 0
            
        elif decision == 0 and curr_context <= 1:
            monstie = np.random.choice((0,1),p=(self.monster_prob_risky))
        
        elif decision == 0 and curr_context >= 2:
            monstie = np.random.choice((0,1),p=(self.monster_prob_safe))
        
        return monstie
    
    def sample_next_points(self,monstie,decision,risky_offer,points):
        
        if decision == 1:
            points = points+self.safe_offer
        elif monstie == 1:
            points = points.min()*np.ones(2)
        else:
            points = points+risky_offer
        
        return points
        
        
class dumbAgent(theGame):
    
    def __init__(self,*args,**kwargs):
        #   super(theGame, self).__init__(*args,**kwargs)
        super().__init__(*args, **kwargs)
    def make_decision(self,*args,**kwargs):
        return np.random.choice((0,1))
    
class forwardPlanning(theGame):
    
    def __init__(self,*args,**kwargs):
        
        super().__init__(*args,**kwargs)
        
        self.num_actions = 2
        self.max_points  = int((self.num_trials*2*self.max_offer/3))
        self.trans_mat   = get_transition_matrix(offer)
        self.all_offers  = [(1,1),(0,5),(0,6),(0,7),(0,8),(5,0),(6,0),(7,0),(8,0)]
        self.average_offers = [(0,7),(7,0)]
        self.num_states  = self.max_points**2
        
        initial_state      = np.zeros(self.num_states)
        initial_state[0]   = 1
        self.initial_state = initial_state
        
        self.trans_mats_dick  = {}        
        
        self.trans_mats_average_dick  = {}
        
        for offer in self.all_offers:
            self.trans_mats_dick[offer] = get_transition_matrix(offer)
            
        for offer in self.average_offers:
            self.trans_mats_average_dick[offer] = get_transition_matrix(offer)
            
            
    def generate_action_sequences(self,curr_trial):
        
        act_sequences = it.product(range(self.num_actions),repeat=(self.num_trials*2)-curr_trial)
        act_sequences = np.array(list(act_sequences))
        
        return act_sequences
    
    def get_transition_matrix(self,offer):
        """Creates one transition matrix, for one offer
        Inputs
        ------
        offer : np.array 
        (red,blue)
        
        Outputs
        -------
        trans_mat : np.2darray 
        Transition Matrix for that offer """
        
        #Determine size of trans mat
        offer       = np.array(offer)
        num_colors  = self.num_colors
        
        max_points  = self.max_points
        matrix_size = max_points**num_colors
        
        trans_mat = np.zeros((matrix_size,matrix_size))
        
        cap         = np.zeros(num_colors,dtype=np.int64)
        
        for ix_col in range(matrix_size):
            
            c_points = np.unravel_index(ix_col, shape=(max_points, max_points))
            temp_universe = c_points + offer
            for ix in range(len(temp_universe)):
                cap[ix] = min(temp_universe[ix],max_points-1)
                
            ix_replace  = np.ravel_multi_index(cap,dims=(max_points,max_points))
            
            trans_mat[ix_replace,ix_col] = 1
        
        return trans_mat
        
    def do_transition(self, curr_state, trans_mat):
        """Performs a transition in two steps. First, it multiplies the current
        state vector by the corresponding transition matrix and then it 
        matches the points.
        
        Returns the new state vector, in which the points have been matched,
        as well as the number of matched points that resulted from this
        transition.
        
        Inputs
        ------
        trans_mat : np.2darray
        transition matrix as outputted by get_transition_matrix
        
        curr_state : np.1darray
        vector of probabilities of what state youre in
        """
        
        next_state = trans_mat.dot(curr_state)
        
        vector_a = np.arange(len(next_state))
        
        unmatched_pts = np.zeros(len(vector_a),dtype=np.int64)
        matched_pts   = np.zeros(len(vector_a),dtype=np.int64)
        
        for ix_a in vector_a:
            
            ipdb.set_trace()
            unraveled_pts = np.array(np.unravel_index(ix_a,
                                                      shape=(self.max_points,self.max_points)))
            
            matched_pts[ix_a] = unraveled_pts.min()
            
            temp_unmatched = unraveled_pts - unraveled_pts.min()
            
            unmatched_pts[ix_a] = np.ravel_multi_index(temp_unmatched, 
                            dims=(self.max_points,self.max_points))
#            
#            for ix_d in unmatched_pts:
#                temp_sum[ix_d] = curr_state[ix_a]+unmatched_pts[ix_d]
#            
#            add_to = temp_sum.sum()
        return matched_pts, unmatched_pts
    
        s_star = np.zeros(curr_state.shape)
        m_star = np.zeros(self.max_points)
        
        for i_x,s_x in enumerate(curr_state):
            m_x = matched_pts[i_x]
            j_x = int(unmatched_pts[i_x])
            
            s_star[j_x] += s_x
            m_star[m_x] += s_x
        
        return s_star, m_star
        
    def make_decision(self,act_sequences):
        
        for act_sequence in act_sequences:
            something = evaluate_ass(act_sequence)
            
        return something
            
    def evaluate_ass(self,curr_state,act_sequences):
        
        final_state = predict_final_state(curr_state)
        
        return final_state
        
    def predict_final_state(self,act_sequences,curr_state=None):
        
        if curr_state is None:
            curr_state = self.initial_state
        #Set initial current state
        
        all_states = [curr_state]
        
        
        for action in act_sequences:
            
            if act_sequences[action] == 0: 
                key = (0,0)
                all_states.append(self.do_transition(curr_state,self.trans_mats_dick[key]))
            elif action == 1 and self.contexts[action]%2 == 0:
                key = (0,7) # POST IT NOTE YOU'RE DANGEROUSLY GUESSING
                all_states.append(self.do_transition(curr_state,
                                self.trans_mats_average_dick[key])) 
                                #double check POST IT NOTE
            else :
                key = (7,0)
                all_states.append(self.do_transition(curr_state,
                                self.trans_mats_average_dick[key]))
            
        return all_states
    
    def expected_value(self,final_state):
        
        pass
        
        
