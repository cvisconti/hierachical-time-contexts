# -*- coding: utf-8 -*-

import itertools as it
import multiprocessing as mp
import os
import pickle
from time import time

import numpy as np

"""Agent skeleton. No longer contained to closet area. Alarm level
rised from 7 to Purple.  Mike knows what that means.

Notes:
(1) States are N**2-dimensional, where N is the maximum number of points
    we are considering. I think 30 is a good number.

(2) There are (R + 1) transition matrices, where R is the number of possible
    offers. However, we decided to make an agent (Florian) which only considers
    a sort of "average" offer in the future, so that one should be used
    to predict future states, except when predicting S_{t+1}, because in
    for that one Florian knows exactly what the offer is, so that transition
    matrix should be used.

(3) In the_game, all_matched is a number, because there is no uncertainty on
    how many points have been matched. The agents, when trying to predict
    future states, need to have uncertainty, so all_matched becomes a vector
    there. In principle, you could do the same with curr_state, and I didnt
    do it only because I hadn't thought of it and now it's too late, it's time
    for me to push the daisies.

"""


def the_game(agent_name='Eric', all_parameters_old=None, random_seed=None,
             agent_pars=None):
    """Main function to run the entire game, with many miniblocks, balloon
    animals and cocaine.

    Note that --curr_state-- refers mostly to unmatched points. Also
    note that both all_matched and curr_state persist through
    miniblocks. Also also note that all_matched is just a number. This
    is different from make_decision_florian() and other functions
    below, which need it to be a vector (probability distribution). If
    this makes you uncomfortable, you can call them different names,
    dress them with different colors and take them to different
    psychiatrists when you ruin their lives with your unrequitted love.
    
    Inputs : 
    agent_name : string 
    Eric, Florian, soft_threshold
    Specifies which type of agent to run
    
    agent_pars : list containing array like
    slopes and centres
    

    """
    np.random.seed(random_seed)

    pars = get_game_parameters(agent_name, all_parameters_old)
    
    initial_state = np.zeros(pars['point_cap']**2)
    initial_state[0] = 1
    
    log_state     = np.zeros((pars['num_mblocks'],pars['num_trials']))
    log_decision  = np.zeros((pars['num_mblocks'],pars['num_trials']))
    log_matched   = np.zeros((pars['num_mblocks'],pars['num_trials']))
    log_monster   = np.zeros((pars['num_mblocks'],pars['num_trials']))
    log_unmatched = np.zeros((pars['num_mblocks'],pars['num_trials']))
    log_uncolored = np.zeros((pars['num_mblocks'],pars['num_trials']), dtype=object)

    if agent_name == 'soft_threshold':
        this_decision = _make_decision_soft_threshold(*agent_pars)
    else:
        this_decision = make_decision
    all_matched = 0
    curr_state = initial_state  # whatever this is
    for mblock in range(pars['num_mblocks']):
        curr_type, next_type = get_mblock_type(mblock)
        for trial in range(pars['num_trials']):
            # print('Mblock: ', mblock, '  Trial: ', trial)
            index_unmatched = np.where(curr_state==1)[0]
            log_state[mblock, trial]       = index_unmatched  # Save state first
            unmatched_unpacked= np.unravel_index(index_unmatched,
                                                 (pars['point_cap'],
                                                  pars['point_cap']))
            unmatched_amount = max(unmatched_unpacked) - min(unmatched_unpacked)
            unmatched_color = ['red', 'blue'][max(unmatched_amount) % 2]
            log_unmatched[mblock, trial] = unmatched_amount
            log_uncolored[mblock, trial] = unmatched_color
            risky_offer                    = sample_offer(curr_type,pars['all_risky_offers'])
            log_matched[mblock,trial]      = all_matched
            decision = this_decision(agent_name=agent_name,
                                     trial=trial,
                                     risky_offer=risky_offer,
                                     curr_type=curr_type,
                                     next_type=next_type,
                                     curr_state=curr_state,
                                     pars=pars,
                                     agent_pars=agent_pars)
            log_decision[mblock,trial]     = decision
            monster                        = sample_monster(decision,
                                                            curr_type,pars['monster_prob_safe'],
                                                            pars['monster_prob_risky'])
            log_monster[mblock,trial]      = monster
            curr_state, matched_points     = sample_next_state(decision,
                                                               curr_state,
                                                               risky_offer,
                                                               monster)
            all_matched = all_matched + matched_points
            
    all_logs      = {'state'          : log_state, 
                     'decisions'      : log_decision,
                     'final_m_pts'    : all_matched,
                     'matched'        : log_matched,
                     'monster'        : log_monster,
                     'unmatched'      : log_unmatched,
                     'unmatched_color': log_uncolored
                     }
    
    #all_logs['pars'] = pars

    return all_logs, pars


def get_game_parameters(agent_name, all_parameters_old=None):
    """Function where all game parameters are stored. In a dictionary, for
    example."""
    if all_parameters_old is None:
        all_parameters_old = {}

    with open('./Paradigm/contexts_behavioural.pi','rb') as dumpy:
        contexts = pickle.load(dumpy)

    all_parameters = { 'monster_prob_safe'  : np.array([.35,.65]),
                       'monster_prob_risky' : np.array([.65,.35]),
                       'all_contexts'       : contexts,
                       'num_mblocks'        : len(contexts),
                       'safe_offer'         : np.array([1,1]),
                       'all_risky_offers'   : np.arange(7, 11),
                       'max_risky_offer'    : 11,
                       'point_cap'          : 60,
                       'num_trials'         : 6,
                       'num_colors'         : 2,
                       'num_actions'        : 2,
                       'av_red'             : np.array([9,0]),
                       'av_blue'            : np.array([0,9]),
                       }
    all_parameters.update(all_parameters_old)
    trans_mats, avg_red, avg_blue = _create_all_trans_mats(all_parameters)

    all_parameters['av_red'] = avg_red
    all_parameters['av_blue'] = avg_blue
    all_parameters['trans_mats'] = trans_mats
    all_parameters['max_risky_offer'] = all_parameters['all_risky_offers'].max()
    all_parameters.update(all_parameters_old)

    all_parameters['agent_name'] = agent_name

    return all_parameters


def _create_all_trans_mats(pars):
    """Creates all transition matrices that will be needed.

    For the safe option and all the "current" transition matrices,
    the keys are the offers themselves, like (6, 0) and (1, 1).
    For the average ones, the keys are the index of the miniblock
    type, from 0 to 3, for redRisky, blueRisky, redSafe, blueSafe.
    CHECK THIS ORDER, AS I HAVE NO POSTIT.

    Returns
    -------
    trans_mats : dictionary
    Dictionary whose keys are the different offers in the format
    (red, blue) (as a tuple). This format applies to all transition
    matrices, regardless of whether they are safe, risky or yo mamma.

    avg_red/blue : tuple
    Each one represents a risky offer, in the order red then blue.

    """
    trans_mats = {}
    for risky_offer in pars['all_risky_offers']:
        offer = (risky_offer, 0)
        trans_mats[offer] = get_transition_matrix(offer,
                                                  pars['point_cap'],
                                                  pars['num_colors'])
        offer = (0, risky_offer)
        trans_mats[offer] = get_transition_matrix(offer,
                                                  pars['point_cap'],
                                                  pars['num_colors'])
    trans_mats[(1, 1)] = get_transition_matrix(np.array((1, 1)),
                                               pars['point_cap'],
                                               pars['num_colors'])
    
    avg_risky_blue = tuple(np.ceil((1  - pars['monster_prob_risky'])
                                   * pars['av_blue']).astype(int))
    avg_safe_blue = tuple(np.ceil((1  - pars['monster_prob_safe'])
                                  * pars['av_blue']).astype(int))
    avg_risky_red = tuple(np.ceil((1 - pars['monster_prob_risky'])
                                  * pars['av_red']).astype(int))
    avg_safe_red = tuple(np.ceil((1 - pars['monster_prob_safe'])
                                  * pars['av_red']).astype(int))
    
    trans_mats[0] = get_transition_matrix(avg_risky_red,
                                          pars['point_cap'],
                                          pars['num_colors'])
    trans_mats[1] = get_transition_matrix(avg_risky_blue,
                                          pars['point_cap'],
                                          pars['num_colors'])
    trans_mats[2] = get_transition_matrix(avg_safe_red,
                                          pars['point_cap'],
                                          pars['num_colors'])
    trans_mats[3] = get_transition_matrix(avg_safe_blue,
                                          pars['point_cap'],
                                          pars['num_colors'])

    return trans_mats, avg_risky_red, avg_risky_blue


def get_mblock_type(mblock):
    """Function that knows which mini-block type to return for every
    --mblock--, where --mblock-- can be assumed to be an index for
    simplicity. This doesn't actually need to be a function, but could
    be simply a vector loaded from a file.
    """
    
    # This is probably a really inefficent way of doing this because
    # one reloads the stupid contexts every time. But maybe it is not a problem ?
    
    with open('./Paradigm/contexts_behavioural.pi','rb') as dumpy:
        contexts = pickle.load(dumpy)
    
    curr_type = contexts[mblock] 
    
    if mblock+1>len(contexts)-1:
        next_type = contexts[0]
    else:
        next_type = contexts[mblock+1]
  
    return curr_type, next_type


def sample_offer(curr_type,all_risky_offers):
    """Function that takes some contextual information (like curr_type)
    and returns a risky offer. Preferably in the form (red, blue), as
    we have been doing so far.
    """
    
    # all_risky_offers = pars['all_risky_offers']
    
    if curr_type%2 == 0:
        risky_offer = np.array((np.random.choice(all_risky_offers),0))
    else:
        risky_offer = np.array((0,np.random.choice(all_risky_offers)))
        
    return risky_offer


def sample_monster(decision,curr_type,monster_prob_safe,monster_prob_risky):
    """Samples Monster"""

    if decision == 1:
        monster = 0
        
    elif decision == 0 and curr_type <= 1:
        monster = np.random.choice((0,1),p=(monster_prob_risky))
    
    elif decision == 0 and curr_type >= 2:
        monster = np.random.choice((0,1),p=(monster_prob_safe))
    
    return monster
        

def sample_next_state(decision, curr_state, risky_offer, monster):
    """Returns what the new state is, given --decision-- and
    --curr_state--. If the state space is just (blue, red), then
    it returns (blue, red) + (accepted_blue, accepted_red).
    
    It additionally returns an int of matched_points
    """
   
    cup_size = np.sqrt(len(curr_state)).astype(int)
    curr_index      = np.where(curr_state==1)[0]
    
    curr_state = np.array(np.unravel_index(curr_index,(cup_size,cup_size))).squeeze()
    
    if decision == 1:
        new_state = curr_state+1
    elif decision == 0 and monster == 0:
        new_state = curr_state + risky_offer
    elif decision == 0 and monster == 1:
        new_state = curr_state.min()*np.ones(2,dtype=int)
    
    matched_points = new_state.min() 
    
    new_state = new_state-matched_points    
    new_state = np.minimum(new_state,(cup_size-1,cup_size-1))
    
    new_ix = np.ravel_multi_index(new_state,(cup_size,cup_size))
    
    new_state         = np.zeros(cup_size**2)
    new_state[new_ix] = 1
    
    return new_state, matched_points

def make_decision(agent_name, *args, **kwargs):
    """This is where the agent lives. The inputs should be everything
    that the agent needs in order to make a decision for a single trial.
    The output should be that decision, either as a boolean (0 == risky,
    1 == safe) or as a string. Whatever it is, it needs to be consistent
    with sample_next_state().

    --agent_name-- should would be a string which tells the function
    which agent to use. For now, we have things like Eric and Florian.
    """

    if agent_name == 'Eric':
        return make_decision_eric(*args, **kwargs)
    elif agent_name == 'Florian':
        return make_decision_florian(*args, **kwargs)
    else:
        raise ValueError('Yo, what the fuck!')


def make_decision_eric(agent_pars=(0.5, 0.5), **kwargs):
    """Makes a single 50/50 decision."""

    decision = np.random.choice([0, 1], p=agent_pars)

    return decision


def make_decision_florian(curr_trial, risky_offer, curr_type, next_type,
                          curr_state, pars, **kwargs):
    """This is the function we have been working on the past few days.
    Takes as input all the information Florian needs to make a choice
    for a single trial and returns the choice.
    
    """
   
    all_pies = get_action_sequences(curr_trial,pars['num_trials'],
                                    pars['num_actions'])
    
    pie_eval = np.zeros(all_pies.shape[0])
    
    # # Calculate the expected value of each for each case
    # prob_safe  = pars['monster_prob_safe'][0] 
    # prob_risky = pars['monster_prob_risky'][0]
    
    # exp_val_safe   = pars['av_red'][0]*(1-prob_safe)
    # exp_val_risky  = pars['av_red'][0]*(1-prob_risky)
    
    # av_red_risky   = (exp_val_risky,0)
    # av_blue_risky  = (0,exp_val_risky)
    # av_red_safe    = (exp_val_safe,0)
    # av_blue_safe   = (0,exp_val_safe)
    
    # the_postit     = [av_red_risky,av_blue_risky,av_red_safe,av_blue_safe]
    
    all_trans_mats = {'curr'         : pars['trans_mats'][tuple(risky_offer)],
                      'curr_average' : pars['trans_mats'][curr_type],
                      'next_average' : pars['trans_mats'][next_type],
                      'safe'         : pars['trans_mats'][(1, 1)]
                      }
 
    for ix_pie, pie in enumerate(all_pies):
        predicted_unmatched, predicted_matched = \
            predict_final_state_florian(pie, curr_state, pars['num_trials'],
                                        all_trans_mats, pars['point_cap'],curr_trial,curr_type,next_type)
        pie_eval[ix_pie] = evaluate_state_florian(predicted_unmatched,
                                                  predicted_matched)

    ix_best_pie = choose_best_pie_florian(pie_eval)

    return all_pies[ix_best_pie,0]   # Returns the first action of the best pie


def _make_decision_soft_threshold(centers, slopes):
    """Returns a function make_decision_soft_threshold() which can be used
    in the_game() as any other agent. This is done because this agent
    needs parameters, and by golly I'm gonna give it to him!

    The size of --centers-- and --slopes-- determines the type of agent. If
    each has one element, it is assumed that the type of the next miniblock
    is ignored and a single sigmoid is used for all trials. If the inputs
    have 8 elements, the adaptive agent is built, which has a different
    sigmoid depending on the transition type.

    Parameters
    ----------
    centers : array-like
    Determines the center of the (inverted) sigmoid used to make decisions.
    Should be in units of unmatched points and its value determines the
    number of earned points for which the chance of accepting the unmatched
    offer is 0.5. Its size must be either 1 or 8, anything else means no spicy noodles.

    slopes : array-like
    Determines the slope at the center of the sigmoids. The size must match
    that of --centers-- or volcanos explode in white-people countries, which
    is a tragedy. 

    Returns
    -------
    make_decision_soft_threshold : function
    Function with inputs (blablabla) which can be used as a make_decision()
    function in the_game().

    """
    if len(centers) != len(slopes):
        raise ValueError('--centers-- and --slopes-- should have the same '
                         'number of elements, you dickwad')
    centers = np.array(centers)
    slopes = np.array(slopes)
    if centers.size == 1:
        centers = np.tile(centers, 8)
        slopes = np.tile(slopes, 8)
#    if not (slopes < 0).all():
#        raise ValueError('All slopes in --slopes-- should be negative.')

    # All transition types, as named in import_data.label_transitions().
    trannies = np.array([[0, 4, 1, 5],
                         [4, 0, 5, 1],
                         [2, 6, 3, 7],
                         [6, 2, 7, 3]])

    def make_decision(curr_type, next_type, curr_state, pars, **kwargs):
        """Makes a single decision given the inputs.
        If the agent can match points, it will. If not, uses sigmoid to
        decide.

        """
        c_trannie = trannies[curr_type, next_type]
        c_center = centers[c_trannie]
        c_slope = slopes[c_trannie]
        c_points = np.squeeze(np.nonzero(curr_state))
        c_points = np.unravel_index(c_points,
                                    (pars['point_cap'], pars['point_cap']))
        c_unmatched = max(c_points) - min(c_points)
        color = np.argmax(c_points) % 2
        if not color == curr_type % 2:
            return 0
        prob_safe = 1 / (1 + np.exp(c_slope * (c_unmatched - c_center)))
        return np.random.choice([0, 1], p=[1 - prob_safe, prob_safe])

    return make_decision


def get_action_sequences(curr_trial,num_trials,num_actions):
    """THIS FUNCTION HAS BEEN HIJACKED! OFFER NO RESISTANCE, BRING US ALL
    THE SANDWICHES AND PROCURE BIRCHES FOR US AND YOU MIGHT YET
    LIVE. THAT IS NOT A TYPO.

    """
    # Just call the _alt version, which is way cooler
    if num_actions == 2:
        return get_action_sequences_alt(curr_trial, num_trials)
    act_sequences = it.product(range(num_actions),repeat=(num_trials*2)-curr_trial)
    act_sequences = np.array(list(act_sequences))
    
    return act_sequences
    

def get_action_sequences_alt(curr_trial, num_trials):
    """Returns a bunch of action sequences. Each one of them has a
    different number of ones, because as I kept telling y'all, where
    exactly in the future you plan to take the risky offer doesn't
    matter, just how many times you will.

    """
    this_mblock = np.ones((num_trials - curr_trial, num_trials - curr_trial))
    this_mblock = np.tril(this_mblock, 0)
    this_mblock = np.vstack([np.zeros(num_trials - curr_trial), this_mblock])

    next_mblock = np.ones((num_trials, num_trials))
    next_mblock = np.tril(next_mblock, 0)
    next_mblock = np.vstack([np.zeros(num_trials), next_mblock])

    all_combinations_weird = list(it.product(this_mblock, next_mblock))
    all_combinations = np.zeros((len(all_combinations_weird),
                                 (2 * num_trials - curr_trial)))
    for ix_combo, one_combo in enumerate(all_combinations_weird):
        all_combinations[ix_combo, :] = np.concatenate(one_combo)
    return all_combinations


def get_transition_matrix(offer,max_points,num_colors):
    """Creates one transition matrix, for one offer
    Inputs
    ------
    offer : np.array 
    (red,blue)
    
    max_points : int
    from game parameters
    
    num_colors : int
    from game parameters
    
    Outputs
    -------
    trans_mat : np.2darray 
    Transition Matrix for that offer """
    
    #Determine size of trans mat
    offer       = np.array(offer)

    matrix_size = max_points**num_colors
    
    trans_mat = np.zeros((matrix_size,matrix_size))
    
    cap         = np.zeros(num_colors,dtype=np.int64)
    
    for ix_col in range(matrix_size):
        
        c_points = np.unravel_index(ix_col, dims=(max_points, max_points))
        temp_universe = c_points + offer
        for ix in range(len(temp_universe)):
            cap[ix] = min(temp_universe[ix],max_points-1)
            
        ix_replace  = np.ravel_multi_index(cap,dims=(max_points,max_points))
        
        trans_mat[ix_replace,ix_col] = 1
    
    return trans_mat

def predict_final_state_florian(pie, curr_state, num_trials, all_trans_mats, 
                                max_points, curr_trial, curr_type, next_type):
    """Takes the current state in --curr_state-- and predicts what
    the probability distribution over states will be after the
    entire action sequence --pie-- has been applied.

    If S_t = curr_state, then it predicts S_{t+1}, S_{t+2}, ...,
    S_{T}. It returns S_{T}.

    Note that --curr_state-- is an N-dimensional vector of floats,
    where N is the number of states. This vector represents a probability
    distribution over states. For example, if the agent is sure that
    he is in state number 3, --curr_state-- will be a bunch of zeros
    with a single 1 in position 3. If the agent has no fucking idea
    where he is, it'd be 1 / N * np.ones(N).

    Also, note that to limit computation times, the process of predicting
    S_{t+1} from S_t, given pi(t) has two steps: First multiply S_{t} by
    B[t], where B[t] is the transition matrix for action pi[t]; second match
    the points.

    This all means that the state S_t actually has two components: the
    unmatched points, (blue, red), and the matched points. If X is the
    maximum number of points we're considering (30 or so), then the
    size of the unmatched points vector is X**2 and the size of the
    matched points vector is X (could even be X / 2 if we want, but there's
    no point in keeping that one small).

    Note that this function should return both matched and unmatched,
    because both are necessary to evaluate how good an action sequence is.

    A final warning, for the end of times is nigh. Florian only considers
    an "average" future offer, because he doesn't know which offers will
    be offered in the future and doesn't want to think too much about it.
    So, B[action] is either the safe one or the "average" one, except when
    predicting S_{t+1}. This is because Florian knows what the current offer
    is, so he can use that transition matrix instead of the average one.

    This is sample next state in a for loop where the input is each
    thinigy in pie in other terms an action of an action sequence

    """
    next_step = curr_state
    all_matched = np.zeros(int(np.sqrt(len(curr_state))))
    
    for ix_trial, action in enumerate(pie):
        # First step
        # Select the appropriate transition matrix, which depends on --action--
        # and on whether it's the first action in pie or not. If it's the first
        # action, take the current offer; if not, take the average offer:
        
        if action == 0:
            if ix_trial == 0:
                c_trans_mat = all_trans_mats['curr']
            elif ix_trial <  num_trials - curr_trial:
                    c_trans_mat = all_trans_mats['curr_average']
            elif ix_trial >=  num_trials - curr_trial:
                    c_trans_mat = all_trans_mats['next_average']
        elif action == 1:
            c_trans_mat = all_trans_mats['safe']
            
        
        next_state, matched = do_transition(curr_state,c_trans_mat,max_points)
        
        # Now, add the new matched points in --matched-- to the previous
        # matched points and re-normalize, because it should be a
        # probability distribution:
        all_matched = all_matched + matched
        all_matched = all_matched / all_matched.sum()
        
    return next_state, all_matched

def do_transition(curr_state,trans_mat,max_points):
    """Performs a transition in two steps. First, it multiplies the current
    state vector by the corresponding transition matrix and then it 
    matches the points.
    
    
    Returns the new state vector, in which the points have been matched,
    as well as the number of matched points that resulted from this
    transition.
    
    Inputs
    ------
    trans_mat : np.2darray
    transition matrix as outputted by get_transition_matrix
    
    curr_state : np.1darray
    vector of probabilities of what state youre in
    """
    
    next_state = trans_mat.dot(curr_state)
    
    vector_a = np.arange(len(next_state))
    
    unmatched_pts = np.zeros(len(vector_a),dtype=np.int64)
    matched_pts   = np.zeros(len(vector_a),dtype=np.int64)
    
    for ix_a in vector_a:
        
        unraveled_pts = np.array(np.unravel_index(ix_a,
                                                  dims=(max_points,max_points)))
        
        matched_pts[ix_a] = unraveled_pts.min()
        
        temp_unmatched = unraveled_pts - unraveled_pts.min()
        
        unmatched_pts[ix_a] = np.ravel_multi_index(temp_unmatched, 
                        dims=(max_points,max_points))
#            
#            for ix_d in unmatched_pts:
#                temp_sum[ix_d] = curr_state[ix_a]+unmatched_pts[ix_d]
#            
#            add_to = temp_sum.sum()

    s_star = np.zeros(curr_state.shape)
    m_star = np.zeros(max_points)
    
    for i_x,s_x in enumerate(curr_state):
        m_x = matched_pts[i_x]
        j_x = int(unmatched_pts[i_x])
        
        s_star[j_x] += s_x
        m_star[m_x] += s_x
    
    return s_star, m_star # UNMATCHED POINTS, MATCHED POINTS, WHAT IS SUPPOOSED TO COME OUT HERE

def evaluate_state_florian(unmatched, matched):
    """Evaluates how good a state is in terms of number of points. For now,
    let's keep this simple by doing an expected average over points,
    counting matched points twice, as they represent both blue and red points.

    Returns a single number. The higher, the better.
    
    matched points
    np.1darray
    probabilities of having X points
    where X is the index of the array
    
    """
    
    expected_matched = np.sum(np.arange(len(matched))*matched)*2
    
    pre_a = np.array(np.unravel_index(np.arange(len(unmatched)),dims=(len(unmatched),len(unmatched))))
    
    unmatched_a = pre_a.sum(axis=0)
    
    expected_unmatched = np.sum(unmatched_a*unmatched)
    
    expected_total = expected_unmatched + expected_matched
    
    return expected_total


def choose_best_pie_florian(pie_eval):
    """Samples a pie, given the evaluations (predicted expected values) in
    --pie_eval--.

    This could be something as simple as choosing the one with the maximum
    value, but to add some variability, you can also run --pie_eval-- through
    a softmax function, which returns a probability distribution, and then
    sample from the distribution.
    """
    
    inv_temp = 1
    
    p = np.exp((pie_eval - pie_eval.max()) * inv_temp)
    p = p / sum(p)
    
    best_pie = np.random.choice(np.arange(len(p)),p=p)
    
    return best_pie


def parallel_game(name='Florian', iterations=1000, dicts=None):
    """Runs the_game() --iterations-- times in parallel."""
    my_robots = mp.Pool()
    dicks = create_dicks()

    num_seeds = len(dicks)*iterations

    nut_sack = np.random.randint(low=0,high=10000,size=num_seeds*10)
    nut_sack = np.unique(nut_sack)

    for ix_dick, dick in enumerate(dicks):
        for ix_iteration in np.arange(iterations):
            ix_nut = len(dicks) * ix_dick + ix_iteration
            my_robots.apply_async(the_game,
                                  [name, dick, nut_sack[ix_nut]],
                                  callback=save_posteriors)
            
    my_robots.close()
    my_robots.join()


def save_posteriors(to_save):
    """ whatever"""
    save_folder = './parallel_results/'
    os.makedirs(save_folder, exist_ok=True)
    identifier = to_save[1]['identifier']
    agent_name = to_save[1]['agent_name']
    with open(save_folder + '{}_{}_{}.pi'.format(agent_name, identifier, time()), 'wb') as dumpy:
        pickle.dump(to_save[0], dumpy)


def create_dicks():
    """Creates some dicks for Florian."""
    num_mblocks = 50
    first = {'num_mblocks': num_mblocks,
             'all_risky_offers': np.arange(5, 10),
             'point_cap': 10,  # 40,
             'av_red': np.array([7, 0]),
             'av_blue': np.array([0, 7]),
             'identifier': 'first'}
    second = {'num_mblocks': num_mblocks,
              'all_risky_offers': np.arange(7, 12),
              'point_cap': 10,  # 50,
              'av_red': np.array([10, 0]),
              'av_blue': np.array([0, 10]),
              'identifier': 'second'}
    third = {'num_mblocks': num_mblocks,
             'all_risky_offers': np.arange(9, 14),
             'point_cap': 10,  # 60,
             'av_red': np.array([12, 0]),
             'av_blue': np.array([0, 12]),
             'identifier': 'third'}
    fourth = {'num_mblocks': num_mblocks,
              'all_risky_offers': np.arange(5, 10),
              'point_cap': 10,  # 40,
              'av_red': np.array([7, 0]),
              'av_blue': np.array([0, 7]),
              'monster_prob_safe': np.array([0.25, 0.75]),
              'monster_prob_risky': np.array([0.75, 0.25]),
              'identifier': 'fourth'}
    fifth = {'num_mblocks': num_mblocks,
             'all_risky_offers': np.arange(7, 12),
             'point_cap': 10,  # 50,
             'av_red': np.array([10, 0]),
             'av_blue': np.array([0, 10]),
             'monster_prob_safe': np.array([0.25, 0.75]),
             'monster_prob_risky': np.array([0.75, 0.25]),
             'identifier': 'fifth'}
    sixth = {'num_mblocks': num_mblocks,
             'all_risky_offers': np.arange(9, 14),
             'point_cap': 10,  # 60,
             'av_red': np.array([12, 0]),
             'av_blue': np.array([0, 12]),
             'monster_prob_safe': np.array([0.25, 0.75]),
             'monster_prob_risky': np.array([0.75, 0.25]),
             'identifier': 'sixth'}

    return [first, second, third, fourth, fifth, sixth]


def the_pos_tit():
    """Just a remidner of what the postit says"""
    return ('red_risky', 'blue_risky', 'red_safe', 'blue_safe')
