#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
A nice collection of functions that give us stuff that we need to feed into
spm or freesurfer or whatever for imaging data.

"""
import glob
import pickle
from datetime import datetime
# import csv

import numpy as np
import pandas as pd
import import_data as imp
# import pydicom as dicom
# import dicom_parser as dp
from dicom_parser import Image
#import jsonpickle as jsonpickle
#import jsonpickle.ext.numpy as jsonpickle_numpy


def give_design_matrix():
    '''
    Inputs
    ------
    analysis_type ; string
    '''

    all_data = imp.import_data()

    participant_list = np.unique(all_data.index)

    all_design_matrices = {}

    for participant in participant_list:
        current_data = all_data.loc[participant]
        risky_points = current_data['risky_points']
        trans_type = current_data['trans_type']
        single_choice = current_data['choice']
        current_risk = -np.ones(len(trans_type))
        response_time = current_data['rts']

        for ix_trans, which_transition in enumerate(trans_type):
            if which_transition in [0, 1, 4, 5]:
                current_risk[ix_trans] = 0
            else:
                current_risk[ix_trans] = 1
            #current_risk[ix_trans] = which_transition in [2,3,6,7]

        current_matrix = np.stack(
            (risky_points, trans_type, single_choice, current_risk, response_time))

        all_design_matrices[participant] = current_matrix

    return all_design_matrices


def subtract_time():
    '''Timestamps were saved using the python time.time function, which was medium smart
    so the only thing this will do is subtract the first time from all of the other ones

    What I eventually want is a .txt file with display of time of each offer
    calculated as time since first display of first offer as one column

    also time that offer was taken, so this plus rts,
    this is called decision_made

    also all the other stuff in give_design_matrix()
    '''

    # Scrape out the wrong timings here
    # cond1_onsets = (all_fmri_data['experiment'][0]['decision_onset'])
    # first_value  = cond1_onsets[0][0]

    # cond2_onsets = (initial_data['experiment'][1]['decision_onset'])
    # shade_fix    = (initial_data['shade']['fix_onset'])
    # shade_stim   = (initial_data['shade']['stim_onset'])

    all_fmri_data = imp.import_data(filename='/data_fmri_{}.pi')
    #import pdb; pdb.set_trace()
    all_regressors = regressors_model_stuffies()

    all_regressors_panda = pd.DataFrame.from_dict(
        all_regressors, orient='index')

    participant_list = np.unique(all_fmri_data.index)

    # set up some containers for comparing the first onset time and the first shade stim
    acq_time_list_2 = []
    shade_fix_first = []

    study_time_all = []
    time_sync_all = []

    trial_cutoff = 120
    # The trial at which the data should be split into the two different runs

    for participant in participant_list:
        data = all_fmri_data.loc[participant]
      #  first_value  = data['decision_onset'][0]
        first_epoch = np.array(data['decision_onset_epoch'])[
            0]  # .round(decimals=3)
        second_epoch = np.array(data['decision_onset_epoch'])[119]
        rts = np.array(data['rts'])  # .round(decimals=3)
        choice = np.array(data['choice'])  # .round(decimals=0)
        risky_points = np.array(data['risky_points'])  # .round()
        trans_type = np.array(data['trans_type'])  # .round()
        p_of_safe = all_regressors_panda.loc[participant]
        all_epochs = np.array(data['decision_onset_epoch'])

        # Getting the start time of the first slice for each run
        acq_time_1, study_time = one_dicom_header(participant, 1)
        acq_time_2, study_time = one_dicom_header(participant, 2)

        # Save the acq time lists
        acq_time_list_2.append(acq_time_2)
        study_time_all.append(study_time)
        time_sync_all.append(study_time)

        # medium smart to not save the epochs for the shade, so setting up a fix for that here
        unconverted_time = np.array(data['decision_onset'])[120]
        time_to_subtract = imp.convert_to_secs(unconverted_time)

        # setting some things up
        onset_epoch = all_epochs-first_epoch
        max_trials = len(onset_epoch)

        # now we loop and write out two different tsv files
        names = ['onset_epoch', 'decision_made', 'rts',
                 'choice', 'risky_points', 'trans_type', 'p_of_safe']
        for file_suffix, indices in zip(['run2', 'run1'], [[trial_cutoff, max_trials], [0, trial_cutoff]]):
            this_onset_epoch = onset_epoch[indices[0]:indices[1]]
            this_onset_epoch -= this_onset_epoch[0]

            this_decision_made = this_onset_epoch+rts[indices[0]:indices[1]]

            one_sub = np.stack([this_onset_epoch,
                                this_decision_made,
                                rts[indices[0]:indices[1]],
                                choice[indices[0]:indices[1]],
                                risky_points[indices[0]:indices[1]],
                                trans_type[indices[0]:indices[1]],
                                p_of_safe[indices[0]:indices[1]]], axis=1)
            df = pd.DataFrame(one_sub)
            df.columns = names

            with open('fmri_regressors/fmri_regressors_{}_{}.tsv'.format(participant, file_suffix), 'a') as poopy:
               # poopy.write('onset_epoch, decision_made, choice, risky_points, trans_type, p_of_safe\n')
                np.savetxt(poopy, one_sub, fmt='%1.3f', delimiter='\t')

        # passive viewing time extraction
        shade_timing_raw = imp.import_shade(participant)
        shade_onset = np.array(shade_timing_raw[0])-acq_time_2
        shade_fix = np.array(shade_timing_raw[1])-acq_time_2

        shade_fix_first.append(shade_fix[0])

        shade_timings = np.stack([shade_onset, shade_fix], axis=1)

        with open('fmri_regressors/fmri_regressors_shade_{}.tsv'.format(participant), 'a') as peepy:
            np.savetxt(peepy, shade_timings, fmt='%1.3f', delimiter='\t')

    return acq_time_list_2, shade_fix_first, study_time_all, time_sync_all


def one_dm_per_subject(sub_num):
    '''Inputs a subject number, outputs a txt file with values and contrasts to be used'''
    pass


def regressors_model_stuffies():
    '''Makes the model stuffies into nicely formatted dictionaries that I have to 
    later turn into something matlab can look at for neuroimaging'''
    import analysis as anal

    p_of_safe, conflict_level = anal.calculate_conflict(
        filename='/data_fmri_{}.pi')

    return p_of_safe, conflict_level


def regressors_conflict():
    '''gets conflict level of each trial for each participant for each trial

    may not work because I don't know what p_of_safe is and it may need to be pu
    in a loop.'''

    p_of_safe = regressors_model_stuffies()

    conflict = 1 - 2*abs(p_of_safe-0.5)

    return conflict


def model_regressors_miniblock(participant_list):
    '''gives me, in the order of the experiment, the model values to as for the particular 
    participant -- assuming that i am lumping the fmri data together miniblock-wise'''

    with open('contexts_fmri.pi', 'rb') as dumpy:
        mblocks = pickle.load(dumpy)
    # hand_done_types = [5,7,6,4,1,6,5,6,4,4,5,7,7,7,6,4,1,6,5]

    color, risk, trans_type = classify_mblocks(mblocks)

    return


def classify_mblocks(mblocks):

    c_mblock = np.array(mblocks)
    n_mblock = np.roll(mblocks, -1)

    # 0 means same
    color = np.abs(c_mblock - n_mblock) % 2
    risk = (n_mblock >= 2) * 1 + 2 * (c_mblock >= 2)
    trans_type = risk + 4 * color

    return color, risk, trans_type


def read_dicom_headers(participant, run):
    '''Boy did I fuck this all up. 
    This fucntion reads into the dicom headersand gives me the metadata 

    Takes as input the prticipnt and the run, reads into the DICOM and looks for the timing,
    and passes it back into the subtract_time() function. '''

    # Use dicom_parser to load the header
    HOME_FOLDER = './first_slices'

    working_dir = (HOME_FOLDER+'/{}'.format(participant) +
                   '/run{}/'.format(run))

    file_list = glob.glob(working_dir + '*.ima')

    # Skip (for now) where there is no DICOM found, make this a warning
    # or do something smart later. Also you should probably look into this.
    ##
    # MAKING A HORRIBLE HACK HERE SO IT DOESN'T CRASH WHEN CALLED BY SUBTRACT TIME
    # FIX THIS !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    if len(file_list) == 0:
        raise ValueError(
            'Participant {} does not have an .ima file'.format(participant))
        # all_fmri_data_2  = imp.import_data(participants=[participant],
        #                                    filename='/data_fmri_{}.pi')
        # data_2 = all_fmri_data_2.loc[participant]
        # unconverted_time = np.array(data_2['decision_onset'])[120]
        # acq_time         = imp.convert_to_secs(unconverted_time)
        # print('DICOM files for {} not found !'.format(participant))

    else:
        ima_file = file_list[0]

        full_header = Image(ima_file)

        raw_acq_time = full_header.header[('0008', '0032')]
        # raw_ppsst    = full_header.header[('0040','0245')]

        acq_time = np.sum(
            [raw_acq_time.hour*3600, raw_acq_time.minute*60, raw_acq_time.second])
    return acq_time

    # GET SOME INFORMATION OUT OF THE DICOM HEADER

    ##### Things that are (potentially) interesting to us #######
    # ((0008,0032) Acquisition Time)
    # ((0020, 0012) Acquisition Number)
    # (0040, 0245) Performed Procedure Step Start Time)
    # (0019, 1016) [TimeAfterStart]
    # (0008, 0030) Study Time                          TM: '131435.041000'
    # (0008, 0031) Series Time                         TM: '132601.706000'
    # (0008, 0032) Acquisition Time                    TM: '132902.805000'


def one_dicom_header(participant, run, stfu=False):
    '''Boy did I fuck this all up.  This fucntion reads into the dicom headers and
    gives me the metadata

    Takes as input the prticipnt and the run, reads into the DICOM and looks
    for the timing, and passes it back into the subtract_time() function.

    AN IMPORTANT NOTE : spm will need things in a not-epoch format, i.e.,
    starting at 0, seconds since onset


    Returns
    -------

    The number of seconds since the Epoch (1970) for the point at which the
    fmri machine sends the J signal to the stimulus machine. Used for fixing
    the timings on the behavioral data.

    '''
    # Use dicom_parser to load the header
    HOME_FOLDER = './first_slices'
    working_dir = HOME_FOLDER + \
        '/{}'.format(participant) + '/run{}/'.format(run)

    file_list = glob.glob(working_dir + "/*.ima")
    if len(file_list) == 0:
        raise ValueError(
            'The .ima file for participant {} was not found.'.format(participant))
    else:
        ima_file = file_list[0]
        full_header = Image(ima_file)
        raw_acq_time = full_header.header[('0008', '0032')]
        raw_acq_date = full_header.header[('0008', '0022')]
        if not stfu:
            print(raw_acq_date, raw_acq_time)
        # raw_acq_time_sync = full_header.header[('0008','002A')]
        # raw_study_time = full_header.header[('0008', '0030')]
        # raw_ppsst    = full_header.header[('0040','0245')]
        onset_epoch = datetime(raw_acq_date.year, raw_acq_date.month,
                               raw_acq_date.day, raw_acq_time.hour,
                               raw_acq_time.minute, raw_acq_time.second)
        onset_epoch = onset_epoch.timestamp()
        onset_epoch += raw_acq_time.microsecond * 10 ** (-6)
        int_acq_time = np.array('{:02}{:02}{:02}'.format(raw_acq_time.hour,
                                                         raw_acq_time.minute,
                                                         raw_acq_time.second))
        acq_time = imp.convert_to_secs(int_acq_time)

        # int_study_time    = np.array('{:02}{:02}{:02}'.format(raw_study_time.hour, raw_study_time.minute, raw_study_time.second))
        # study_time        = imp.convert_to_secs(int_study_time)
        if not stfu:
            print('First slice Acq Time for {} on run {} is : {}'.format(participant,
                                                                         run, acq_time))

    return onset_epoch  # acq_time


def repandify_things():
    '''Adds new columns from the regressors produced onto the old dataframe'''

    old_pd = imp.import_all(data_sets=['fmri'], no_timeouts=False)

    # Get the regressors out
    p_of_safe, conflict_level = regressors_model_stuffies()

    # organise them nicely
    dm_regressors = [p_of_safe, conflict_level]
    dm_regressors_names = ['p_of_safe', 'conflict_level']
    # Turn each array into a series

    for regressor, names in zip(dm_regressors, dm_regressors_names):
        all_series = []
        for key, value in regressor.items():
            all_series.append(
                pd.Series(value, index=key * np.ones(len(value))))
        big_series = pd.concat(all_series)
        old_pd[names] = big_series
    old_pd = zero_decision_times(old_pd)
    # old_pd = old_pd.assign(p_of_safe = series_safe)
    # old_pd = old_pd.assign(conflict_level = series_conflict)

    # new_pd = old_pd  # <--- unnecessary. You can simply return old_pd

    return old_pd


def zero_decision_times(old_pd):
    """Finds the time of the first slice in the fmri header and uses that to set
    the zero for the decision onset times in the behavioral data (during fmri).

    """
    panda = old_pd.copy()
    participant_list = np.unique(panda.index.values)

    # Create an absolute trial number from 0 to 239
    panda['total_trial'] = panda['trial'] + panda['mblock'] * 6
    for participant in participant_list:
        onset_1 = one_dicom_header(participant, 1)
        onset_2 = one_dicom_header(participant, 2)
        cond_1 = np.logical_and(panda['condition'] == 0,
                                panda.index == participant)
        cond_2 = np.logical_and(panda['condition'] == 0.75,
                                panda.index == participant)
        panda.loc[cond_1, ['decision_onset_epoch']] -= onset_1
        panda.loc[cond_2, ['decision_onset_epoch']] -= onset_2

    return panda


def read_all_slices(folder=None):
    """Read all dicom slice headers in --folder--."""
    if folder is None:
        folder = '/media/dario/Bug_Out_Bag/new_pmt_scans/012896/1002_4003_1/20210706/1738543000002021070617073720150/s05_boldB_pmt_1_1_32_t_a_1_1_2_8_30302505_25_2360_m/17591296496322194/'
    headers = []
    for filename in glob.glob(folder + '*.ima'):
        headers.append(Image(filename))

    return headers


def all_header_times(headers=None, header_label=None):
    """Read the --header_label-- for all the --headers--. Returns in numpy."""
    if header_label is None:
        header_label = ('0008', '0032')
    all_read = []
    for header in headers:
        all_read.append(header.header[header_label])
    return np.array(all_read)


def loop_all_header_times(folder=None):
    """Loops all_header_times with the list in candidate_list()

    """
    candidates = candidate_list()
    headers = read_all_slices(folder=folder)
    all_infos = {}
    for candidate in candidates:
        try:
            header_info = all_header_times(headers=headers,
                                           header_label=candidate)
        except KeyError:
            print('{} was not found'.format(candidate))
        # header_info = np.array([imp.convert_to_secs(thing)
        #                         for thing in header_info])
        all_infos[candidate] = header_info
    return all_infos


def loop_folders():
    """Loops loop_all_header_times for all the folders in folder_names()."""
    sequences = {}
    for key, folder in folder_names().items():
        all_infos = loop_all_header_times(folder=folder)
        sequences[key] = all_infos
    return sequences


def candidate_list():
    """List of all possible dicom header timing things that could possibly be.
    To be fed into other fuckitions later. Great !"""

    return [('0008', '0013'),
            ('0008', '0030'),
            ('0008', '0031'),
            ('0008', '0032'),
            ('0008', '0033'),
            ('0008', '0034'),
            ('0008', '0035'),
            ('0010', '0032'),
            ('0014', '0103'),
            ('0014', '3077'),
            ('0014', '407C'),
            ('0018', '0027'),
            ('0018', '0035'),
            ('0018', '1014'),
            ('0018', '1042'),
            ('0018', '1043'),
            ('0018', '1072'),
            ('0018', '1073'),
            ('0018', '1201'),
            ('0018', '700E'),
            ('0020', '3405'),
            ('0032', '0033'),
            ('0032', '0035'),
            ('0032', '1001'),
            ('0032', '1011'),
            ('0032', '1041'),
            ('0032', '1051'),
            ('0038', '001B'),
            ('0038', '001D'),
            ('0038', '0021'),
            ('0038', '0032'),
            ('0040', '0003'),
            ('0040', '0005'),
            ('0040', '0245'),
            ('0040', '0251'),
            ('0040', '2005'),
            ('0040', 'A024'),
            ('0040', 'A112'),
            ('0040', 'A122'),
            ('0040', 'A193'),
            ('0070', '0083'),
            ('0072', '006B'),
            ('2100', '0050'),
            ('3006', '0009'),
            ('3008', '0025'),
            ('3008', '0164'),
            ('3008', '0168'),
            ('3008', '0251'),
            ('300A', '0007'),
            ('300A', '022E'),
            ('300E', '0005'),
            ('4008', '0101'),
            ('4008', '0109'),
            ('4008', '0113')]


def get_all_onsets():
    """Finds all participants and breaks their legs because they didn't pay up.
    Also reads their dicom headers for the first slices (first and second
    sequences), reads the first decision onset from the behavioral files and,
    if available, the timing of the first trigger. Puts them all together in
    one panda.

    """
    data = imp.import_all(data_sets=['fmri'])
    participants = np.unique(data.index)

    first_decision_1 = data.query('phase == "fmri"').groupby(['participant'])['decision_onset_epoch'].min()
    first_decision_2 = data.query('phase == "fmri" and condition == 0.75').groupby(['participant'])['decision_onset_epoch'].min()

    first_decision = pd.concat([first_decision_1, first_decision_2],
                               axis=1, keys=['run1_beha', 'run2_beha'])
    imas = {}
    for participant in participants:
        try:
            acq_time1 = one_dicom_header(participant, 1, stfu=True)
            acq_time2 = one_dicom_header(participant, 2, stfu=True)
        except ValueError:
            print('Participant {} did not have its ima '
                  'file in first_slices'.format(participant))
            continue
        imas[participant] = [acq_time1, acq_time2]

    imas = pd.DataFrame.from_dict(imas, orient='index',
                                  columns=['run1_ima', 'run2_ima'])
    triggers = {}
    for participant in participants:
        with open('./exp_data/data_fmri_{}.pi'.format(participant), 'rb') as mafi:
            datum = pickle.load(mafi)
        try:
            run1 = datum['experiment']['onsets']['first_sequence_onset_epoch']
            run2 = datum['experiment']['onsets']['second_sequence_onset_epoch']
            triggers[participant] = [run1, run2]
        except KeyError:
            pass
    triggers = pd.DataFrame.from_dict(triggers, orient='index',
                                      columns=['run1_tri', 'run2_tri'])
    return pd.concat([first_decision, imas, triggers], axis=1)


def beha_vs_ima():
    """Compares the timings of the first decision and the acquisition time
    of the first slice, for both sequences.

    Returns behavioral - ima, which should be a positive number; ideally
    a few seconds (anywhere 3<t<20, I suppose?)

    """
    all_onsets = get_all_onsets()
    run1 = all_onsets['run1_beha'] - all_onsets['run1_ima']
    run2 = all_onsets['run2_beha'] - all_onsets['run2_ima']
    return pd.concat([run1, run2], axis=1, keys=['run1', 'run2'])


def tri_vs_ima():
    """Compares the trigger and ima acquisition time, for both sequences.

    Returns tri - ima.

    """
    all_onsets = get_all_onsets()
    run1 = all_onsets['run1_tri'] - all_onsets['run1_ima']
    run2 = all_onsets['run2_tri'] - all_onsets['run2_ima']
    return pd.concat([run1, run2], axis=1, keys=['run1', 'run2'])
