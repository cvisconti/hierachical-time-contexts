#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
First-level analysis Pipeline, using NiPype but with SPM under the hood
"""

#from matplotlib import inline
from os.path import join as opj
import json
import pandas as pd
import numpy as np
from nipype.interfaces.spm import Level1Design, EstimateModel, EstimateContrast
from nipype.algorithms.modelgen import SpecifySPMModel
from nipype.interfaces.utility import Function, IdentityInterface
from nipype.interfaces.io import SelectFiles, DataSink
from nipype import Workflow, Node
# Import modules
from os.path import join as opj
from nipype.interfaces.afni import Despike
from nipype.interfaces.freesurfer import (BBRegister, ApplyVolTransform,
                                          Binarize, MRIConvert, FSCommand)
from nipype.interfaces.spm import (SliceTiming, Realign, Smooth, Level1Design,
                                   EstimateModel, EstimateContrast)
from nipype.interfaces.utility import Function, IdentityInterface
from nipype.interfaces.io import FreeSurferSource, SelectFiles, DataSink
from nipype.algorithms.rapidart import ArtifactDetect
from nipype.algorithms.misc import TSNR, Gunzip
from nipype.algorithms.modelgen import SpecifySPMModel
from nipype.pipeline.engine import Workflow, Node, MapNode

# MATLAB - Specify path to current SPM and the MATLAB's default mode
from nipype.interfaces.matlab import MatlabCommand
MatlabCommand.set_default_paths('/Users/visconti/Documents/spm12/spm12_mcr')
MatlabCommand.set_default_matlab_cmd("matlab -nodesktop -nosplash")

experiment_dir = '/Volumes/Bug Out Bag/Work/derivatives/fmriprep'
output_dir = '/Volumes/Bug Out Bag/Work/derivatives/first_level'
working_dir = '/Volumes/Bug Out Bag/Work/derivatives/working_dir'

# Just one Subject for Now
sub_path = '/Volumes/Bug Out Bag/Work/derivatives/fmriprep/3013/func/sub-010944_task-pointmonster_run-1_space-MNI152NLin2009cAsym_desc-preproc_bold.json'


# TR of functional images
with open(sub_path, 'rt') as fp:
    task_info = json.load(fp)
TR = task_info['RepetitionTime']


# # Smoothing withds used during preprocessing
# fwhm = [4, 8]

# # SpecifyModel - Generates SPM-specific Model
# modelspec = Node(SpecifySPMModel(concatenate_runs=False,
#                                  input_units='secs',
#                                  output_units='secs',
#                                  time_repetition=TR,
#                                  high_pass_filter_cutoff=128),
#                  name="modelspec")

# # Level1Design - Generates an SPM design matrix
# level1design = Node(Level1Design(bases={'hrf': {'derivs': [1, 0]}},
#                                  timing_units='secs',
#                                  interscan_interval=TR,
#                                  model_serial_correlations='FAST'),
#                     name="level1design")

# # EstimateModel - estimate the parameters of the model
# level1estimate = Node(EstimateModel(estimation_method={'Classical': 1}),
#                       name="level1estimate")

# # EstimateContrast - estimates contrasts
# level1conest = Node(EstimateContrast(), name="level1conest")

# # Get Subject Info - get subject specific condition information
# getsubjectinfo = Node(Function(input_names=['subject_id'],
#                                output_names=['subject_info'],
#                                function=make_onsets_choice(),name='getsubjectinfo')
# # is this not just
# # subjectinfo = make_onsets_choice(subject_id) ??

# # Volume Transformation - transform contrasts into anatomical space
# applyVolReg = MapNode(ApplyVolTransform(fs_target=True),
#                       name='applyVolReg',
#                       iterfield=['source_file'])

# # MRIConvert - to gzip output files
# mriconvert = MapNode(MRIConvert(out_type='niigz'),
#                      name='mriconvert',
#                      iterfield=['in_file'])

# ###
# # Specify 1st-Level Analysis Workflow & Connect Nodes

# # Initiation of the 1st-level analysis workflow
# l1analysis = Workflow(name='l1analysis')

# # Connect up the 1st-level analysis components
# l1analysis.connect([(modelspec, level1design, [('session_info',
#                                                 'session_info')]),
#                     (level1design, level1estimate, [('spm_mat_file',
#                                                      'spm_mat_file')]),
#                     (level1estimate, conestimate, [('spm_mat_file',
#                                                     'spm_mat_file'),
#                                                    ('beta_images',
#                                                     'beta_images'),
#                                                    ('residual_image',
#                                                     'residual_image')]),
#                     (conestimate, applyVolReg, [('con_images',
#                                                  'source_file')]),
#                     (applyVolReg, mriconvert, [('transformed_file',
#                                                 'in_file')]),
#                     ])



# ###
# # Input & Output Stream

# # Infosource - a function free node to iterate over the list of subject names
# infosource = Node(IdentityInterface(fields=['subject_id',
#                                             'contrasts'],
#                                     contrasts=make_contrasts()),
#                   name="infosource")
# infosource.iterables = [('subject_id', subject_list)]

# # SelectFiles - to grab the data (alternativ to DataGrabber)
# templates = {'func': 'data/{subject_id}/run*.nii.gz'}
# selectfiles = Node(SelectFiles(templates,
#                                base_directory=experiment_dir),
#                    name="selectfiles")

# # Datasink - creates output folder for important outputs
# datasink = Node(DataSink(base_directory=experiment_dir,
#                          container=output_dir),
#                 name="datasink")

# # Use the following DataSink output substitutions
# substitutions = [('_subject_id_', ''),
#                  ('_despike', ''),
#                  ('_detrended', ''),
#                  ('_warped', '')]
# datasink.inputs.substitutions = substitutions

# # Connect Infosource, SelectFiles and DataSink to the main workflow
# metaflow.connect([(infosource, selectfiles, [('subject_id', 'subject_id')]),
#                   (infosource, preproc, [('subject_id',
#                                           'bbregister.subject_id'),
#                                          ('subject_id',
#                                           'fssource.subject_id')]),
#                   (selectfiles, preproc, [('func', 'despike.in_file')]),
#                   (infosource, getsubjectinfo, [('subject_id', 'subject_id')]),
#                   (getsubjectinfo, l1analysis, [('subject_info',
#                                                  'modelspec.subject_info')]),
#                   (infosource, l1analysis, [('contrasts',
#                                              'conestimate.contrasts')]),
#                   (preproc, datasink, [('realign.mean_image',
#                                         'preprocout.@mean'),
#                                        ('realign.realignment_parameters',
#                                         'preprocout.@parameters'),
#                                        ('art.outlier_files',
#                                         'preprocout.@outliers'),
#                                        ('art.plot_files',
#                                         'preprocout.@plot'),
#                                        ('binarize.binary_file',
#                                         'preprocout.@brainmask'),
#                                        ('bbregister.out_reg_file',
#                                         'bbregister.@out_reg_file'),
#                                        ('bbregister.out_fsl_file',
#                                         'bbregister.@out_fsl_file'),
#                                        ('bbregister.registered_file',
#                                         'bbregister.@registered_file'),
#                                        ]),
#                   (l1analysis, datasink, [('mriconvert.out_file',
#                                            'contrasts.@contrasts'),
#                                           ('conestimate.spm_mat_file',
#                                            'contrasts.@spm_mat'),
#                                           ('conestimate.spmT_images',
#                                            'contrasts.@T'),
#                                           ('conestimate.con_images',
#                                            'contrasts.@con'),
#                                           ]),
#                   ])


# ###
# # Run Workflow
# metaflow.write_graph(graph2use='colored')
# metaflow.run('MultiProc', plugin_args={'n_procs': 8})

pass


def make_contrasts():
    '''Sets up some contrasts for the first-level GLM analysis.'''
    
    # Condition names
    condition_names = ['safe', 'risky']
    
    # Contrasts
    cont01 = ['safe',   'T', condition_names, [1, 0]]
    cont02 = ['risky', 'T', condition_names, [0, 1]]
    cont03 = ['safe vs risky', 'T', condition_names, [1, -1]]
    cont04 = ['risky vs safe', 'T', condition_names, [-1, 1]]
    cont05 = ['Cond vs zero', 'F', [cont01, cont02]]
    cont06 = ['Diff vs zero', 'F', [cont03, cont04]]
    
    contrast_list = [cont01, cont02, cont03, cont04, cont05, cont06]
    
    return contrast_list

def read_tsv_file(participant,run):
    '''Reads in tsv file for a given participant and a given run'''
    
    if run == 1:
        run = 'run1'
    elif run == 2:
        run = 'run2'
    else:
        raise ValueError('Please specify run 1 or 2 !')
    
    
    dir_regressors = '/Users/visconti/Documents/hierachical-time-contexts/fmri_regressors/'
    
    regressors_pd = pd.read_csv(dir_regressors+'fmri_regressors_{}_{}.tsv'.format(participant,run),sep='\t', names=['onset_epoch','decision_made', 'rts','choice', 'risky_points', 'trans_type', 'p_of_safe'])
    
    return regressors_pd

def make_onsets_choice(participant):
    '''For a given participant, returns a list of the onsets when a participant made a risky or
    safe decision. 
    
    This puts both runs into one thing, and then bunches them together. Not a good idea for future analysis.
    
    This could maybe be done smarter actually, using the decision time ?'''
    
    onsets_safe_run1  = []
    onsets_risky_run1 = []
    onsets_safe_run2  = []
    onsets_risky_run2 = []
    
    # Scraping out the times of interest for the risky and safe decisions for run 1
    # Could this be looped ? Sure. But I won't want to do this eventually and don't feel like doing it now.
    regressors_run1 = read_tsv_file(participant, 1)
    regressors_run1['choice'].astype('int64')
        
    for ix_onset, epoch in regressors_run1.iterrows():
        if regressors_run1['choice'][ix_onset] == 0:
            onsets_safe_run1.append(regressors_run1['onset_epoch'][ix_onset])
        elif regressors_run1['choice'][ix_onset] == 1:
            onsets_risky_run1.append(regressors_run1['onset_epoch'][ix_onset])
            
    # Second run, again, fuck you, it works.
    regressors_run2 = read_tsv_file(participant, 2)
    regressors_run2['choice'].astype('int64')
        
    for ix_onset, epoch in regressors_run2.iterrows():
        if regressors_run2['choice'][ix_onset] == 0:
            onsets_safe_run2.append(regressors_run2['onset_epoch'][ix_onset])
        elif regressors_run2['choice'][ix_onset] == 1:
            onsets_risky_run2.append(regressors_run2['onset_epoch'][ix_onset])
    
     
    choice_onset_list = [onsets_safe_run1,onsets_risky_run1,onsets_safe_run2,onsets_risky_run2]
    
    ### Following code is from NiPype for Beginners, maybe not a good idea for this particular situation
    condition_names = ['safe', 'risky']
    from nipype.interfaces.base import Bunch
    
    subjectinfo = []
    for r in range(2):
        onsets = [choice_onset_list[r*2], choice_onset_list[r*2+1]]
        subjectinfo.insert(r,
                            Bunch(conditions=condition_names,
                                  onsets=onsets,
                                  durations=[[0], [0]],
                                  amplitudes=None,
                                  tmod=None,
                                  pmod=None,
                                  regressor_names=None,
                                  regressors=None))
    
    return subjectinfo, choice_onset_list

def make_durations_choice(participant):
    '''For a given participant, returns a list of the onsets when a participant made a risky or
    safe decision. 
    
    This puts both runs into one thing, and then bunches them together. Not a good idea for future analysis.
    
    This could maybe be done smarter actually....'''
    
    durations_safe_run1  = []
    durations_risky_run1 = []
    durations_safe_run2  = []
    durations_risky_run2  = []
    
    # Scraping out the times of interest for the risky and safe durations for run 1
    regressors_run1 = read_tsv_file(participant, 1)
    regressors_run1['choice'].astype('int64')
    regressors_run2 = read_tsv_file(participant, 2)
    regressors_run2['choice'].astype('int64')
        
    for ix_onset, decision_made in regressors_run1.iterrows():
        if regressors_run1['choice'][ix_onset] == 0:
            durations_safe_run1.append(regressors_run1['decision_made'][ix_onset]-regressors_run1['onset_epoch'][ix_onset])
        elif regressors_run1['choice'][ix_onset] == 1:
            durations_risky_run1.append(regressors_run1['decision_made'][ix_onset]-regressors_run1['onset_epoch'][ix_onset])
    
    for ix_onset, decision_made in regressors_run1.iterrows():
       if regressors_run2['choice'][ix_onset] == 0:
          durations_safe_run2.append(regressors_run2['onset_epoch'][ix_onset]+regressors_run2['decision_made'][ix_onset])
       elif regressors_run2['choice'][ix_onset] == 1:
           durations_risky_run2.append(regressors_run2['onset_epoch'][ix_onset]+regressors_run2['decision_made'][ix_onset])
    
                    
    durations_safe_run1 = np.array(durations_safe_run1).round(3)
    durations_risky_run1 = np.array(durations_risky_run1).round(3)
   
     
    durations_onset_list = [durations_safe_run1,durations_risky_run1,durations_safe_run2,durations_risky_run2]

    return durations_onset_list