# -*- coding: utf-8 -*-
"""
Make design matrix

@author: cassandra

A collection of functions that will make design matrices on demand 
"""
import numpy as np
import pandas as pd
import import_data as imp
import pickle 
import csv
import pydicom as dicom
import dicom_parser as dp 
from dicom_parser import Image
import glob

def get_choice_conflict(center, slope, unmatched_points):
    cc = 1 / (1 + np.exp(slope * (unmatched_points - center)))
    
    return cc

def convert_to_secs(unconverted_time):
    '''small function that converts HHMMSS into seconds
    Inputs : HHMMSS time in seconds (int)
    Outputs : time in seconds '''

    new_time = []

    str_value = str(unconverted_time)
    hrs_str = int(str_value[:2])*3600
    min_str = int(str_value[2:4])*60
    sec_str = int(str_value[4:6])
    new_time = hrs_str+min_str+sec_str

    return new_time


def subtract_time():
    '''Timestamps were saved using the python time.time function, which was medium smart
    so the only thing this will do is subtract the first time from all of the other ones
    
    What I eventually want is a .txt file with display of time of each offer
    calculated as time since first display of first offer as one column
    
    also time that offer was taken, so this plus rts,
    this is called decision_made
    
    also all the other stuff in give_design_matrix()
    '''
         
    # Scrape out the wrong timings here
    # cond1_onsets = (all_fmri_data['experiment'][0]['decision_onset'])
    # first_value  = cond1_onsets[0][0]
    
    # cond2_onsets = (initial_data['experiment'][1]['decision_onset'])
    # shade_fix    = (initial_data['shade']['fix_onset'])
    # shade_stim   = (initial_data['shade']['stim_onset'])
        

    all_fmri_data  = imp.import_data(filename='\data_fmri_{}.pi')
    #import pdb; pdb.set_trace()
    all_regressors = regressors_model_stuffies()
    
    all_regressors_panda = pd.DataFrame.from_dict(all_regressors,orient='index')
    
    participant_list       = np.unique(all_fmri_data.index)
    
    # set up some containers for comparing the first onset time and the first shade stim
    acq_time_list_2        = []
    shade_fix_first        = []
    
    study_time_all         = []
    time_sync_all          = []
    
    trial_cutoff = 120 
    # The trial at which the data should be split into the two different runs

    for participant in participant_list: 
        data = all_fmri_data.loc[participant]
      #  first_value  = data['decision_onset'][0]
        first_epoch  = np.array(data['decision_onset_epoch'])[0]#.round(decimals=3)
        second_epoch = np.array(data['decision_onset_epoch'])[119]
        rts          = np.array(data['rts'])#.round(decimals=3)
        choice       = np.array(data['choice'])#.round(decimals=0)
        risky_points = np.array(data['risky_points'])#.round()
        trans_type   = np.array(data['trans_type'])#.round()
        p_of_safe    = all_regressors_panda.loc[participant]
        all_epochs   = np.array(data['decision_onset_epoch'])
        
        # Getting the start time of the first slice for each run
        acq_time_1, study_time      = one_dicom_header(participant, 1)
        acq_time_2, study_time      = one_dicom_header(participant, 2)
        
        # Save the acq time lists
        acq_time_list_2.append(acq_time_2)
        study_time_all.append(study_time)
        time_sync_all.append(study_time) 
        
        
        
        # medium smart to not save the epochs for the shade, so setting up a fix for that here
        unconverted_time       = np.array(data['decision_onset'])[120]
        time_to_subtract       = convert_to_secs(unconverted_time)
        
        # setting some things up
        onset_epoch   = all_epochs-first_epoch
        max_trials    = len(onset_epoch)

        # now we loop and write out two different tsv files
        names=['onset_epoch', 'decision_made', 'rts', 'choice', 'risky_points', 'trans_type', 'p_of_safe']
        for file_suffix, indices in zip(['run2', 'run1'], [[trial_cutoff, max_trials], [0, trial_cutoff]]):
            this_onset_epoch = onset_epoch[indices[0]:indices[1]]
            this_onset_epoch -= this_onset_epoch[0]
            
            this_decision_made = this_onset_epoch+rts[indices[0]:indices[1]]
            
            one_sub = np.stack([this_onset_epoch,
                                this_decision_made,
                                rts[indices[0]:indices[1]],
                                choice[indices[0]:indices[1]],
                                risky_points[indices[0]:indices[1]],
                                trans_type[indices[0]:indices[1]],
                                p_of_safe[indices[0]:indices[1]]],axis=1)
            df = pd.DataFrame(one_sub)
            df.columns = names
            
            with open('fmri_regressors/fmri_regressors_{}_{}.tsv'.format(participant, file_suffix),'a') as poopy:
               # poopy.write('onset_epoch, decision_made, choice, risky_points, trans_type, p_of_safe\n')
                np.savetxt(poopy, one_sub,fmt='%1.3f',delimiter='\t')
                
    
        # passive viewing time extraction 
        shade_timing_raw = imp.import_shade(participant)
        shade_onset      = np.array(shade_timing_raw[0])-acq_time_2
        shade_fix        = np.array(shade_timing_raw[1])-acq_time_2
        
        shade_fix_first.append(shade_fix[0])
        
        
        shade_timings    = np.stack([shade_onset,shade_fix],axis=1) 
        
        with open('fmri_regressors/fmri_regressors_shade_{}.tsv'.format(participant),'a') as peepy:
                np.savetxt(peepy, shade_timings,fmt='%1.3f',delimiter='\t')
    
    return acq_time_list_2, shade_fix_first, study_time_all, time_sync_all

def regressors_model():
    '''Makes the model regressors into nicely formatted dictionaries that I have to 
    later turn into something matlab can look at for neuroimaging'''
    
    # with open('all_inferred_parameters.pi','rb') as dumpy:
    #     all_inferred_parameters = pickle.load(dumpy)    
    all_inferred_parameters = pd.read_pickle('all_inferred_parameters.pi')
    
    all_data = imp.import_data(filename='/data_fmri_{}.pi')
    all_risky_points = {}
    all_trans_types  = {}
    p_of_safe        = {}
    conflict_level   = {}
    
    participant_list = np.unique(all_data.index)
    
    
    for participant in participant_list:
        current_data = all_data.loc[participant]
        risky_points = current_data['risky_points']
        trans_type   = current_data['trans_type']
        all_risky_points[participant] = np.array(risky_points)
        all_trans_types[participant]  = np.array(trans_type)
    
    means = all_inferred_parameters.groupby(['participant','condition','trans_type']).mean()
    
    for each_participant in participant_list:
        screech          = np.ones(len(all_risky_points[each_participant]))
                                   
        for ix_trial, amt_pts in enumerate(all_risky_points[each_participant]):
            try:
                center,slope = np.array(means.loc[(each_participant,0,all_trans_types[each_participant][ix_trial])])
            except:
                screech[ix_trial] = -100
                continue
            temp_exponent = slope*(amt_pts-center)
            
            screech[ix_trial] = 1 / (1 + np.exp(temp_exponent))
            
        p_of_safe[each_participant] = screech
        conflict_level[each_participant] = 1 - 2*abs(p_of_safe[each_participant]-0.5)
        
    d = dict( A = np.array([1,2]), B = np.array([1,2,3,4]) )

    pd.DataFrame(dict([ (k,pd.Series(v)) for k,v in d.items() ]))
        
    
    return p_of_safe, conflict_level

def repandify_things():
    '''Adds new columns from the regressors produced onto the old dataframe'''
    
    old_pd = imp.import_all(data_sets=['fmri']) 
    
    #Get the regressors out 
    p_of_safe, conflict_level = regressors_model_stuffies()
    
    
    #Turn each array into a series 
    series_safe = pd.Series(p_of_safe)
    series_conflict = pd.Series(conflict_level)
    
    # Add it as a column to the preexisting data frame
    old_pd = old_pd.assign(p_of_safe = series_safe)
    old_pd = old_pd.assign(conflict_level = series_conflict)
    
    new_pd = old_pd

        
    return new_pd
    
