# -*- coding: utf-8 -*-
# ../plotting.py

"""Plots for data analysis and stuff."""
import pickle
import itertools as it

import numpy as np
from matplotlib import pyplot as plt
import pandas as pd
import statsmodels.formula.api as smf
import seaborn as sns

import analysis as anal
import import_data


def calc_subplots(num_plots):
    """ Calculates a good arrangement for the subplots given the number of
    subjects.
    """
    if num_plots == 2 or num_plots == 3:
        return num_plots, 1

    sqrtns = np.sqrt(num_plots)
    if abs(sqrtns - np.ceil(sqrtns)) < 0.001:
        a1 = a2 = np.ceil(sqrtns)
    else:
        divs = num_plots % np.arange(2, num_plots)
        divs = np.arange(2, num_plots)[divs == 0]
        if divs.size == 0:
            return calc_subplots(num_plots + 1)
        else:
            a1 = divs[np.ceil(len(divs) / 2).astype(int)]
            a2 = num_plots / a1
    return np.array((int(a1), int(a2)))


def plot_prob_risky(participants=None, category=None, per_part=True, ax=None,
                    fignum=1):
    """Plots the frequency with which participants chose the risky option as
    a function of the variable --category--, averaging over all other variables
    in the data.

    Parameters
    ----------
    participants : array-like
    Participants for which the data is to be imported.

    category : str
    String t that determines the x-axis of the plot. It must be one of
    {'trial', 'curr_mblock', 'mblock_next', 'offer', 'points'}.
    All other elements will be averaged over. If None provided, defaults
    to 'risky_points'.

    per_part : bool
    Whether to make one line per participant (True) or average across
    participants.

    ax : matplotlib.ax (or whatever)
    Axis to plot on. If not provided, a new figure will be created with
    fignum.

    fignum : int
    Used for plt.figure(fignum) if no --ax-- is provided.
    """
    if ax is None:
        fig = plt.figure(fignum)

    prob_risky = anal.get_prob_risky(participants, categories=[category],
                                     per_part=per_part)
    num_parts = len(prob_risky)
    subplots = calc_subplots(num_parts)
    for ix_part, (part, probs) in enumerate(prob_risky.items()):
        if ax is None:
            c_ax = fig.add_subplot(*subplots, ix_part + 1)
        else:
            c_ax = ax[ix_part]
            probs.plot(x=category, y='choice', ax=c_ax)
            plt.title('Participant: {}'.format(part))
    plt.show(block=False)


def next_mblock(participants=None, per_part=True, ax=None,
                x_axis='risky_points', fignum=2):
    """Plots the frequency of the risky choice as a function of the
    different types of miniblock transitions, divided into
    color-matching and color-unmatching. Only trials in which the
    current miniblock color matches the color of the accumulated risky
    points are included; the case when those two don't match is
    assumed to be uninteresting, as the participant would just be
    matching them if possible. (there might be cases where it's not
    the case, though, like when the current mblock is risky and the
    following is the same color but safe, so it might be best to wait
    until the safe one to match.)

    Parameters
    ----------
    per_part : bool
    If True, there will be one subplot per participant in the data. If False,
    the plots are averaged across all participants.

    ax : pyplot.ax or list of them
    Where to plot. If per_part is True, this should be a list of axes with
    as manny elements as there are participants.

    x_axis : str
    Which variable to have as the x-axis. 'risky_color' and 'trial'
    are tested, others might work or not. It's all up to the goddess
    of bad code and poor life choices.

    """
    num_bins = 3

    if ax is None:
        fig = plt.figure(fignum)
    fig.clear()
    categories = ['risky_points', 'risky_color', 'mblock_type',
                  'mblock_next']
    if x_axis not in set(categories):
        categories.append(x_axis)
    prob_risky = anal.get_prob_risky(participants, categories=categories,
                                     per_part=per_part)
    subplots = calc_subplots(len(prob_risky))
    for ix_part, (part, probs) in enumerate(prob_risky.items()):
        if ax is None:
            c_ax = fig.add_subplot(*subplots, ix_part + 1)
        current_color = ['blue' if x.endswith('blue') else 'red'
                         for x in prob_risky[part]['mblock_type']]
        current_color = np.array(current_color)
        next_color = ['blue' if x.endswith('blue') else 'red'
                      for x in prob_risky[part]['mblock_next']]
        next_color = np.array(next_color)
        current_risky = prob_risky[part]['risky_color']
        now_matching = current_color == current_risky
        next_matching = current_color == next_color
        # ipdb.set_trace()
        # Plot matching
        matched_data = probs.loc[now_matching & next_matching]
        matched_data = matched_data[matched_data.notnull()]
        matched_data['bins'] = pd.cut(matched_data[x_axis], num_bins)
        to_plot = matched_data.groupby('bins').mean()['choice']
        to_plot.plot(x='bins', y='choice', ax=c_ax,
                     label='matched', marker='X')
        m_count = np.array([(matched_data['bins'] == c_bin).sum()
                            for c_bin in np.unique(matched_data['bins'])])
        print('Part: ', part, 'Matched. ', m_count)
        natched_data = probs.loc[now_matching & np.logical_not(next_matching)]
        natched_data = natched_data[natched_data.notnull()]
        natched_data['bins'] = pd.cut(natched_data[x_axis], num_bins)
        to_plot = natched_data.groupby('bins').mean()['choice']
        to_plot.plot(x='bins', y='choice', ax=c_ax,
                     label='not matched', marker="X")

        m_count = np.array([(natched_data['bins'] == c_bin).sum()
                            for c_bin in np.unique(natched_data['bins'])])
        print('Part: ', part, 'Not matched. ', m_count)
        c_ax.legend()
        c_ax.set_ylim((0, 1))
        c_ax.set_title('Participant {}'.format(part))
        c_ax.set_xlabel(x_axis)
        c_ax.set_ylabel('Freq. of risky offer')
    plt.tight_layout()

    plt.show(block=False)
    return matched_data, natched_data


def performance(participants=None, ax=None, fignum=3):
    """Plots a histogram with the number of earned points for all participants
    in --participants--.
    """
   # pal = sns.cubehelix_palette(5, rot=-.25, light=.7)
    if ax is None:
        fig = plt.figure(fignum)
        fig.clear()
        ax = fig.add_subplot()
        flag_show = True
    else:
        flag_show = False
    data = import_data.import_data(participants)
    participants = np.unique([*data.index.values])
    performance = np.ones(len(participants))
    for ix_part, part in enumerate(participants):
        performance[ix_part] = np.array(data.loc[part, ['points_red']].max())\
            + np.array(data.loc[part, ['points_blue']].max())\
            + np.array(data.loc[part, ['stars']].max() * 40)
    ax.barh(np.arange(len(participants)), performance)
    ax.set_yticks(np.arange(len(participants)))
    ax.set_yticklabels(participants)
    ax.set_xlabel('Total points')
    ax.set_ylabel('Participant number')
    ax.set_title('Overall performance')
    if flag_show:
        plt.show(block=False)


def overall_prob_risky(participants=None, ax=None, fignum=4):
    """Plots the overall frequency of risky choices for the participants."""
    if ax is None:
        fig = plt.figure(fignum)
        fig.clear()
        ax = fig.add_subplot(111)
        flag_show = True
    else:
        flag_show = False
    data = import_data.import_data(participants)
    participants = np.unique([*data.index.values])
    prob = np.ones(len(participants))
    for ix_part, part in enumerate(participants):
        prob[ix_part] = 1 - np.array(data.loc[part, ('choice')].mean())
    ax.barh(np.arange(len(participants)), prob)
    ax.set_yticks(np.arange(len(participants)))
    ax.set_yticklabels(participants)
    ax.set_xlabel('Probability of accepting risky offer')
    ax.set_ylabel('Participant number')
    ax.set_title('Overall frequency of risky choices')
    if flag_show:
        plt.show(block=False)


def performance_vs_risk(ax=None, fignum=7, **kwargs_import):
    """Scatter plot of performance vs frequency of risk for each participant.

    **kwargs_import are sent to import_data.import_data().
    """
    flag_show = False
    if ax is None:
        fig = plt.figure(fignum, clear=True)
        ax = fig.add_subplot(111)
        flag_show = True
    data = import_data.import_data(**kwargs_import)
    participants = np.unique([*data.index.values])
    performance = np.ones(len(participants))
    for ix_part, part in enumerate(participants):
        performance[ix_part] = np.array(data.loc[part, ['points_red']].max())\
            + np.array(data.loc[part, ['stars']].max() * 40)
    prob = np.ones(len(participants))
    for ix_part, part in enumerate(participants):
        prob[ix_part] = np.array(data.loc[part, ('choice')].mean())
    ax.scatter(performance, prob)
    formula = 'prob ~ performance'
    model = smf.ols(formula=formula, data={
                    'prob': prob, 'performance': performance})
    results = model.fit()
    b, m = results.params
    text = ('y = {:2.3f}x + {:2.2f}, '
            '\np_m = {:2.3f}, p_b = {:2.3f}').format(m, b, *results.pvalues)
    ax.text(x=0.05, y=0.9, s=text, transform=ax.transAxes)
    fit_x = np.sort(performance)
    fit_y = fit_x * m + b
    ax.plot(fit_x, fit_y, color='gray')
    ax.set_xlabel('Total points in experiment')
    ax.set_ylabel('Frequency of safe responses')
    ax.set_ylim([0, 1])
    ax.set_xlim(auto=True)
    if flag_show:
        plt.draw()
        plt.show(block=False)
    print(fit_x)


def basic_stats(participants=None, fignum=5):
    """Calls overall_prob_risky and performance and draws
    them in subplots. Makes pancakes.
    """
    fig = plt.figure(fignum)
    fig.clear()

    ax1 = fig.add_subplot(2, 1, 1)
    overall_prob_risky(participants=participants, ax=ax1)

    ax2 = fig.add_subplot(2, 1, 2)
    performance(participants=participants, ax=ax2)

    plt.tight_layout()
    plt.show(block=False)


def choice_vs_points_part(participants=None, transitions=None,
                          conditions=None, hue=False,
                          no_match=True, plot_avg=True, fit=True,
                          data=None,
                          axes=None, fignum=6):
    """Plots, for every transition type in --transitions--, the average
    choice made as a function of the number of unmatched points.

    All participants in --participants are plotted together, with their
    data all mashed together. A subplot is created for every
    combination of condition and transition type.

    Parameters
    ----------
    participants : int or array-like
    Participant(s) to be used for the plots. If a single participant
    is provided, the plot can be used for intra-participant
    analysis. Defaults to all participants for whom there is data in
    the exp_data folder.

    transitions : int or array-like
    Type of transitions to use. These are labelled according to
    import_data.import_data's scheme: 0 - nothing matches, 1 - colors
    match, 2 - risks match, 3 - everything matches.  Defaults to all
    transition types in the data.

    conditions : float or array-like
    Conditions to use, referring to how reliable the next-mblock cues
    are. Defaults to all conditions in the data.

    hue : None or bool
    If True, the dots for participants will have different colors,
    controlled by sns.

    plot_avg : bool
    Whether to plot a line with the averages for each risky_points point.

    fit : bool
    Whether to fit, using staatsmodels, the sigmoid to the data and plot it.
    Takes a while.

    no_match : bool
    Whether to remove trials in which the participant could match points.

    axes : np.array of matplotlib axis objects
    Axes to use for plotting. If None, new axes are created. A
    ValueError is raised if the number of axes in --axes-- is not
    enough to plot all conditions and transitions.

    fignum : integer
    Figure number if --axes-- is None.

    """
    if hue:
        hue = 'participant'
    else:
        hue = None
    labels = import_data.label_transitions()
    if transitions is None:
        transitions = np.arange(8)
    else:
        try:
            transitions[0]
        except TypeError:
            transitions = [transitions]
    if data is None:
        data = import_data.import_data(participants, no_match=no_match)
        # data = data.query('condition == 0')
    if participants is None:
        participants = np.unique(data.index)
    data = data.query('participant in @participants')
    if conditions is None:
        conditions = np.unique(data['condition'])
    else:
        try:
            conditions[0]
        except TypeError:
            conditions = [conditions]
    num_conds = len(conditions)
    subplots_conds = calc_subplots(num_conds)
    num_trans = len(transitions)
    subplots_trans = calc_subplots(num_trans)[::-1]
    flag_show = False
    if axes is None:
        fig = plt.figure(fignum)
        fig.clear()
        outer_gs = fig.add_gridspec(*subplots_trans)
        inner_gs = [outer.subgridspec(*subplots_conds) for outer in outer_gs]
        axes = np.array([[fig.add_subplot(inner) for inner in innr]
                         for innr in inner_gs])
        flag_show = True
    if axes.size < len(transitions) * len(conditions):
        raise ValueError('The number of axes does not match the number '
                         'of plots '
                         'to create, according to --transitions-- and '
                         '--conditions--')
    for ix_trans, transition in enumerate(transitions):
        for ix_cond, condition in enumerate(conditions):
            axis = axes[ix_trans, ix_cond]
            # axis.set_xlim([-1, 83])
            cond_text = 'condition == @condition and trans_type == @transition'
            datum = data.query(cond_text)
            if ix_cond == 0:
                axis.set_title(labels[transition])
            if plot_avg:
                sns.lineplot(data=datum, x='risky_points', y='choice',
                             hue=hue, ax=axis, legend=False, markers=['x'])
            if fit:
                sns.regplot(data=datum, x='risky_points', y='choice',
                            logistic=True, n_boot=500, y_jitter=0.1, ax=axis)
            axis.set_ylim([0, 1])
            # axis.set_xlim([0, 18])
            if ix_cond == len(conditions) - 1:
                axis.set_xlabel('Risky points')
            else:
                axis.set_xlabel('')
                axis.set_xticks([])
            axis.set_ylabel('Cond = {}\nChoice'.format(condition))
    if flag_show:
        fig.tight_layout()
        plt.draw()
        plt.show(block=False)


def loop_choice_vs_points_part(participants=None, **kwargs):
    """ """
    for part in participants:
        fig, axes = plt.subplots(1, 1, clear=True, squeeze=False)
        choice_vs_points_part(participants=[part], axes=axes, **kwargs)

def choice_vs_points_line(participants=None, transitions=None,
                          conditions=None, split_by=None,
                          no_match=True,
                          axes=None, fignum=7):
    """Plots, for every participant in --participants--, their average
    choice for every combination of risky_points, transition type and
    condition.

    All participants are plotted together and subplots are created for
    either every transition or for every condition, depending on
    --split_by--.

    Parameters
    ----------
    participants : int or array-like
    Participant(s) to be used for the plots. If a single participant
    is provided, the plot can be used for intra-participant
    analysis. Defaults to all participants for whom there is data in
    the exp_data folder.

    transitions : int or array-like
    Type of transitions to use. These are labelled according to
    import_data.import_data's scheme: 0 - nothing matches, 1 - colors
    match, 2 - risks match, 3 - everything matches.  Defaults to all
    transition types in the data.

    conditions : float or array-like
    Conditions to use, referring to how reliable the next-mblock cues
    are. Defaults to all conditions in the data.

    split_by : str in {'transition', 'condtion'}
    Whether to make subplots for each transition or condition. Defaults
    to 'transition'.

    no_match : bool
    Whether to remove trials in which the participant could match points.

    axes : np.array of matplotlib axis objects
    Axes to use for plotting. If None, new axes are created. A
    ValueError is raised if the number of axes in --axes-- is not
    enough to plot all conditions and transitions.

    fignum : integer
    Figure number if --axes-- is None.

    """
    types = np.array(['trans_type', 'condition'])
    hues = np.array(['trans_label', 'condition'])
    if split_by is None:
        split_by = 'transition'
    if split_by == 'condition':
        types = types[::-1]
        hues = hues[::-1]
    if transitions is None:
        transitions = np.arange(8)
    else:
        try:
            transitions[0]
        except TypeError:
            transitions = [transitions]
    data = import_data.import_data(participants, no_match=no_match)
    participants = np.unique(data.index)
    if conditions is None:
        conditions = np.unique(data['condition'])
    else:
        try:
            conditions[0]
        except TypeError:
            conditions = [conditions]
    flag_show = False
    if split_by == 'condition':
        num_subplots = len(conditions)
        subplot_types = conditions
    elif split_by == 'transition':
        num_subplots = len(transitions)
        subplot_types = transitions
    else:
        raise ValueError('--split_by-- was not recognized')
    if axes is None:
        subplots = calc_subplots(num_subplots)[::-1]
        fig, axes = plt.subplots(*subplots, num=fignum, clear=True,
                                 squeeze=True)
        axes = axes.reshape(-1)
        flag_show = True
    else:
        num_subplots = len(axes)
    # data = data.groupby(['trans_label', 'condition', 'risky_points']).mean()
    # data.reset_index(['trans_label', 'condition',
    #                   'risky_points'], inplace=True)
    # Hack because seaborn's hue is retarded:
    if split_by == 'transition':
        data['condition'] = [":{}".format(x) for x in data['condition']]
    labels = {'trans_type': import_data.label_transitions(),
              'condition': {condition: condition for condition in conditions}}
    xmax = 0
    for ix_plot, thing in enumerate(subplot_types):
        datum = data.query('{} == @thing'.format(types[0]))
        axis = axes[ix_plot]
        sns.lineplot(data=datum, x='risky_points', y='choice',
                     hue=hues[1], ax=axis, legend='full')
        axis.set_title(labels[types[0]][thing])
        axis.set_ylim([0, 1])
        xmax = max(axis.get_xlim()[1], xmax)
    for axis in axes:
        axis.set_xlim([0, xmax])
    fig.tight_layout()
    if flag_show:
        plt.draw()
        plt.show(block=False)

def loop_diff_cond():
    '''annoying little loop that will print both the fitted means so that I
    can save about 5 seconds in the future'''

    versions = ['center','slope']
    inferred_parameters = pd.read_pickle('all_inferred_parameters.pi')
    
    for version in versions:
        diff_fitted_means_cond(inferred_parameters, versions)


def scatter_points_vs_risky(condition, transition, participants=None,
                            fignum=13):
    """Scatter plots of average choice per points made by all participants in
    --participants--.  One subplot per participant.

    """
    sigmoids = part_diff_sigmoids(participants=participants,
                                  condition=condition,
                                  transition=transition, plot=False)
    title = 'Cond: {}, Trans: {}'.format(condition, transition)
    data = import_data.import_data(participants)
    if transition != -1:
        cond_str = 'trans_type == @transition and condition == @condition'
    else:
        cond_str = 'condition == @condition'
    data = data.query(cond_str)
    if participants is None:
        participants = np.unique(data.index)
    means = data.groupby(['participant', 'risky_points']).mean()
    means.reset_index('risky_points', inplace=True)
    counts = data.groupby(['participant', 'risky_points']).count()['choice']
    num_subplots = len(participants)
    subplots = calc_subplots(num_subplots)[::-1]

    fig, axes = plt.subplots(*subplots, clear=True, num=fignum,
                             squeeze=False, sharex=True, sharey=True)
    fig.suptitle(title)
    axes = axes.reshape(-1)
    for ix_part, participant in enumerate(participants):
        axes[ix_part].set_title(participant)
        norm_counts = counts / max(counts.loc[participant])
        one_rp = np.array(means.loc[participant]['risky_points'], ndmin=1)
        one_choice = np.array(means.loc[participant]['choice'], ndmin=1)
        all_count = norm_counts.loc[participant]
        for ix_point in range(one_rp.size):
            # ipdb.set_trace()
            axes[ix_part].scatter(one_rp[ix_point],
                                  one_choice[ix_point],
                                  alpha=all_count.iloc[ix_point],
                                  color='black', s=100)
        axes[ix_part].plot(sigmoids[participant])
        # axes[ix_part].scatter(x=means.loc[participant]['risky_points'],
        #                       y=means.loc[participant]['choice'],
        #                       color=counts.loc[participant])
    # plt.tight_layout()
    plt.show(block=False)


def loop_scatter_points_vs_risky(participants=None, base_fignum=1000):
    """Calls scatter_points_vs_risky() for every combination of condition and
    transition types.

    """
    transitions = np.arange(8)

    loop_indices = it.chain(it.product([0], transitions), [[0.75, -1]])

    for idx, one in enumerate(loop_indices):
        scatter_points_vs_risky(*one, participants=participants,
                                fignum=base_fignum + idx)

    
def choice_vs_points_diff(participants=None, transitions=None,
                          conditions=None, no_match=True,
                          axes=None, fignum=8):

    """Plots, for every participant in --participants--, the absolute difference in
    the average choice as a function of the unmatched points. Each transition
    type is plotted separately. Each dot represents one participant.

    Parameters
    ----------
    participants : int or array-like
    Participant(s) to be used for the plots. If a single participant
    is provided, the plot can be used for intra-participant
    analysis. Defaults to all participants for whom there is data in
    the exp_data folder.

    transitions : int or array-like
    Type of transitions to use. These are labelled according to
    import_data.import_data's scheme: 0 - nothing matches, 1 - colors
    match, 2 - risks match, 3 - everything matches.  Defaults to all
    transition types in the data.

    conditions : array-like
    Conditions to use, referring to how reliable the next-mblock cues
    are. Defaults to all conditions in the data. Must contain two elements,
    referring to the reliability of the condition (e.g. (0, 0.6)). Defaults
    to (0, 0.6).

    no_match : bool
    Whether to remove trials in which the participant could match points.

    axes : np.array of matplotlib axis objects
    Axes to use for plotting. If None, new axes are created. A
    ValueError is raised if the number of axes in --axes-- is not
    enough to plot all conditions and transitions.

    fignum : integer
    Figure number if --axes-- is None.

    """
    if transitions is None:
        transitions = np.arange(8)
    else:
        try:
            transitions[0]
        except TypeError:
            transitions = [transitions]
    data = import_data.import_data(participants, no_match=no_match)
    participants = np.unique(data.index)
    if conditions is None:
        conditions = np.array((0, 0.6))
    flag_show = False
    num_subplots = len(transitions)
    if axes is None:
        subplots = calc_subplots(num_subplots)[::-1]
        fig, axes = plt.subplots(*subplots, num=fignum, clear=True,
                                 squeeze=True, sharex=True, sharey=True)
        axes = axes.reshape(-1)
        flag_show = True
    elif num_subplots != len(axes):
        raise ValueError('The number of axes in --axes-- does not match '
                         'the number of transitions.')
    data = data.groupby(['participant', 'trans_type',
                         'condition', 'risky_points']).mean()
    data.reset_index(['trans_type'], inplace=True)
    labels = import_data.label_transitions()
    xmax = 0
    for ix_plot in range(num_subplots):
        datum = data.query('trans_type == @ix_plot').loc[:, ['choice']]
        strc_1 = 'condition == @conditions[0]'
        strc_2 = 'condition == @conditions[1]'
        difference = (datum.query(strc_1).reset_index(['condition']) -
                      datum.query(strc_2).reset_index(['condition'])).abs()
        difference.reset_index(['risky_points'], inplace=True)
        difference.reset_index(['participant'], inplace=True)

        # Hack because seaborn's hue is retarded:
        difference['participant'] = [":{}".format(
            x) for x in difference['participant']]

        axis = axes[ix_plot]
        sns.scatterplot(data=difference, x='risky_points', y='choice',
                        hue='participant', ax=axis, legend=False)
        axis.set_ylim([0, 1])
        axis.set_title(labels[ix_plot])
        xmax = max(axis.get_xlim()[1], xmax)
    # for axis in axes:
    #     axis.set_xlim([0, xmax])
    fig.tight_layout()
    if flag_show:
        plt.draw()
        plt.show(block=False)
    return data


def choice_diff_violin(participants=None, transitions=None,
                       conditions=None, no_match=True,
                       axis=None, fignum=9):
    """One violin plot for every combination of unmatched points and transition
    type, where the data points for the violin plot are the difference in the
    average choice between the two conditions in --conditions--.

    Parameters
    ----------
    participants : array-like
    Participants to use. Note that the numbers provided must match those
    of the data files in the data folder, with file names data_XX.pi. Defaults
    to all participants for whom there is a data file.

    transitions : int or array-like
    Type of transitions to use. These are labelled according to
    import_data.import_data's scheme: 0 - nothing matches, 1 - colors
    match, 2 - risks match, 3 - everything matches.  Defaults to all
    transition types in the data.

    conditions : array-like
    Conditions to use, referring to how reliable the next-mblock cues are. Must
    contain two elements, referring to the reliability of the condition
    (e.g. (0, 0.6)). Defaults to (0, 0.75).

    no_match : bool
    Whether to remove trials in which the participant could match points.
    Defaults to True.

    axis : np.array of matplotlib axis objects
    Axis to use for plotting. If None, a new one is created.

    fignum : integer
    Figure number if --axis-- is None.

    """
    if transitions is None:
        transitions = np.arange(8)
    else:
        try:
            transitions[0]
        except TypeError:
            transitions = [transitions]
    data = import_data.import_data(participants, no_match=no_match)
    participants = np.unique(data.index)
    if conditions is None:
        conditions = np.array((0, 0.75))
    flag_show = False
    if axis is None:
        fig, axis = plt.subplots(1, 1, num=fignum, clear=True,
                                 squeeze=True, sharex=True, sharey=True)
        flag_show = True
    #ipdb.set_trace()
    data = data.groupby(['participant', 'trans_label',
                         'condition', 'risky_points']).mean()
    strc_1 = 'condition == @conditions[0]'
    strc_2 = 'condition == @conditions[1]'
    difference = (data.query(strc_1).reset_index(['condition']) -
                  data.query(strc_2).reset_index(['condition'])).abs()
    difference.reset_index(
        ['risky_points', 'participant', 'trans_label'], inplace=True)

    difference = difference.query('risky_points <= 27')
    difference = difference.astype({'choice': float})
    # difference.loc[:, 'choice'] = difference['choice'].astype(float)

    # sns.violinplot(data=difference, x='risky_points', y='choice',
    #                hue='trans_label',
    #                ax=axis, cut=0, inner='points')
    pal = sns.color_palette(palette='Spectral')
    #pal = sns.cubehelix_palette(5, rot=-.55, light=.7)
    sns.boxenplot(data=difference, x='risky_points', y='choice', hue='trans_label', ax=axis, palette=pal)
    # '''boxen looks nicer than the violinplot and also i like the colours better''''

    if flag_show:
        plt.show(block=False)
    return difference


def diff_fitted_means_cond(data, parameter, tt_labels=None):
    """Shows the effect of the conditions on the inferred parameters by plotting the
    inferred parameters as a scatter diagram. If multiple conditions are present
    in --data--, they are plotted as a dot-line-dot plot (sns.catplot) to show
    the effect of the conditions.

    Parameters
    ----------
    data : DataFrame
    Contains the inferred parameters. Output of anal.infer_parameters().

    parameter : string
    Name of the parameter to plot. "slope" or "mean".

    tt_labels : array
    Don't remember what this is.

    """
    df_means = data.groupby(['participant', 'condition', 'trans_type']).mean()
    df_means.reset_index(inplace=True)

    if tt_labels is not None:
        for index, item in enumerate(df_means.loc[:, 'trans_type']):
            df_means.loc[index, 'trans_type'] = tt_labels[int(item)]

    sns.catplot(x='condition', y=parameter, hue='participant',
                kind='point', data=df_means, dodge=True,
                legend=True)

def part_diff_sigmoids(condition, transition, participants=None, inferred=None,
                       axis=None, plot=True, fignum=10):
    """Shows the inferred sigmoids for all --participants-- separately (one
    line per participant) for --condition-- and --transition--, all in one
    figure.

    Returns the points to plot.

    """
    if axis is None and plot:
        fig, axis = plt.subplots(1, 1, num=fignum, clear=True,
                                 squeeze=True, sharex=True, sharey=True)
        flag_show = True
    if inferred is None:
        # with open('./results_inference/inferred_parameters.pi', 'rb') as mafi:
        #     inferred = pickle.load(mafi)
        inferred = pd.read_pickle('all_inferred_parameters.pi')
    inferred = inferred.query('condition == @condition and '
                              'trans_type == @transition')
    inferred = inferred.groupby('participant').mean()
    if participants is None:
        participants = np.unique(inferred.index)

    support = np.arange(54)
    sigmoid = lambda m, x0: 1 - 1 / (1 + np.exp(m * (support - x0)))
    out_sigmoids = {}
    for ix_part, part in enumerate(participants):
        try:
            slope, center = inferred.loc[part, ['slope', 'center']]
        except KeyError:
            pass
        out_sigmoids[part] = sigmoid(slope, center)
        if plot:
            axis.plot(out_sigmoids[part], alpha=0.5)
    return out_sigmoids


def loop_part_diff_sigmoids(conditions=None, transitions=None,
                            participants=None,
                            inferred=None, axes=None, fignum=11):
    """Loops over part_diff_sigmoids, plotting each condition(x)transition into
     a different subplot.

    """
    if inferred is None:
        # with open('./results_inference/inferred_parameters.pi', 'rb') as mafi:
        #     inferred = pickle.load(mafi)
        inferred = pd.read_pickle('all_inferred_parameters.pi')
    if conditions is None:
        conditions = np.unique(inferred.index.levels[1])
    if participants is None:
        participants = np.unique(inferred.index.levels[0])
    if transitions is None:
        transitions = np.unique(inferred.index.levels[2])

    trans_cond = np.array(list(it.product(transitions, conditions)))
    if axes is None:
        fig, axes = plt.subplots(trans_cond.shape[0], num=fignum, clear=True,
                                 squeeze=False, sharex=True, sharey=True)
        axes = axes.reshape(-1)
    for ix_loop, (c_trans, c_cond) in enumerate(trans_cond):
        axis = axes[ix_loop]
        part_diff_sigmoids(condition=c_cond, transition=c_trans,
                           participants=participants, inferred=inferred,
                           axis=axis)


def diff_fitted_centers_trans(inferred, trans_types=(3, 7), axes=None,
                              fignum=12):
    """Shows the difference in the fitted centers between the transitions
    in --trans_groups--.

    """
    if axes is None:
        fig, axes = plt.subplots(1, 1, num=fignum, clear=True,
                                 squeeze=True)
    df_means = inferred.groupby(['trans_type', 'participant']).mean()
    df_means.reset_index(inplace=True)
    df_means = df_means.query('trans_type in @trans_types')

    sns.catplot(x='trans_type', y='center', hue='participant',
                kind='point', data=df_means, dodge=False,
                legend=False, ax=axes)


def diff_avg_risk_trans(data=None, trans_types=((0,1,2,3),(4,5,6,7)), group_labels=None,
                        axis=None, add_condition=False,
                        condition=0, kwargs_barplot=None, fignum=13):
    """Plots the difference in frequency of choices between the
    transition types in trans_type.

    Parameters
    ----------
    data : DataFrame
    Behavioral data. Output of import_data.import_data()

    trans_types : array-like
    Transitions to use. If an array of two ints, the first element is
    compared against the second one. If any of the elements of the array
    is an array itself, all its elements are groupped together. For example,
    [[1, 2], [3, 4]] will plot two bars, one for all the trials that are
    trans_type either 1 or 2, and one for 3 and 4.

    add_condition : bool
    If True, the condition 0.75 is also considered, by setting the trials
    of that condition to a transition type 8 or 9, depending on whether
    the miniblock was risky or safe (regardless of the next one or of its
    color). These new transition types can then be plotted alongside
    the existing 0-7 ones.
    """
    
    if data is None:
        data = import_data.import_all(data_sets=['behavioural'],no_timeouts=False)
    
    if kwargs_barplot is None:
        kwargs_barplot = {}
    if axis is None:
        fig, axis = plt.subplots(1, 1, num=fignum, clear=True,
                                 squeeze=True)

    if add_condition:
        cond_cond = data['condition'] == 0.75
        cond_safe = np.zeros(len(cond_cond), dtype=bool)
        cond_risk = np.zeros(len(cond_cond), dtype=bool)
        for ix_trial, mblock_type in enumerate(data['mblock_type']):
            cond_safe[ix_trial] = mblock_type.startswith('safe')
            cond_risk[ix_trial] = mblock_type.startswith('risky')
        data.loc[np.logical_and(cond_cond, cond_safe), 'trans_type'] = 9
        data.loc[np.logical_and(cond_cond, cond_risk), 'trans_type'] = 8
    else:
        data = data.query('condition == @condition')
    new_trans = []
    for ix_trans, one_group in enumerate(trans_types):
        try:
            one_new = 10 + ix_trans
            for one_trans in one_group:
                data.loc[data['trans_type'] == one_trans,
                         'trans_type'] = one_new
            new_trans.append(one_new)
        except TypeError:
            new_trans.append(one_group)
    data = data.query('trans_type in @new_trans')
    sns.barplot(data=data, x='trans_type', y='choice',
                ax=axis, **kwargs_barplot)
    if group_labels:
        axis.set_xticklabels(group_labels)
    axis.set_title('Behavior across transitions')
    axis.set_ylabel('Frequency of safe choice')
    axis.set_xlabel('Transition type')
    
def diff_conflict_trans(data=None, trans_types=((0,1,4,5),(2,3,6,7)), group_labels=None,
                        axis=None, add_condition=True,
                        condition=0, kwargs_barplot=None, fignum=13):
    """Plots the difference in level of conflict between the
    transition types in trans_type.

    Parameters
    ----------
    data : DataFrame
    Behavioral data. Output of import_data.import_data()

    trans_types : array-like
    Transitions to use. If an array of two ints, the first element is
    compared against the second one. If any of the elements of the array
    is an array itself, all its elements are groupped together. For example,
    [[1, 2], [3, 4]] will plot two bars, one for all the trials that are
    trans_type either 1 or 2, and one for 3 and 4.

    add_condition : bool
    If True, the condition 0.75 is also considered, by setting the trials
    of that condition to a transition type 8 or 9, depending on whether
    the miniblock was risky or safe (regardless of the next one or of its
    color). These new transition types can then be plotted alongside
    the existing 0-7 ones.
    """
    
    if data is None:
        data = anal.append_conflict_panda()
    
    if kwargs_barplot is None:
        kwargs_barplot = {}
    if axis is None:
        fig, axis = plt.subplots(1, 1, num=fignum, clear=True,
                                 squeeze=True)

    if add_condition:
        cond_cond = data['condition'] == 0.75
        cond_safe = np.zeros(len(cond_cond), dtype=bool)
        cond_risk = np.zeros(len(cond_cond), dtype=bool)
        for ix_trial, mblock_type in enumerate(data['mblock_type']):
            cond_safe[ix_trial] = mblock_type.startswith('safe')
            cond_risk[ix_trial] = mblock_type.startswith('risky')
        data.loc[np.logical_and(cond_cond, cond_safe), 'trans_type'] = 9
        data.loc[np.logical_and(cond_cond, cond_risk), 'trans_type'] = 8
    else:
        data = data.query('condition == @condition')
    new_trans = []
    for ix_trans, one_group in enumerate(trans_types):
        try:
            one_new = 10 + ix_trans
            for one_trans in one_group:
                data.loc[data['trans_type'] == one_trans,
                         'trans_type'] = one_new
            new_trans.append(one_new)
        except TypeError:
            new_trans.append(one_group)
    data = data.query('trans_type in @new_trans')
    sns.barplot(data=data, x='trans_type', y='conflict',
                ax=axis, **kwargs_barplot)
    if group_labels:
        axis.set_xticklabels(group_labels)
    axis.set_title('Choice Conflict across Transitions')
    axis.set_ylabel('Choice Conflict')
    axis.set_xlabel('Transition type')



def diff_avg_risk_cond(data=None, axis=None, kwargs_barplot=None, fignum=14):
    """Plots the difference in frequency of choices between the
    transition types in trans_type.

    """
    if data is None:
        data = import_data.import_all(data_sets=['behavioural'],no_timeouts=False)
        
    if kwargs_barplot is None:
        kwargs_barplot = {}
    if axis is None:
        fig, axis = plt.subplots(1, 1, num=fignum, clear=True,
                                 squeeze=True)
    sns.barplot(data=data, x='condition', y='choice',
                ax=axis, **kwargs_barplot)
    axis.set_xticklabels(['No uncertainty', 'High uncertainty'])
    axis.set_title('Behavior across conditions')
    axis.set_ylabel('Frequency of safe choice')
    axis.set_xlabel('Condition')
    
def diff_conflict_cond(data=None, axis=None, kwargs_barplot=None, fignum=14):
    """Plots the difference in the conflict levels between the
    experimental conditions.

    """
    if data is None:
        data = anal.append_conflict_panda()
        
    if kwargs_barplot is None:
        kwargs_barplot = {}
    if axis is None:
        fig, axis = plt.subplots(1, 1, num=fignum, clear=True,
                                 squeeze=True)
    sns.barplot(data=data, x='condition', y='conflict',
                ax=axis, **kwargs_barplot)
    axis.set_xticklabels(['No uncertainty', 'High uncertainty'])
    axis.set_title('Choice Conflict across Conditions')
    axis.set_ylabel('Choice Conflict')
    axis.set_xlabel('Condition')


def stefan_sfb_poster(fignum=15, data=None):
    """Figure for Stefan's poster for the third phase of the sfb940.

    """
    fig, axes = plt.subplots(2, 2, num=fignum, clear=True, sharey=True,
                             sharex=False)
    axes = axes.reshape(-1)
    
    if data is None:
        data = import_data.import_all(data_sets=['behavioural'],no_timeouts=False)
    else:
        data = data
        
    kwargs_barplot = {'palette': (sns.xkcd_rgb["faded purple"],
                                  sns.xkcd_rgb["light tan"])}
    diff_avg_risk_trans(data=data, trans_types=((0, 1, 2, 3), (4, 5, 6, 7)),
                        group_labels=('Same color', 'Different colors'),
                        axis=axes[0], kwargs_barplot=kwargs_barplot)
    diff_avg_risk_trans(data=data, trans_types=((2, 3, 6, 7), (0, 1, 4, 5)),
                        group_labels=('Safe context',
                                      'Risky context'),
                        axis=axes[1], kwargs_barplot=kwargs_barplot)
    axes[1].set_title('Behavior over risk levels')
    diff_avg_risk_cond(data=data, axis=axes[2], kwargs_barplot=kwargs_barplot)
    labels = ('s_rr', 's_rs', 's_sr', 's_ss', 'd_rr',
              'd_rs', 'd_sr', 'd_ss', 'HU')
    kwargs_barplot = {'palette': sns.cubehelix_palette(9, start=0.5, rot=-0.75)}
    diff_avg_risk_trans(data=data, trans_types=np.arange(9),
                        add_condition=True, axis=axes[3], group_labels=labels,
                        kwargs_barplot=kwargs_barplot)
    axes[3].set_xticklabels(axes[3].get_xticklabels(), rotation=45)
    axes[3].set_title('Behavior for all transitions')
    for axis in axes[:-1]:
        axis.set_xlabel('')


def stefan_sfb_poster_v2(fignum=16):
    """Second version of the poster figure.

    """
    data = import_data.import_data()
    kwargs_barplot = {'palette': (sns.xkcd_rgb["grey/green"], sns.xkcd_rgb["grey/green"], sns.xkcd_rgb["grey/green"], sns.xkcd_rgb["grey/green"],
                                  sns.xkcd_rgb["dull teal"], sns.xkcd_rgb["dull teal"], sns.xkcd_rgb["dull teal"], sns.xkcd_rgb["dull teal"],
                                  sns.xkcd_rgb["stormy blue"], sns.xkcd_rgb["stormy blue"])}
    labels = ('s_rr', 's_rs', 's_sr', 's_ss', 'd_rr',
              'd_rs', 'd_sr', 'd_ss', 'HU_r', 'HU_s')

    diff_avg_risk_trans(data=data, trans_types=np.arange(10),
                        add_condition=True, group_labels=labels,
                        kwargs_barplot=kwargs_barplot)


# def plot_mcmc_samples(samples, participants=None, conditions=None, trans_types=None):
#     """Visualizes the posterior distribution over parameters given the data.

#     Parameters
#     ----------
#     samples: string or pd.DataFrame
#     Filename (if string) or DataFrame containing the samples for all the participants.

#     participants: array-like of ints
#     Participants for whom to plot. Participants not in the list of samples are ignored and
#     warning is raised. Defaults to all participants in --samples--.

#     conditions: array-like of floats
#     Conditions to use. Combinations of 0 and 0.75. Defaults to all conditions in --samples--.

#     trans_types: array-like of ints
#     Transition types to use, indexed from 0 to 7, inclusive. Defaults to all transition types
#     in --samples--.

#     Returns
#     -------
#     axes: plt.Axes
#     Or sumethin

#     """
#     if isinstance(samples, str):
#         pandata = pd.read_pickle(samples)
#     else:
#         pandata = samples  # quack!

#     if participants is None:
#         participants = np.unique(pandata.index.levels[0])

#     if conditions is None:
#         conditions = 
