# -*- coding: utf-8 -*-

"""Agent skeleton. No longer contained to closet area. Alarm level
rised from 7 to Purple.  Mike knows what that means.

Notes:
(1) States are N**2-dimensional, where N is the maximum number of points
    we are considering. I think 30 is a good number.

(2) There are (R + 1) transition matrices, where R is the number of possible
    offers. However, we decided to make an agent (Florian) which only considers
    a sort of "average" offer in the future, so that one should be used
    to predict future states, except when predicting S_{t+1}, because in
    for that one Florian knows exactly what the offer is, so that transition
    matrix should be used.

(3) In the_game, all_matched is a number, because there is no uncertainty on
    how many points have been matched. The agents, when trying to predict
    future states, need to have uncertainty, so all_matched becomes a vector
    there. In principle, you could do the same with curr_state, and I didnt
    do it only because I hadn't thought of it and now it's too late, it's time
    for me to push the daisies.

"""


def the_game():
    """Main function to run the entire game, with many miniblocks, balloon
    animals and cocaine.

    Note that --curr_state-- refers mostly to unmatched points. Also
    note that both all_matched and curr_state persist through
    miniblocks. Also also note that all_matched is just a number. This
    is different from make_decision_florian() and other functions
    below, which need it to be a vector (probability distribution). If
    this makes you uncomfortable, you can call them different names,
    dress them with different colors and take them to different
    psychiatrists when you ruin their lives with your unrequitted love.

    """
    pars = get_game_parameters()
    all_matched = 0
    curr_state = initial_state  # whatever this is
    for mblock in range(pars['mblocks']):
        curr_type, next_type = get_mblock_type(mblock)
        for trial in range(pars['trials']):
            log_state[trial, :] = curr_state  # Save state first
            offer = sample_offer("whatever is needed")
            decision = make_decision("whatever is needed")
            log_decision[trial] = decision
            curr_state, matched_points = sample_next_state(decision,
                                                           curr_state)
            all_matched = all_matched + matched_points
    return all_logs


def get_game_parameters():
    """Function where all game parameters are stored. In a dictionary, for
    example."""
    pass


def get_mblock_type(mblock):
    """Function that knows which mini-block type to return for every
    --mblock--, where --mblock-- can be assumed to be an index for
    simplicity. This doesn't actually need to be a function, but could
    be simply a vector loaded from a file.
    """
    pass


def sample_offer("whatever is needed"):
    """Function that takes some contextual information (like curr_type)
    and returns a risky offer. Preferably in the form (red, blue), as
    we have been doing so far.
    """
    pass


def sample_next_state(decision, curr_state):
    """Returns what the new state is, given --decision-- and
    --curr_state--. If the state space is just (blue, red), then
    it returns (blue, red) + (accepted_blue, accepted_red).
    """
    pass


def make_decision(agent_name, *args, **kwargs):
    """This is where the agent lives. The inputs should be everything
    that the agent needs in order to make a decision for a single trial.
    The output should be that decision, either as a boolean (0 == risky,
    1 == safe) or as a string. Whatever it is, it needs to be consistent
    with sample_next_state().

    --agent_name-- should would be a string which tells the function
    which agent to use. For now, we have things like Eric and Florian.
    """

    if agent_name == 'Eric':
        return make_decision_eric(*args, **kwargs)
    elif agent_name == 'Florian':
        return make_decision_florian(*args, **kwargs)
    else:
        raise ValueError('Yo, what the fuck!')


def make_decision_florian(*args, **kwargs):
    """This is the function we have been working on the past few days.
    Takes as input all the information Florian needs to make a choice
    for a single trial and returns the choice.
    """
    trans_mats = get_trans_mats(whatever)  # we have this already

    all_pies = get_action_sequences(whatever)  # we have this already

    for ix_pie, pie in enumerate(all_pies):
        predicted_unmatched, predicted_matched = \
            predict_final_state_florian(pie, curr_state)
        pie_eval[ix_pie] = evaluate_state_florian(predicted_unmatched,
                                                  predicted_matched)

    best_pie = choose_best_pie_florian(pie_eval)

    return best_pie[0]   # Returns the first action of the best pie


def predict_final_state_florian(pie, curr_state, trans_mats):
    """Takes the current state in --curr_state-- and predicts what
    the probability distribution over states will be after the
    entire action sequence --pie-- has been applied.

    If S_t = curr_state, then it predicts S_{t+1}, S_{t+2}, ...,
    S_{T}. It returns S_{T}.

    Note that --curr_state-- is an N-dimensional vector of floats,
    where N is the number of states. This vector represents a probability
    distribution over states. For example, if the agent is sure that
    he is in state number 3, --curr_state-- will be a bunch of zeros
    with a single 1 in position 3. If the agent has no fucking idea
    where he is, it'd be 1 / N * np.ones(N).

    Also, note that to limit computation times, the process of predicting
    S_{t+1} from S_t, given pi(t) has two steps: First multiply S_{t} by
    B[t], where B[t] is the transition matrix for action pi[t]; second match
    the points.

    This all means that the state S_t actually has two components: the
    unmatched points, (blue, red), and the matched points. If X is the
    maximum number of points we're considering (30 or so), then the
    size of the unmatched points vector is X**2 and the size of the
    matched points vector is X (could even be X / 2 if we want, but there's
    no point in keeping that one small).

    Note that this function should return both matched and unmatched,
    because both are necessary to evaluate how good an action sequence is.

    A final warning, for the end of times is nigh. Florian only considers
    an "average" future offer, because he doesn't know which offers will
    be offered in the future and doesn't want to think too much about it.
    So, B[action] is either the safe one or the "average" one, except when
    predicting S_{t+1}. This is because Florian knows what the current offer
    is, so he can use that transition matrix instead of the average one.

    """
    next_step = curr_state
    all_matched = np.zeros(len(curr_state))
    for action in pie:
        # First step
        # Select the appropriate transition matrix, which depends on --action--
        # and on whether it's the first action in pie or not. If it's the first
        # action, take the current offer; if not, take the average offer:
        c_trans_mat = "something"
        next_state = c_trans_mat.dot(next_step)

        # Second step
        next_state, matched = match_points(next_state)  # We have this already

        # Now, add the new matched points in --matched-- to the previous
        # matched points and re-normalize, because it should be a
        # probability distribution:
        all_matched = all_matched + matched
        all_matched = all_matched / all_matched.sum()
    return new_state, all_matched


def evaluate_state_florian(unmatched, matched):
    """Evaluates how good a state is in terms of number of points. For now,
    let's keep this simple by doing an expected average over points,
    counting matched points twice, as they represent both blue and red points.

    Returns a single number. The higher, the better.
    """
    pass


def choose_best_pie_florian(pie_eval):
    """Samples a pie, given the evaluations (predicted expected values) in
    --pie_eval--.

    This could be something as simple as choosing the one with the maximum
    value, but to add some variability, you can also run --pie_eval-- through
    a softmax function, which returns a probability distribution, and then
    sample from the distribution.
    """
    pass
