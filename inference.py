# -*- coding: utf-8 -*-
# ../inference.py

"""Functions for running pymc3-inference on data, as well as other bad
things.

"""
from itertools import product
import logging

import pymc3 as pm
import numpy as np
# from theano import tensor as tt
from pymc3.parallel_sampling import ParallelSamplingError
import pandas as pd
from theano import tensor as tt

malogger = logging.getLogger(__name__)


def define_model(data, for_data=False):
    """Defines the pymc3 model for the sigmoids agent.

    The data in --data-- is assumed to be for a single sigmoid (i.e. for a
    single transition type).

    """
    if for_data:
        slope_sd = 0.001
        center_sd = 0.1
    else:
        slope_sd = 2
        center_sd = 20
    model = pm.Model()
    risky_points = data['risky_points'].values
    choices = data['choice'].values
    with model:
        center = pm.Normal('center', mu=9, sd=center_sd)
        slope = pm.Normal('slope', mu=0.2, sd=slope_sd)
        variable = slope * (risky_points - center)
        sigmoid = pm.math.sigmoid(variable)
        if for_data:
            choices = pm.Bernoulli('choices', sigmoid,
                                   shape=len(choices))
        else:
            choices = pm.Bernoulli('choices', sigmoid,
                                   observed=choices, testval=choices)
    return model


def define_behavioral_bold_model(beha_data, fmri_data, for_data=False):
    """Does inference on the behavioral-bold hierarchical model. Maybe.

    As with define_model(), the data (beha_data and fmri_data) is assumed
    to be from a single sigmoid and will be treated as such.

    Additionally, the data is assumed to come from the import_data module,
    so that format is assumed.

    """
    fmri_risky_points = fmri_data['risky_points'].values
    if for_data:
        slope_sd = 0.001
        center_sd = 0.1
        beta0_sd = 0.1
        beta1_sd = 0.1
        bold_sd = 0.01
    else:
        slope_sd = 2
        center_sd = 20
        beta0_sd = 10
        beta1_sd = 10
        bold_sd = 1
    model = pm.Model()
    risky_points = beha_data['risky_points'].values
    with model:
        center = pm.Normal('center', mu=9, sd=center_sd)
        slope = pm.Normal('slope', mu=0.2, sd=slope_sd)
        variable = slope * (risky_points - center)
        sigmoid = pm.math.sigmoid(variable)
        if for_data:
            pm.Bernoulli('choices', sigmoid,
                         shape=len(risky_points))
        else:
            choices = beha_data['choice'].values
            pm.Bernoulli('choices', sigmoid,
                         observed=choices, testval=choices)
        p_of_safe = pm.math.sigmoid(slope * (fmri_risky_points - center))
        conflict_fmri = 1 - 2 * tt.abs_(p_of_safe - 0.5)
        beta0 = pm.Normal('beta0', mu=0, sd=beta0_sd)
        beta1 = pm.Normal('beta1', mu=1, sd=beta1_sd)
        mu_bold = beta0 + beta1 * conflict_fmri
        if for_data:
            pm.Normal('bold', mu=mu_bold, sd=bold_sd,
                      shape=len(fmri_risky_points))
        else:
            bold_data = fmri_data['bold'].values
            pm.Normal('bold', mu=mu_bold, sd=bold_sd,
                      observed=bold_data)
    return model


def infer_many(data, transitions=None, conditions=None,
               num_samples=1000, num_tune=2000, cores=8,
               retry=0, silent=True, fmri_data=None):
    """Runs inference on the many-sigmoid model by separating the data by
    conditions and transition types.

    Note that the indices of the DataFrame --data-, which contain participant
    numbers, are ignored, and the entire dataset is assumed to come from the
    same participant.

    Parameters
    ----------
    data : DataFrame
    DataFrame with columns ['choice', 'risky_points', 'condition',
    'trans_type']. Assumed to be for a single participant, and so the index is
    ignored. This is meant to come from import_data.import_data(), but any
    matching DataFrame works.

    num_samples : int
    NUmber of samples for MCMC inference.

    num_tune : int
    Number of tuning steps for NUTS to do its magic.

    retry : int
    Number of times to retry inference if it crashes. Note that this is number
    of retries, not total number of tries.

    fmri_data : DataFrame
    Behavioral data from the fmri session. If provided, the behavioral-bold
    model is used. If None, the purely behavioral model is used. It is assumed
    that it contains a column called 'bold' with the bold signal for each
    decision in the fmri data. Note that this need not be related to --data--
    in any way, and the only columns needed for this are 'bold' and
    'risky_points'.

    Returns
    -------
    traces : 2darray of objects
    Each row represents a transition type; each column a condition. Even if
    there is a single condition or transition in the data, the array is 2d.
    Each element is a MultiTrace pymc3 object.

    """
    if transitions is None:
        transitions = np.unique(data['trans_type'])
    if conditions is None:
        conditions = np.unique(data['condition'])

    looping = product(transitions, conditions)
    traces = np.empty((len(transitions), len(conditions)), dtype=object)
    labels = -np.ones((len(transitions), len(conditions), 2))
    for ix_loop, (c_trans, c_cond) in enumerate(looping):
        malogger.info('##### Running inference on trans_type {}, '
                      'condition {} #####'.format(c_trans, c_cond))
        ix_trans, ix_cond = np.unravel_index(ix_loop,
                                             (len(transitions),
                                              len(conditions)))
        labels[ix_trans, ix_cond, :] = [c_trans, c_cond]
        querying = 'trans_type == @c_trans and condition == @c_cond'
        datum = data.query(querying)
        if fmri_data is None:
            malogger.info('##### Using purely behavioral model. #####')
            model = define_model(datum)
        else:
            malogger.info('#### Using behavioral-bold model. #####')
            fmri_datum = fmri_data.query(querying)
            if len(fmri_datum) == 0:
                ipdb.set_trace()
            model = define_behavioral_bold_model(datum, fmri_datum)

        c_retry = 0
        while c_retry <= retry:
            if c_retry > 0:
                malogger.info('Trying inference again')
            c_retry += 1
            try:
                traces[ix_trans, ix_cond] = pm.sampling.sample(
                    num_samples, model=model, tune=num_tune,
                    progressbar=not silent, cores=cores,
                    return_inferencedata=False)
                c_retry = np.inf
            except ParallelSamplingError:
                traces[ix_trans, ix_cond] = []
                if c_retry > retry:
                    malogger.warning('Inference has gone and fucked up. '
                                     'Inference for this participant is '
                                     'incomplete.')
                else:
                    malogger.warning('Inference failed. Will try again.')

    return traces, labels


def infer_one(data, num_samples=1000, num_tune=2000, num_chains=8,
              retry=0, silent=True, fmri_data=None):
    """Runs inference on the one-sigmoid model. The entire dataset is used,
    regardless of conditions and transitions

    Note that the indices of the DataFrame --data-, which contain participant
    numbers, are ignored, and the entire dataset is assumed to come from the
    same participant.


    Parameters
    ----------
    data : DataFrame
    DataFrame with columns ['choice', 'risky_points', 'condition',
    'trans_type']. Assumed to be for a single participant, and so the index is
    ignored. This is meant to come from import_data.import_data(), but any
    matching DataFrame works.

    num_samples : int
    NUmber of samples for MCMC inference.

    num_tune : int
    Number of tuning steps for NUTS to do its magic.

    retry : int
    Number of times to retry inference if it crashes. Note that this is number
    of retries, not total number of tries.

    fmri_data : DataFrame
    Behavioral data from the fmri session. If provided, the behavioral-bold
    model is used. If None, the purely behavioral model is used. It is assumed
    that it contains a column called 'bold' with the bold signal for each
    decision in the fmri data. Note that this need not be related to --data--
    in any way, and the only columns needed for this are 'bold' and
    'risky_points'.


    Returns
    -------
    traces : MultiTrace
    Results from the MCMC inference.

    """
    malogger.info('##### Running inference on the single-sigmoid model '
                  '#####')
    if fmri_data is None:
        malogger.info('##### Using purely behavioral model. #####')
        model = define_model(data)
    else:
        malogger.info('##### Using behavioral-bold model. #####')
        model = define_behavioral_bold_model(data, fmri_data)
    c_retry = 0
    while c_retry <= retry:
        if c_retry > 0:
            malogger.info('Trying inference again.')
        c_retry += 1
        try:
            traces = pm.sampling.sample(num_samples,
                                        model=model,
                                        tune=num_tune,
                                        chains=num_chains,
                                        progressbar=not silent,
                                        return_inferencedata=False)
            c_retry = np.inf
        except ParallelSamplingError:
            traces = []
            if c_retry > retry:
                malogger.warning('Inference has gone and fucked up. '
                                 'Inference for this participant is '
                                 'incomplete.')
            else:
                malogger.warning('Inference failed. Will try again.')
    return traces


def extract_means(traces):
    """Returns a dictionary of the means of the variables in --traces--.

    Parameters
    ----------
    traces : pymc3 traces or numpy array of them
    Samples from the model. Can be the output of pm.sampling directly,
    or can be a numpy array of samples, as output by infer_many.

    Returns
    -------
    means : dict or numpy array of dicts
    Dictionary whose keys are the varnames in the traces and the values
    are the mean of the samples. A single dictionary is returned if
    --traces-- is a single trace; a numpy array of dictionaries is
    returned if --traces-- is a numpy array, in which case the returned
    array has the same shape as --traces--.
    """
    if not isinstance(traces, np.ndarray):
        means = {}
        for varname in traces.varnames:
            means[varname] = traces.get_values(varname).mean()
        return means
    shape = traces.shape
    traces = traces.reshape(-1)
    means = np.empty(traces.shape, dtype=dict)
    indices_traces = np.arange(len(traces))

    for ix_trace in indices_traces:
        c_means = {}
        for varname in traces[ix_trace].varnames:
            c_means[varname] = traces[ix_trace].get_values(varname).mean()
        means[ix_trace] = c_means
    return means.reshape(shape)


def concatenate_different_runs(filenames):
    """Concatenates the samples from different runs of inference into one big
    panda. The different runs are assumed to have been made with the same
    parameters, so if two files contain samples for the same index
    (participant, condition, transition), they will be placed together.

    """
    data = []
    for filename in filenames:
        # with open(filename, 'rb') as mafi:
        #     data.append(pickle.load(mafi))
        pandita = pd.read_pickle(filename)
        if len(pandita.index.levels) != 4:
            pandita['condition'] = 0.75
            pandita['trans_type'] = -1
            pandita.set_index(['condition', 'trans_type'], append=True,
                              inplace=True)
            pandita.reset_index(1, inplace=True, drop=True)
        else:
            pandita.reset_index(3, drop=True, inplace=True)
        data.append(pandita)
    big_panda = pd.concat(data,
                          levels=('participant', 'condition', 'trans_type'))
    return big_panda


def find_missing_data(inferred=None, participants=None, conditions=None,
                      transitions=None):
    """Finds the missing samples in the --inferred-- panda.  That is, it finds the
    (participant, condition, transition) tuple for which no samples are to be
    found. If no --participants--, --conditions-- and/or --transitions-- are
    provided, they are inferred from the data.

    Works for the many- and single-sigmoid models.

    Parameters
    ----------
    inferred : DataFrame or str
    If a string, it is assumed to be the name of the file.

    """
    if inferred is None:
        inferred = 'all_inferred_parameters.pi'
    if isinstance(inferred, str):
        # with open(inferred, 'rb') as mafi:
        #     inferred = pickle.load(mafi)
        inferred = pd.read_pickle(inferred)
    type_model = len(inferred.index.levels) == 2
    if participants is None:
        participants = np.unique(inferred.index.get_level_values(0))
    all_things = [participants]
    if not type_model:
        if conditions is None:
            conditions = np.unique(inferred.index.get_level_values(1))
        if transitions is None:
            transitions = np.unique(inferred.index.get_level_values(2))
        all_things.append(conditions)
        all_things.append(transitions)
    all_indices = set(product(*all_things))
    found_indices = set(inferred.index)
    return all_indices - found_indices


def infer_missing_data(data, inferred=None, **kwargs_infer):
    """Runs inference (infer_many/one) for the missing data indexed by
    --missing_data--. Each row in --missing_data-- is assumed to be
    the tuple (participant, condition, transition).

    The type of model is inferred from the values in --missing_data--
    for each row, so many- and one-sigmoid models can be mixed.

    **kwargs_infer are sent to infer_many/one.

    """
    raise NotImplementedError('Not finished! Perhaps never will.')
    if inferred is None:
        inferred = 'all_inferred_parameters.pi'
    if isinstance(inferred, str):
        # with open(inferred, 'rb') as mafi:
        #     inferred = pickle.load(mafi)
        inferred = pd.read_pickle(inferred)
    missing_data = find_missing_data(inferred)
    new_data = {}
    data = import_data.import_data()
    for missing_datum in missing_data:
        if len(missing_datum) == 3:
            datum = data.query('participant == @missing_datum[0]')
            results = infer_many(datum, transitions=[missing_datum[2]],
                                 conditions=[missing_datum[1]])
            means = extract_means(results)
