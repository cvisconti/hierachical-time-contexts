# -*- coding: utf-8 -*-
# ... import_data.py

"""Functions related to importing the experimental data from the files and
formatting to more agreeable structures.
"""
import os
import glob
import re
import pickle
import logging
import ntpath

import numpy as np
import pandas as pd

DIR = os.path.dirname(os.path.abspath(__file__))
DATA_FOLDER = os.path.abspath(DIR + '/exp_data/')

malogger = logging.getLogger(__name__)


def find_participants(data_folder=None, filename=None):
    """Searches the DATA_FOLDER (or --data_folder--, if provided) and determiens
    which participants have their data there. Returns a list of participant
    labels.

    Note that it assumes that the data files are in the format data_XX.pi,
    where XX is the participant number.
    """
    if data_folder is None:
        data_folder = DATA_FOLDER
    if filename is None:
        filename = _find_filename(data_folder).format('*')
    else:
        filename = filename.format('*')
    files = glob.glob(data_folder + filename)
    participants = []
    for one_file in files:
        found_file = ntpath.basename(one_file)
        part_number = re.search(r'[0-9]+', found_file)
        if part_number is None:
            continue
        else:
            part_number = part_number[0]
        participants.append(int(part_number))

    return sorted(participants)


def _find_filename(data_folder):
    """Goes to --data_folder-- and finds which type of filenames are to be found
    there.

    This function was created to deal with the switch in file naming convention
    in different versions of the path. If it finds both data_###.pi and
    data_behavioural_###.pi, it will prefer the behavioural one.

    Returns
    -------
    filename : str
    Filename structure to be used for find_participants().

    """
    all_files = glob.glob(data_folder + '/*.pi')

    flag_beha = 0
    flag_oldie = 0
    for one_file in all_files:
        filename = ntpath.basename(one_file)
        beha = re.search('behavioural', filename)
        oldie = re.search('data_[0-9]+.pi', filename)
        if beha:
            flag_beha += 1
        if oldie:
            flag_oldie += 1
    if flag_beha:
        return '/data_behavioural_{}.pi'
    if oldie:
        return '/data_{}.pi'
    raise FileNotFoundError('No data files with names data_###.pi or '
                            'data_behavioural_###.pi were found in '
                            '{}'.format(data_folder))


def import_data(participants=None, data_folder=None, force=False,
                no_match=False, no_timeouts=True, align_onsets=False,
                filename=None):
    """Imports the data from the experiment and returns it as a pandas
    dataframe.

    Adds 'trials' and 'mblocks' columns, as well as 'risky_points', which
    is the maximum of 'points_blue' and 'points_red' for every trial.

    Parameters
    ----------
    subjects_numbers : array-like
    List of subject numbers for whom the data should be imported. If a
    single subject's data is to be imported, it should still be given
    as an array (e.g. (1, ))

    data_folder : str
    Folder where the experimental data is found. If none (or None)
    provided, the module's default DATA_FOLDER is used.

    force : bool
    If True, missing datafiles are ignored and the data for the ones
    present is returned.

    no_match : bool
    Whether to remove trials in which participants can simply match their
    points.

    no_timeouts : bool
    If True, timeout trials (choice == -1) are removed from data. Defaults
    to True because otherwise inference FREAKS OUT.

    align_onsets : bool 
    In the new version of the experiment (06.2021), decision times can be
    aligned to the onsets of the fmri sequences in the data, if
    --align_onsets-- is True. If the onset information is not contained in the
    data, this is ignored. Note that there are two fmri sequences, so there are
    two "t0" in the data. To differentiate them, one can look at the condition
    (as decisions belonging to the first sequence are those of the condition ==
    0.0).

    filename : str
    Structure of the filenames for the data. Must include a single {}, to
    which the participant number will be appended. If None, an attempt
    will be made to find either data_behavioural_{}.pi or data_{}.pi;
    if both present, the behavioural will be prefered. Note that this is
    not on a per-participant level, so if not all participants have the
    same naming structure, the files will not be found.

    Returns
    -------
    data : pandas DataFrame
    Pandas DataFrame in which all the requested subjects are included
    in one dataframe. The columns are as follows:
      belly_color : float
      Color of the points eaten by the monster. Is NaN when nothing was eaten.

      choice : bool (ish)
      0 for unmatched points, 1 for matched.

      condition : float
      Experimental condition of the trial, described in terms of the probability
      that the next miniblock cue is a filthy lie.

      mblock : int
      Miniblock number (as indexed from the beginning of the experiment phase).

      mblock_next : string
      Describes the next miniblock type, e.g. 'risky_red'

      mblock_type : string
      Describes the miniblock type, e.g. 'risky_red'.

      monster_appearance : bool (ish)
      1 for a trial in which the monster appeard. BROKEN.

      monster_belly : float
      How many points the monster ate. 0 means the monster didn't show up,
      that lazy bastard. Note that this is after the choice was made.

      offered_points_red/blue : float
      How many red/blue unmatched points were offered.

      points_red/blue : float
      How many red/blue points the participant had. Note that matched points
      are also included.

      risky_color : string
      Color of the unmatched points. NaN if no unmatched points.

      risky_points : float
      How many unmatched points the participant had at the beginning of the trial.

      rts : float
      Response time, measured from the moment the offer is drawn to the screen to
      the moment when they make their choice. Note that some data files from
      earlier runs do not have this, and so it is set to NaN for consistency.

      stars : float
      How many starts had been earned so far.

      trans_color_match : bool
      Whether the colors of the current and next mblocks match.

      trans_label : str
      Label of the current transition type, as defined in label_transitions().

      trans_type : int
      Type of miniblock transition, in terms of whether the color and/or the
      risk match: 0 - nothing matches. 1 - only color matches. 2 - only risk
      matches.  3 - both match.

      trial : int
      Trial number.

    """
    if data_folder is None:
        data_folder = DATA_FOLDER
    if participants is None:
        participants = find_participants(data_folder, filename)
    try:
        participants[0]
    except TypeError:
        participants = [participants]
    data = []
    good_subjects = []
    if filename is None:
        filename = _find_filename(data_folder)
    filename = data_folder + filename
    for ix_subject in participants:
        c_filename = filename.format(ix_subject)
        try:
            with open(c_filename, 'rb') as mafi:
                data_three = pickle.load(mafi)
        except FileNotFoundError:
            if not force:
                raise
            print("File {} not found.".format(c_filename))
            continue

        # This is a fix for the different versions of the experiment that save
        # the data differently. Earlier ones save the training data in a
        # separate file and the current ones save it alongside the experiment
        # data:
        if 'training' in data_three:
            data_three = data_three['experiment']

        if 'experiment' in data_three:
            data_three = data_three['experiment']

        # Catch for the newest version of the experiment (06.2021). It will extract
        # the first-slice time.
        if 'onsets' in data_three:
            onsets = data_three.pop('onsets')
        else:
            align_onsets = False

        for ix_cond, (condition, data_one) in enumerate(data_three.items()):
            varnames = data_one.keys()
            if ix_cond != -10000:
                good_subjects.append((ix_subject, condition))
            c_dict = {varname: data_one[varname].reshape(-1, *data_one[varname].shape[2:])
                      for varname in varnames}

            num_mblocks, num_trials = data_one['points_blue'].shape
            c_dict['mblock_type'] = c_dict.pop('miniblock_type')
            c_dict['mblock_next'] = np.roll(c_dict['mblock_type'], -1)
            c_dict['mblock_next'] = np.tile(
                c_dict['mblock_next'][:, None], (1, num_trials)).reshape(-1)
            # mblock_type needs to be made bigger
            c_dict['mblock_type'] = np.tile(
                c_dict['mblock_type'][:, None], (1, num_trials)).reshape(-1)
            _mblock_transitions(c_dict)
            # monster_belly has to be separated
            c_dict['belly_color'] = c_dict['monster_belly'].argmax(axis=1)
            c_dict['monster_belly'] = c_dict['monster_belly'].max(axis=1)

            # This part is changed once converted to pandas below
            c_dict['belly_color'][c_dict['monster_belly'] == 0] = -100
            # Create stuff:
            c_dict['trial'] = np.tile(np.arange(num_trials),
                                      (num_mblocks, 1)).reshape(-1)
            c_dict['mblock'] = np.tile(np.arange(num_mblocks),
                                       (num_trials, 1)).reshape(-1, order='f')
            c_dict['risky_points'] = np.squeeze(
                np.abs(np.diff(np.array([c_dict['points_blue'],
                                         c_dict['points_red']]), axis=0)))
            c_dict['risky_color'] = np.array([c_dict['points_blue'],
                                              c_dict['points_red']]).argmax(axis=0)
            c_dict['risky_color'][c_dict['points_blue']
                                  == c_dict['points_red']] = -100
            c_dict['risky_points'][c_dict['points_blue']
                                   == c_dict['points_red']] = 0
            if align_onsets:
                reonsetify(c_dict, onsets, condition)
            data.append(pd.DataFrame.from_dict(c_dict))

    if len(data) > 0:
        data = pd.concat(data, keys=good_subjects,
                         names=['participant', 'condition', 'nothing'])
        data.reset_index('nothing', drop=True, inplace=True)
        data.reset_index('condition', inplace=True)
        data.replace(to_replace=-100, value=np.nan, inplace=True)
        data.loc[data['belly_color'] == 0, ('belly_color')] = 'blue'
        data.loc[data['belly_color'] == 1, ('belly_color')] = 'red'
        data.loc[data['risky_color'] == 0, ('risky_color')] = 'blue'
        data.loc[data['risky_color'] == 1, ('risky_color')] = 'red'
    else:
        raise ValueError(
            'None of the data files was found. Try again, sucker!')
    if no_match:
        data = remove_matched(data)
    if no_timeouts:
        data = remove_timeouts(data)
    return data


def reonsetify(c_dict, onsets, condition):
    """Re-aligns the onsets of the decisions to the fmri triggers.

    Modifies --c_dict-- in place, so nothing is returned.
    """
    string = 'first' if condition == 0 else 'second'
    onset_epoch = onsets[string + '_sequence_onset_epoch']
    onset = onsets[string + '_sequence_onset']
    onset_seconds = time_to_seconds(onset)
    decision_onsets = c_dict['decision_onset']
    decision_seconds = np.array(list(map(time_to_seconds,
                                         decision_onsets)))
    aligned_decisions = decision_seconds - onset_seconds
    aligned_decisions = np.array(list(map(seconds_to_time,
                                          aligned_decisions)))

    c_dict['decision_onset'] = aligned_decisions
    c_dict['decision_onset_epoch'] -= onset_epoch


def time_to_seconds(onset):
    """Transforms a time in the hhmmss format to number of seconds (since the
    start of that day)."""
    stringed = str(onset)
    return float(stringed[:2]) * 3600 + float(stringed[2:4]) * 60 + \
        float(stringed[4:])


def seconds_to_time(seconds):
    """Transforms a number of seconds to the hhmmss format. Raises a
    ValueError if the number of seconds exceeds the number of seconds in a day.
    """
    max_seconds = 24 * 60 * 60
    if seconds > max_seconds:
        raise ValueError('Too many seconds for one day')
    hours = np.floor(seconds / 3600).astype(int)
    seconds = seconds - 3600 * hours
    minutes = np.floor(seconds / 60).astype(int)
    seconds = int(seconds - minutes * 60)
    return ''.join(['{:0>2}'.format(hours), '{:0>2}'.format(minutes),
                    '{:0>2}'.format(seconds)])


def import_shade(participant):
    '''Gets the timings, because there is nothing else, out of that little
    passive viewing bit of the fMRI part of the experiment'''

    with open(DATA_FOLDER+'/data_fmri_{}.pi'.format(participant), 'rb') as mafi:
        raw_data = pickle.load(mafi)

    shade_data = raw_data['shade']

    new_onset_time = []
    new_fix_time = []

    for value in shade_data['stim_onset']:
        str_value = str(value)
        hrs_str = int(str_value[:2])*3600
        min_str = int(str_value[2:4])*60
        sec_str = int(str_value[4:6])
        new_time = hrs_str+min_str+sec_str

        new_onset_time.append(new_time)

    for value in shade_data['fix_onset']:
        str_value = str(value)
        hrs_str = int(str_value[:2])*3600
        min_str = int(str_value[2:4])*60
        sec_str = int(str_value[4:6])
        new_time = hrs_str+min_str+sec_str

        new_fix_time.append(new_time)

    shade_timings = [new_onset_time, new_fix_time]

   # shade_pd = pd.DataFrame(raw_data['shade'],dtype='str')

    return shade_timings


def convert_to_secs(unconverted_time):
    '''small function that converts HHMMSS into seconds
    Inputs : HHMMSS time in seconds (int)
    Outputs : time in seconds '''

    new_time = []

    str_value = str(unconverted_time)
    hrs_str = int(str_value[:2])*3600
    min_str = int(str_value[2:4])*60
    sec_str = int(str_value[4:6])
    new_time = hrs_str+min_str+sec_str

    return new_time


def _mblock_transitions(dick):
    """For each miniblock, determines whether the next one is of the same
    color or not and of the same risk or not. Saves the results as a
    boolean in --dick-- with keys trans_color_match and
    trans_risk_match, with 1 if they match, 0 otherwise.

    Additionally, 4 transition types are defined, depending on whether
    colors and risk match. They are: 0 - nothing matches.
    1 - only color matches. 2 - only risk matches. 3 - all matches.

    """
    curr = 'mblock_type'
    nexr = 'mblock_next'
    trans_color = []
    trans_label = []
    trans_type = []
    counting = np.arange(3)
    trans_labels = label_transitions()
    for cc, nc in zip(dick[curr], dick[nexr]):
        tc = cc.endswith('red') == nc.endswith('red')
        trans_color.append(tc)
        counting[2] = not tc
        counting[1] = not cc.startswith('risky')
        counting[0] = not nc.startswith('risky')
        trans_type.append((counting.dot(np.power(2, np.arange(3)))))
        trans_label.append(
            trans_labels[counting.dot(np.power(2, np.arange(3)))])
    dick['trans_color_match'] = trans_color
    dick['trans_type'] = trans_type
    dick['trans_label'] = trans_label


def remove_matched(data):
    """Removes the trials in which the risky points' color does not
    match the color of the current miniblock, as participants presumably
    just chose to match their points in these trials.

    Returns a new dataframe.
    """
    mblock_type = data['mblock_type'].values
    risky_color = data['risky_color'].values
    condition = - np.ones(len(data))
    for idx in range(len(data)):
        if isinstance(risky_color[idx], str):
            test = mblock_type[idx].endswith(risky_color[idx])
        else:
            test = 1
        condition[idx] = test
    return data.iloc[condition.astype(bool)]


def remove_timeouts(data):
    """Removes trials in which the participant timed out, which are signaled
    by -1 in the --choice-- column.

    Returns a new dataframe

    """
    return data.query('choice != -1')


def label_transitions():
    """Returns a list with the labels of the transitions. The index of the
    list represents the trans_type in the pandified data.

    """
    return ['Same color, risky-risky', 'Same color, risky-safe',
            'Same color, safe-risky', 'Same color, safe-safe',
            'Diff. colors, risky-risky', 'Diff. colors, risky-safe',
            'Diff. colors, safe-risky', 'Diff. colors, safe-safe']


def import_all(data_sets=('behavioural', 'prefmri', 'fmri'), **kwargs):
    """Imports all the data for the requested participants. Behavioural,
    prefmri and fmri data is all saved together in one big panda.

    For more info on the panda, see import_data().

    Parameters
    ---------
    data_sets : array-like of strings
    Which experiment phases to import. Should be an array with elements
    in {'behavioural', 'prefmri', 'fmri'}. Defaults to importing all.

    **kwargs are sent to import_data().

    Returns
    -------
    data : DataFrame
    DataFrame with all the columns and indices as import_data() with
    the added column called --phase--.

    """
    if 'filename' in kwargs:
        malogger.warning('The --filename-- passed to '
                         'import_data.import_all() will be ignored')
        kwargs.pop('filename')
    data = []
    for data_type in data_sets:
        filename = '/data_{}_{}.pi'.format(data_type, '{}')
        data.append(import_data(**kwargs, filename=filename))
    data = pd.concat(data, keys=data_sets, names=['phase'])
    data.reset_index('phase', inplace=True)
    return data
