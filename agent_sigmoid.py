# -*- coding: utf-8 -*-
# ../agent_sigmoid.py


"""Implements the soft-threshold agent and lets it play. This is a
more optimized version of that implemented in agent_plays_game;
optimization is needed to train the agent to maximize performance. It
should behave identically to the one in agent_plays_game.

"""
import itertools as it
import pickle

import numpy as np
import pandas as pd
from theano.compile.ops import as_op
import theano
import pymc3 as pm

def the_game(agent_pars=None, pandify=True, random_seed=None):
    """Main function to run the entire game, with many miniblocks, balloon
    animals and cocaine.

    """
    if agent_pars is None:
        agent_pars = {'slopes': [-0.02],
                      'centers': [15]}
    np.random.seed(random_seed)

    pars = get_game_parameters()

    initial_points = np.zeros(2)

    log_decision = np.zeros((pars['num_mblocks'], pars['num_trials']))
    log_matched = np.zeros((pars['num_mblocks'], pars['num_trials']))
    log_monster = np.zeros((pars['num_mblocks'], pars['num_trials']))
    log_unmatched = np.zeros((pars['num_mblocks'], pars['num_trials']))
    log_uncolored = np.zeros(
        (pars['num_mblocks'], pars['num_trials']), dtype=object)
    log_context = - np.ones(pars['num_mblocks'])

    make_decision = _make_decision_soft_threshold(agent_pars)
    c_points = initial_points
    for mblock in range(pars['num_mblocks']):
        c_type, n_type = get_mblock_type(mblock, pars)
        log_context[mblock] = c_type
        for trial in range(pars['num_trials']):
            unmatched_color = ['red', 'blue'][np.argmax(c_points) % 2]
            log_unmatched[mblock, trial] = c_points.ptp()
            log_uncolored[mblock, trial] = unmatched_color
            log_matched[mblock, trial] = c_points.min()
            decision = make_decision(c_type=c_type,
                                     n_type=n_type,
                                     c_points=c_points)
            log_decision[mblock, trial] = decision
            monster = sample_monster(decision, c_type, pars)
            log_monster[mblock, trial] = monster
            if decision:
                c_points += 1
            elif not decision and monster:
                c_points = c_points.min() * np.ones(2)
            elif not decision and not monster:
                c_color = c_type % 2
                c_points[c_color] += pars['risky_offer']

    all_logs = {'decisions': log_decision,
                'final_m_pts': c_points.sum(),
                'matched': log_matched,
                'monster': log_monster,
                'unmatched': log_unmatched,
                'unmatched_color': log_uncolored,
                'mblock_type': log_context
                }
    if pandify:
        all_logs = pandify_results(all_logs, pars)
    return all_logs, pars


def pandify_results(results, pars):
    """Puts the results of the_game in a nice panda dataframe.

    """
    for key in ['decisions', 'matched', 'monster',
                'unmatched', 'unmatched_color']:
        results[key] = results[key].reshape(-1)
    results['mblock_type'] = np.tile(results['mblock_type'],
                                     pars['num_trials']).reshape(
                                         (-1, pars['num_mblocks'])).reshape(
                                             -1, order='f')

    pandified = pd.DataFrame(results)
    pandified.loc[pandified['unmatched'] == 0, ('unmatched_color')] = np.nan
    return pandified


def get_game_parameters():
    """Function where all game parameters are stored. In a dictionary, for
    example."""
    with open('./Paradigm/contexts.pi', 'rb') as dumpy:
        contexts = pickle.load(dumpy)

    all_parameters = {'monster_prob_safe': np.array([.35, .65]),
                      'monster_prob_risky': np.array([.65, .35]),
                      'contexts': contexts,
                      'num_mblocks': 50,  # len(contexts),
                      'safe_offer': np.array([1, 1]),
                      'risky_offer': 9,
                      'num_trials': 6,
                      'num_colors': 2,
                      'num_actions': 2}

    return all_parameters


def get_mblock_type(mblock, pars):
    """Function that knows which mini-block type to return for every
    --mblock--, where --mblock-- can be assumed to be an index for
    simplicity. This doesn't actually need to be a function, but could
    be simply a vector loaded from a file.
    """
    contexts = pars['contexts']
    curr_type = contexts[mblock]

    if mblock+1 > len(contexts)-1:
        next_type = contexts[0]
    else:
        next_type = contexts[mblock+1]

    return curr_type, next_type


def sample_offer(curr_type, all_risky_offers):
    """Function that takes some contextual information (like curr_type)
    and returns a risky offer. Preferably in the form (red, blue), as
    we have been doing so far.
    """

    # all_risky_offers = pars['all_risky_offers']

    if curr_type % 2 == 0:
        risky_offer = np.array((np.random.choice(all_risky_offers), 0))
    else:
        risky_offer = np.array((0, np.random.choice(all_risky_offers)))

    return risky_offer


def sample_monster(decision, curr_type, pars):
    """Samples Monster"""

    if decision == 1:
        monster = 0

    elif decision == 0 and curr_type <= 1:
        monster = np.random.choice((0, 1), p=pars['monster_prob_risky'])

    elif decision == 0 and curr_type >= 2:
        monster = np.random.choice((0, 1), p=pars['monster_prob_safe'])

    return monster


def _make_decision_soft_threshold(agent_pars):
    """Returns a function make_decision_soft_threshold() which can be used
    in the_game() as any other agent. This is done because this agent
    needs parameters, and by golly I'm gonna give it to him!

    The size of --centers-- and --slopes-- determines the type of agent. If
    each has one element, it is assumed that the type of the next miniblock
    is ignored and a single sigmoid is used for all trials. If the inputs
    have 8 elements, the adaptive agent is built, which has a different
    sigmoid depending on the transition type.

    The sigmoid, with positive slope, determines the probability of choosing
    the matched offer as a function of the number of unmatched points already
    earned.

    Parameters
    ----------
    centers : array-like
    Determines the center of the (inverted) sigmoid used to make decisions.
    Should be in units of unmatched points and its value determines the
    number of earned points for which the chance of accepting the unmatched
    offer is 0.5. Its size must be either 1 or 8, anything else kills kittens.

    slopes : array-like
    Determines the slope at the center of the sigmoids. The size must match
    that of --centers-- or volcanos explode in white-people countries, which
    is a tragedy.

    Returns
    -------
    make_decision_soft_threshold : function
    Function with inputs (blablabla) which can be used as a make_decision()
    function in the_game().

    """
    centers, slopes = agent_pars
    # if len(centers) != len(slopes):
    #     raise ValueError('--centers-- and --slopes-- should have the same '
    #                      'number of elements, you dickwad')
    centers = np.array(centers)
    slopes = np.array(slopes)
    if centers.size == 1:
        centers = np.tile(centers, 8)
        slopes = np.tile(slopes, 8)
    # All transition types, as named in import_data.label_transitions().
    trannies = np.array([[0, 4, 1, 5],
                         [4, 0, 5, 1],
                         [2, 6, 3, 7],
                         [6, 2, 7, 3]])

    def make_decision(c_type, n_type, c_points):
        """Makes a single decision given the inputs.
        If the agent can match points, it will. If not, uses sigmoid to
        decide.

        """
        if np.size(c_type) == 1:
            c_type = np.array(c_type, ndmin=1)
        if np.size(n_type) == 1:
            n_type = np.array(n_type, ndmin=1)
        if np.size(c_points) == 2:
            c_points = c_points[None, :]
        decisions = np.zeros_like(c_type)
        c_trannie = trannies[c_type, n_type]
        c_center = centers[c_trannie]
        c_slope = slopes[c_trannie]
        c_unmatched = c_points.ptp(axis=1)
        color = np.argmax(c_points, axis=1) % 2
        prob_safe = 1 / (1 + np.exp(c_slope * (c_unmatched - c_center)))
        decisions = np.random.rand(prob_safe.size) < prob_safe
        color_condition = np.logical_and(color != c_type % 2, c_unmatched > 0)
        decisions[color_condition] = 0
        return decisions
    return make_decision

def simulate_dataset(*args, **kwargs):
    """Uses the_game() to generate a bunch of trials, with points, mblock types
    and all the parafernalia, and outputs them in a dictionary. The decisions
    made by the agent that played the game are not included.

    The keys of the dictionary are consistent with the inputs of
    _make_decision_soft_threshold().make_decision() for ease of use.

    args and kwargs are sent to the_game()

    """
    results, pars = the_game(*args, **kwargs)
    output = {}
    output['c_type'] = results['mblock_type'].astype(int)
    output['n_type'] = np.roll(results['mblock_type'],
                               -pars['num_trials']).astype(int)
    c_points = np.zeros((results['mblock_type'].size, 2)).astype(int)
    un_color = results['unmatched_color']
    c_points[np.arange(un_color.size), (un_color == 'red') * 1] =\
        results['unmatched']
    output['c_points'] = c_points

    return output


def performance_map():
    """Calculates the performance of the single-threshold agent for many values
    of the parameters.

    """
    centers = np.arange(50)
    slopes = np.linspace(0.01, 0.5, 10)

    all_pars = it.product(centers, slopes)
    performance = np.zeros(len(centers) * len(slopes))
    out_pars = np.zeros((len(centers) * len(slopes), 2))
    for ix_pars, pars in enumerate(all_pars):
        agent_pars = {'centers': [pars[0]], 'slopes': [pars[1]]}
        performance[ix_pars] = the_game(agent_pars=agent_pars,
                                        pandify=False)[0]['final_m_pts']
        out_pars[ix_pars, :] = pars
    return performance, out_pars


@as_op(itypes=[theano.tensor.dvector],
       otypes=[theano.tensor.dscalar])
def loglikelihood(agent_pars=None):
    """This function transforms the_game's output into a likelihood. Used
    for training the optimal agent.
    """

    results, _ = the_game(agent_pars=agent_pars, pandify=False)
    return np.array(results['final_m_pts'])


def infer_one_perf():
    """Infers the best-performing parameters for the single-threshold agent. Uses
    Metropolis-Hastings to sample from a posterior, with wide Gaussian priors
    over the parameters centered on center=10, slope=0.1.

    """
    mu_slope = 0.1
    std_slope = 1
    mu_center = 30
    std_center = 15
    
    with pm.Model():
        slope = pm.Normal('slope', mu=mu_slope, sd=std_slope)
        center = pm.Normal('center', mu=mu_center, sd=std_center)
        agent_pars = theano.tensor.as_tensor_variable([center, slope])
        pm.DensityDist('likelihood', loglikelihood, observed=agent_pars)

        trace = pm.sample(1000, tune=200, discard_tuned_samples=True, step=pm.Metropolis())
    return trace

def infer_many_data():
    """Infers the best-fitting parameters for the many-sigmoid model from
    experimental or simulated data.

    """
    pass
