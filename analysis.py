# -*- coding: utf-8 -*-
# ... statistics.py

"""Bunch of basic statistics on the data."""
import logging
import pickle

import numpy as np
import scipy.stats as stats
import pandas as pd
import seaborn as sns
from pymc3.parallel_sampling import ParallelSamplingError
import pymc3 as pm
from matplotlib import pyplot as plt
import statsmodels.formula.api as smf

import import_data
import agents_play_game as ass
import inference as infy


malogger = logging.getLogger(__name__)
malogger.handlers = []
malogger.setLevel(logging.INFO)
logger_pymc3 = logging.getLogger('pymc3')
logger_pymc3.handlers = []

file_handler = logging.FileHandler('./logs/inference.log', mode='w')
maformatter = logging.Formatter(fmt='%(asctime)s %(levelname)-8s %(message)s',
                                datefmt='%Y-%m-%d %H:%M:%S')
file_handler.setFormatter(maformatter)
root_logger = logging.getLogger()
root_logger.handlers = []
root_logger.addHandler(file_handler)
root_logger.setLevel(logging.INFO)


def get_prob_risky(participants=None, categories=None, per_part=True):
    """Calculates the probability of choosing the risky for the participants in
    --participants--, divided by the --categories--.

    Parameters
    ----------
    participants : array-like
    Participants for which the data is to be imported.

    categories : list of str
    List that determines the categories into which the probabilities are to
    be separated. Elements must be in {'trial', 'curr_mblock', 'mblock_next',
    'offer', 'points'}. Any element that is not in --categories-- will be
    averaged over. If None provided, all categories will be averaged.

    per_part : bool
    If True, the analysis will be done on a per-participant basis and the
    results will be returned as a dictionary with keys --participants--. If
    False, all participants are pooled together and the results returned
    in a dictionary will key 'all'.

    Returns
    -------
    probs : dict
    Pandas DataFrame with the means grouped by --categories--. The keys
    are --participants-- or 'all', depending on --per_part--.
    """
    data = import_data.import_data(participants)
    if per_part:
        prob_risky = {}
        participants = np.unique(data.index.values)
        for part in participants:
            prob_safe = data.loc[part].groupby(categories).mean()[['choice']]
            prob_risky[part] = 1 - prob_safe
            prob_risky[part].reset_index(inplace=True)
    else:
        prob_safe = data.groupby(categories).mean()[['choice']]
        prob_risky = {'all': 1 - prob_safe}
        prob_risky['all'].reset_index(inplace=True)
    return prob_risky


def come_through_eric(how_many=1000):
    """Eric's performance

    """
    eric_does_it = np.zeros(how_many)

    for ix in range(how_many):
        eric_does_it[ix] = ass.the_game('Eric')['final_m_pts']

    return eric_does_it.mean(), eric_does_it.std()


# def extract_coordinates(Agent):
#     """Does... something?"""
#     joined_files = jf.join_files(Agent)
#     mean_pts, std_pts = jf.calculate_points_mean(joined_files)

#     with open('./Paradigm/contexts.pi', 'rb') as dumpy:
#         contexts = pickle.load(dumpy)

#     trannies = extract_trannies(contexts)


def extract_trannies(contexts):

    trannies = np.zeros_like(contexts)

    for ix_mblock, curr_value in enumerate(contexts):
        if ix_mblock == len(contexts):
            next_mblock = contexts[0]
        else:
            next_mblock = contexts[ix_mblock+1]

        type_of_mblock = np.ravel_multi_index(
            (curr_value, next_mblock), dims=(4, 4))
        trannies[ix_mblock] = type_of_mblock

    return trannies


def infer_parameters(participants=None, data=None, fmri_data=None,
                     kwargs_import=None, kwargs_infer=None):
    """Runs pymc3 inference on the real data for the requested participants.

    Parameters
    ----------
    participants : int, str or an array of those
    Sent to import_data. Can be included in --kwargs_import-- instead.

    data : DataFrame
    Data to use for inference. If provided, --kwargs_import-- is ignored.
    If not, import_data is called with --kwargs_import--.

    fmri_data : DataFrame
    fMRI data that should include columns 'bold' and 'risky_points'.

    print_screen : bool (NOT IMPLEMENTED YET)
    If True, will print the functions' logs to screen. If False, they will
    be sent to the root logger.

    kwargs_import/infer : dict
    Sent to import_data and infy.infer_many, respectively.

    Returns
    -------
    panda_many : DataFrame
    Contains all the samples for all the participants and transitions, and all
    conditions except 0.75. If --data-- contains no conditions except 0.75,
    an empty panda is returned.

    panda_one : DataFrame
    Contains all the samples for all participants and condition 0.75. If
    --data-- doesn't have this condition, an empty panda will be returned.

    """
    global root_logger
    rnd_num = np.random.randint(1000)
    dire = './results_inference/'
    logger_file = dire + './log_inference_{}.log'.format(rnd_num)
    data_file_many = dire + './inferred_parameters_{}.pi'.format(rnd_num)
    data_file_one = dire + './inferred_parameters_one_{}.pi'.format(rnd_num)
    logger_file = logging.FileHandler(logger_file, 'w')
    logger_file.setFormatter(maformatter)
    root_logger.addHandler(logger_file)

    if kwargs_import is None:
        kwargs_import = {}
    if kwargs_infer is None:
        kwargs_infer = {}
    if 'participants' not in kwargs_import:
        kwargs_import['participants'] = participants
    if 'retry' not in kwargs_infer:
        kwargs_infer['retry'] = 10
    if data is None:
        data = import_data.import_data(**kwargs_import)

    if participants is None:
        participants = np.unique(data.index)
    panda_many = pd.DataFrame()
    panda_one = pd.DataFrame()
    all_dataframes = []
    one_dataframes = []
    all_indices = []
    one_indices = []
    for part in participants:
        part_dict = {}
        log_message = ('\n\n################################## '
                       'Starting participant {}'
                       ' ##################################\n\n'.format(part))
        malogger.info(log_message)
        flag_infer_success = 0
        try:
            querying = 'participant == @part and condition != 0.75'
            datum_many = data.query(querying)
            if fmri_data is not None:
                fmri_datum_many = fmri_data.query(querying)
                kwargs_infer['fmri_data'] = fmri_datum_many
            traces_many, names = infy.infer_many(data=datum_many,
                                                 **kwargs_infer)
            querying = 'participant == @part and condition == 0.75'
            datum_one = data.query(querying)
            if fmri_data is not None:
                fmri_datum_one = fmri_data.query(querying)
                kwargs_infer['fmri_data'] = fmri_datum_one
            if len(datum_one) > 0:
                traces_one = infy.infer_one(data=datum_one,
                                            **kwargs_infer)
            flag_infer_success = 1
        except ParallelSamplingError:
            malogger.warning('Inference crashed')
            flag_infer_success = 0

        # If inference failed, moves to next participant:
        if not flag_infer_success:
            continue
        if len(datum_one) > 0 and len(traces_one) != 0:
            part_dict_one = {varname: traces_one.get_values(varname)
                             for varname in traces_one.varnames}
            one_dataframes.append(pd.DataFrame(part_dict_one))
            one_indices.append(part)

        for ix_trans, trace_trans in enumerate(traces_many):
            for ix_cond, c_trace in enumerate(trace_trans):
                condition = names[ix_trans, ix_cond, 1]
                trans_type = names[ix_trans, ix_cond, 0]
                if len(c_trace) == 0:
                    continue
                for varname in c_trace.varnames:
                    part_dict[varname] = c_trace.get_values(varname)
                index_array = (part, condition, trans_type)
                all_dataframes.append(pd.DataFrame(part_dict))
                all_indices.append(index_array)
    panda_one = pd.concat(one_dataframes, keys=one_indices,
                          names=['participant'])
    panda_many = pd.concat(all_dataframes, keys=all_indices,
                           names=['participant', 'condition',
                                  'trans_type'])
    panda_many.to_pickle(data_file_many)
    panda_one.to_pickle(data_file_one)
    root_logger.handlers[-1].flush()
    root_logger.handlers = root_logger.handlers[:-1]
    return panda_many, panda_one


def infer_parameters_behavioral_bold():
    """Runs infer_parameters() with some faked bold data, for testing
    purposes.
    """
    fmri_data = import_data.import_all(data_sets=('fmri',))
    fmri_data = fake_bold(fmri_data)
    beha_data = import_data.import_data()
    beha_data = beha_data.query('trans_type in [-1, 1, 4, 5, 6, 7]')
    infer_many, infer_one = infer_parameters(participants=[3010],
                                             data=beha_data,
                                             fmri_data=fmri_data)
    return infer_many, infer_one


def pandify_all_traces(panda_many, panda_one, permasave=False):
    """Puts all traces in a single panda.

    Inputs are those from infer_parameters().

    Returns the joint panda, which has keys (participant, condition, transition),
    where for condition==0.75, the transition is set to -1.

    If --permasave-- is True, the results are saved to all_traces.pi,
    which can be used to create all_inferred_parameters.pi, so thread carefully.
    """
    panda_many.index.rename('traces', level=-1, inplace=True)
    panda_one.index.rename('traces', level=-1, inplace=True)
    panda_one['condition'] = 0.75
    panda_one.set_index('condition', append=1, inplace=True)
    panda_one['trans_type'] = -1
    panda_one.set_index('trans_type', append=1, inplace=True)
    panda_one = panda_one.reorder_levels([0, 2, 3, 1])

    panda_all = pd.concat([panda_many, panda_one])
    if permasave:
        panda_all.to_pickle('all_samples.pi')
    return panda_all


def easy_inference():
    """Runs inference for all participants and gets all samples together
    into one big happy panda. This panda is returned.

    """
    infer_results = infer_parameters()
    panda_all = pandify_all_traces(*infer_results, permasave=True)
    return panda_all


def create_all_inferred_parameters(participants=None, append=True,
                                   overwrite=True, write_file=True):
    """Creates the file all_inferred_parameters.pi, from which all
    other analyses are performed.

    If --append-- is True, the function
    checks which participants are already in the existing
    all_inferred_parameters.pi file and ignores them, unless
    --overwrite-- is also True.

    NOTE:
    This is what you want to run if you get new participants. The
    default parameters are good if you want to re-do the inference
    for all participants and write to file, so you can do the rest
    of the analyses in peace, though it might take a while. To
    just add new participants, leave participants=None, but
    change overwrite to False.

    Parameters
    ----------
    participants : array-like
    List of participants on which to run inference.

    append : bool
    If True, the existing all_inferred_parameters.pi file is loaded
    and the results of the new inference are appended to it.

    overwrite : bool
    If True, the results for a participant overwrite the existing one
    in all_inferred_parameters.pi. If False, inference is not
    done on the participant at all, because fuck that guy, seriously.

    write_file : bool
    Whether to write the results (with or without the previous
    results, depending on --append--) back to all_inferred_parameters.pi.

    """
    if append:
        with open('./all_inferred_parameters.pi', 'rb') as mafi:
            existing = pickle.load(mafi)
        if not overwrite:
            parts_done = set(existing.index.levels[0])
            parts_new = set(participants)
            participants = np.array(parts_new - parts_done)
            ignored = np.array(parts_new - set(participants))
            root_logger.info('The following participants were not '
                             'included in inference because they were '
                             'already in all_inferred_parameters.pi:\n\n '
                             '{}'.format(ignored))
    if participants is None:
        participants = import_data.find_participants()
    panda_many, panda_one = infer_parameters(participants)
    panda_all = pandify_all_traces(panda_many, panda_one)
    inferred = panda_all.groupby(['participant', 'condition', 'trans_type']).mean()
    if append:
        new_inferred = inferred.combine_first(existing)
    else:
        new_inferred = inferred
    if write_file:
        with open('./all_inferred_parameters.pi', 'wb') as mafi:
            pickle.dump(new_inferred, mafi)
    return new_inferred


def criterion_matching(cutoff=0.2, **kwargs):
    """Calculates how often the participants chose the safe option when
    they could have matched. Divides the results by the risk of the current
    miniblock.


    Parameters
    ----------
    cutoff : float
    Maximum value of average choice for matching in safe miniblocks.

    **kwargs are sent to import_data.import_data.

    Returns
    -------
    matching : DataFrame
    Panda with the average safe when matching.

    """
    data = import_data.import_data(**kwargs)

    data['risk'] = [row.split('_')[0] for row in data['mblock_type']]
    mblock_type = data['mblock_type'].values
    risky_color = data['risky_color'].values

    condition = [not c_type.endswith(c_color)
                 if isinstance(c_color, str) else False
                 for c_type, c_color in zip(mblock_type, risky_color)]
    data = data.loc[condition]
    data = data.query('risk == "safe" and condition == 0')
    groupped = data.groupby(['participant'])
    matching = groupped.mean()['choice']
    return matching


def get_corr_rts_inf(df_emp, df_fitted):
    """Calculates the correlation between reaction times and choice conflict, as
    calculated with the inferred sigmoids, on a per-participant basis.

    """
    df_emp = df_emp.reset_index()
    # calculate means
    df_means = df_fitted.groupby(['participant',
                                  'condition',
                                  'trans_type']).mean()

    cc_vec = np.zeros(len(df_emp))  # preallocation
    for i in np.arange(len(df_emp)):
        p, con, tt, unm_p = df_emp.loc[i, ['participant', 'condition',
                                           'trans_type', 'risky_points']]
        center = df_means.query(
            'participant==@p & condition==@con & trans_type==@tt')['center']
        if len(center) == 0:
            center = np.nan
        slope = df_means.query(
            'participant==@p & condition==@con & trans_type==@tt')['slope']
        if len(slope) == 0:
            slope = np.nan
        cc_vec[i] = get_choice_conflict(center, slope, unm_p)

    # add choice conflict vector to the data frame
    df_emp['choice_conflict'] = cc_vec
    df_emp = df_emp.dropna()

    # calculate correlation
    rs = np.zeros(len(df_means.index.levels[0]))  # preallocation

    # loop over participants
    for i, p in enumerate(df_means.index.levels[0]):
        data = df_emp.query('participant==@p')
        rs[i] = np.corrcoef(data['choice_conflict'], data['rts'])[0, 1]

    return rs


def get_corr_rts_freq(data):
    """Calculates the correlations between the reaction times and the
    frequency of safe choices for each trial.

    """
    data = append_conflict_panda()
    freqs = data.groupby(['participant', 'trans_type',
                          'condition', 'risky_points'])
    freqs = pd.DataFrame(freqs.mean()['choice'])
    freqs_trial = []
    for participant, trial in data.iterrows():
        freqs_trial.append(freqs.loc[(participant, trial['trans_type'],
                                      trial['condition'],
                                      trial['risky_points'])])
    conflict = np.array(freqs_trial).squeeze()
    data['conflict'] = conflict

    coeff = []
    for participant in np.unique(data.index):
        coeff.append(data.query('participant == @participant').corr()['rts']['conflict'])

    return coeff

def plot_corr_rts_cc():
    
    rts_data = append_conflict_panda()
    
    rts_trans_grouped = rts_data.groupby('trans_type').mean()
    
    plt.figure(666, clear=True)
    list_of_participants = np.unique(rts_data.index)
    
    for idx, participant in enumerate(list_of_participants):
        #plt.figure(num=participant, clear=True)
        plt.subplot(4, 5, idx + 1)
        sns.scatterplot(data=rts_data.query('participant==@participant'), x="conflict", y="rts", hue="trans_type" ,palette='Greys', legend=False)
        for transition in np.unique(rts_data['trans_type']):
            formula = 'conflict ~ rts'
            model = smf.ols(formula=formula, data=rts_data.query('participant==@participant and trans_type==@transition'))
            results = model.fit()
            b, m = results.params
            print(b,m) 
            plt.plot([0,1],[b,m+b])
        
     
    print(results.summary())


def test_significance_groups(data, label, groups):
    """Performs chi-squared test on the data between the two --groups--
    in the --label-- column of --data--.

    That is, the data is split according to the values of the column --label--
    into the groups in --groups--, and chi-squared statistics are returned
    for these groups.

    Parameters
    ----------
    data : DataFrame
    Behavioral data. Output of import_data.import_data().

    label : string
    Name of the column in --data-- whose data will be split into groups.

    groups : array-like
    Array with two or more elements. Each element determines a group and should
    be either one of the values in the column --label-- of --data--, or an
    array of such values.

    """
    groups = [np.array(group, ndmin=1) for group in groups]
    table = np.zeros((2, len(groups)))
    for ix_group, group in enumerate(groups):
        choices = data.query('{} in @group'.format(label))['choice']
        table[:, ix_group] = (choices.sum(), len(choices) - choices.sum())
    return stats.chi2_contingency(table)


def significance_poster_sfb():
    """Returns the significance levels for panels A-C of
    plotting.stefan_sfb_poster().

    """
    data = import_data.import_data()
    p_a = test_significance_groups(data, 'trans_type',
                                   groups=((0, 1, 2, 3), (4, 5, 6, 7)))[1]
    p_b = test_significance_groups(data, 'trans_type',
                                   groups=((2, 3, 6, 7), (0, 1, 4, 5)))[1]
    p_c = test_significance_groups(data, 'condition',
                                   groups=(0, 0.75))[1]

    return p_a, p_b, p_c

def chi2_choice_conflict_conditions():
    
    data = append_conflict_panda()
    cond_data = data.query('condition== 0.75')
    
    groups=((2, 3, 6, 7), (0, 1, 4, 5))
    
    groups = [np.array(group, ndmin=1) for group in groups]
    
    table = np.zeros((2, len(groups)))
    for ix_group, group in enumerate(groups):
        conflict = cond_data.query('{} in @group'.format('trans_type'))['conflict']
        print(conflict.sum())
        table[:, ix_group] = (conflict.sum(), len(conflict) - conflict.sum())
        
        
    return stats.chi2_contingency(table)[1]


def plot_choices_by_risk():
    """Makes a plot for all participants averaged and each conditions 
    (future of no future) of average choice for miniblocks that start with 
    risky, or those that start with safe"""
    
    data = import_data.import_all(data_sets=['behavioural'])
    pal = sns.cubehelix_palette(10, rot=-.25, light=.7)
    
    data_2 = data.groupby(by=['participant','condition','trans_type']).mean()
    
    grouped_data = data_2.reset_index()
    
    risky_trans_cond1  = grouped_data.query('trans_type in [0,1,4,5] and condition==0').mean()['choice']
    safe_trans_cond1 = grouped_data.query('trans_type in [2,3,6,7] and condition==0').mean()['choice']
    
    risky_trans_cond2  = grouped_data.query('trans_type in [0,1,4,5] and condition==0.75').mean()['choice']
    safe_trans_cond2 = grouped_data.query('trans_type in [2,3,6,7] and condition==0.75').mean()['choice']
    
    colours = ['cyan', 'lightblue', 'lightgreen', 'red','tan']
    
    ticks = ['risky, cond1','safe, cond1', 'risky cond2', 'safe cond2']
    plot = plt.bar([1,2,3,4],[safe_trans_cond1,risky_trans_cond1,safe_trans_cond2,risky_trans_cond2], tick_label=ticks, color=colours)
    plt.ylabel('Choices by Risk')
    
    return plot


def plot_choices_by_color():
    """Makes a plot for all participants averaged and each conditions 
    (future of no future) of average choice for miniblocks that start with 
    RED or those that start with BLUE what a great function"""
    
    data = import_data.import_all(data_sets=['behavioural'])
    pal = sns.cubehelix_palette(5, rot=-.25, light=.7)
    
    data_2 = data.groupby(by=['participant','condition','mblock_type']).mean()
    
    grouped_data = data_2.reset_index()
    
    red_trans_cond1  = grouped_data.query('mblock_type in ["risky_red","safe_red"] and condition==0').mean()['choice']
    blue_trans_cond1 = grouped_data.query('mblock_type in ["risky_blue","safe_blue"] and condition==0').mean()['choice']
    
    red_trans_cond2  = grouped_data.query('mblock_type in ["risky_red","safe_red"] and condition==0.75').mean()['choice']
    blue_trans_cond2 = grouped_data.query('mblock_type in ["risky_blue","safe_blue"] and condition==0.75').mean()['choice']
    
    colours = ['lightcoral', 'lightsteelblue', 'indianred','steelblue']
    
    ticks = ['Red, cond1','Blue, cond1', 'Red cond2', 'Blue cond2']
    plot = plt.bar([1,2,3,4],[red_trans_cond1,blue_trans_cond1,red_trans_cond2,blue_trans_cond2], tick_label=ticks, color=colours)
    plt.ylabel('Choices by Color')
    plt.title('Color identity does not affect choice')
    
    return plot

def plot_conflict_by_risk():
    """Plots the conflict for all participants conflict levels averaged
    and each conditions (future or no future) of average choice for miniblocks that 
    start with risky or those that start with safe"""
    
    data = append_conflict_panda()
    
    data_2 = data.groupby(by=['participant','condition','trans_type']).mean()
    
    grouped_data = data_2.reset_index()
    
    risky_trans_cond1 = grouped_data.query('trans_type in [0,1,4,5] and condition ==0').mean()['conflict']
    safe_trans_cond1 = grouped_data.query('trans_type in [2,3,6,7] and condition==0').mean()['conflict']
    
    risky_trans_cond2  = grouped_data.query('trans_type in [0,1,4,5] and condition==0.75').mean()['conflict']

    safe_trans_cond2 = grouped_data.query('trans_type in [2,3,6,7] and condition==0.75').mean()['conflict']

    ticks = ['risky, cond1','safe, cond1', 'risky cond2', 'safe cond2']
    
    plot = plt.bar([1,2,3,4],[safe_trans_cond1,risky_trans_cond1,safe_trans_cond2,risky_trans_cond2],tick_label=ticks)
    plt.ylabel('Choice Conflict')
    
    return plot

def plot_participants_by_condition():
    
    data = append_conflict_panda()
    data.reset_index(inplace=True)
    
    ax1 = plt.subplot(121)
    sns.pointplot(data=data, x="condition", y="choice", hue="participant", ax=ax1, legend=False)
    
    ax2 = plt.subplot(122)
    sns.pointplot(data=data, x="condition", y="conflict", hue="participant", ax=ax2, legend=False)
    
    ax1.get_legend().remove()
    ax2.get_legend().remove()
    
    plt.savefig('participants_by_condition.svg')
    
    
def plot_conflict_levels_all_trans(data=None):
    
    if data is None:
        data = append_conflict_panda()
        data = squish_no_future(data)
    
    pal =  sns.cubehelix_palette(10, rot=-.25, light=.7)

        
    labels = ('s_rr', 's_rs', 's_sr', 's_ss', 'd_rr',
              'd_rs', 'd_sr', 'd_ss', 'hu_r','hu_s')
    
    sns.barplot(data=data, x='trans_type', y='conflict', palette=pal)
    plt.xticks(ticks=np.arange(10),labels=labels)
    
    plt.savefig('conflict_levels_all_trans.svg')
    
def plot_conflict_per_participant(part):
    
    data = append_conflict_panda()
    data_2 = squish_no_future(data)
    
    data_3 = data_2.query('participant == @part')
    
    plot_conflict_levels_all_trans(data_3)
    
    
def squish_no_future(data):
    '''Squishes the data from the High Uncertainty / No Future condition into
    two seperate things, starting with either risky or safe'''
    
    
    cond_cond = data['condition'] == 0.75
    cond_safe = np.zeros(len(cond_cond), dtype=bool)
    cond_risk = np.zeros(len(cond_cond), dtype=bool)
    for ix_trial, mblock_type in enumerate(data['mblock_type']):
        cond_safe[ix_trial] = mblock_type.startswith('safe')
        cond_risk[ix_trial] = mblock_type.startswith('risky')
    data.loc[np.logical_and(cond_cond, cond_safe), 'trans_type'] = 9
    data.loc[np.logical_and(cond_cond, cond_risk), 'trans_type'] = 8  

    return data      
    
def plot_conflict_levels(data=None):
    
    if data is None:
        data = append_conflict_panda()
    
    labels = import_data.label_transitions()
    
    #data_2 = data.groupby(by=['participant','condition','trans_type']).mean()
    
    #grouped_data = data_2.reset_index()
    
    #sns.displot(data, x='conflict', kind='kde', row='condition')
    g = sns.violinplot(data=data, y='conflict', x='trans_type', hue='condition', split=True, labels=labels)
    g.set_xticklabels(labels)
    
    
def plot_fmri_conflict_levels(data=None):
    '''Making the violin plots of conflict level distributions for different
    for fMRI. There is some sort of problem with participants 3026 and 3012 so implementing
    a quick and dirty workaround'''
    
    if data is None:
        data = append_conflict_fmri()

    
    # we dont have as many transition types in the fmri so making some fmri specific labels
    #fmri_label_list = [1,4,5,6,7]
    #labeled_trans = import_data.label_transitions()
    #fmri_labels = []
    #for idx, label in enumerate(fmri_label_list):
    #    fmri_labels[idx] = labeled_trans[label]
    
    
    part_list = np.unique(data.index)
    
    clean_data = pd.DataFrame()
    
    
    clean_data = data.query('conflict >=0 and conflict <=1')
        
    #data_2 = data.groupby(by=['participant','condition','trans_type']).mean()
    
    #grouped_data = data_2.reset_index()
    
    #sns.displot(data, x='conflict', kind='kde', row='condition')
    g = sns.violinplot(data=clean_data, y='conflict', x='trans_type', hue='condition', split=True)
    g.set_xticklabels(['Same Colour \n Risky-Safe', 'Different Colour \n Risky-Risky', 'Different Colour \n Risky-Safe',
                       'Different Colour \n Safe-Risky', 'Different Colour \n Safe-Safe'])
    
    
def plot_conflict_each_participant(data=None):
    ''' Makes the wave plot of conflict level for each participants distribution of 
    conflict levels. DEFAULTS to looking at just the behavioural data, but you 
    can also feed in the fmri data by running append_conflict_fmri'''
    
    if data is None:
        data = append_conflict_panda()
    
    sns.set_theme(style="white", rc={"axes.facecolor": (0, 0, 0, 0)})
    
    part_list = np.unique(data.index)
        
    # Create the data
    g = part_list
    
    # Initialize the FacetGrid object
    pal = sns.cubehelix_palette(10, rot=-.25, light=.7)
    data.reset_index(inplace=True)
    g = sns.FacetGrid(data=data, row='participant', hue='participant', aspect=15, height=.5, palette=pal)
    
    # Draw the densities in a few steps
    g.map(sns.kdeplot, 'conflict',
          bw_adjust=.5, clip_on=False,
          fill=True, alpha=1, linewidth=1.5)
    g.map(sns.kdeplot, 'conflict', clip_on=False, color="w", lw=2, bw_adjust=.5)
    g.map(plt.axhline, y=0, lw=2, clip_on=False)
    
    
    # Define and use a simple function to label the plot in axes coordinates
    def label(x, color, label):
        ax = plt.gca()
        ax.text(0, .2, label, fontweight="bold", color=color,
                ha="left", va="center", transform=ax.transAxes)
    
    
    g.map(label, 'conflict')
    
    # Set the subplots to overlap
    g.fig.subplots_adjust(hspace=-.25)
    
    # Remove axes details that don't play well with overlap
    g.set_titles("")
    g.set(yticks=[])
    g.despine(bottom=True, left=True)
    
def plot_mean_conflict():
    
    data = append_conflict_panda()
    plt.subplot(211)
    sns.lineplot(data=data, y='conflict', x='participant', palette="ch:r=-.2,d=.3_r",)
    
    
    plt.subplot(212)
    sns.lineplot(data=data, y='choice', x='participant',palette="ch:r=-.2,d=.3_r")
    
    

def append_conflict_panda(data=None):
    
    if data is None:
        data = import_data.import_all(data_sets=['behavioural'],no_timeouts=False)
    
    _, conflict_level = calculate_conflict()
    
    conflict_panda = pd.DataFrame()
    
    for key, value in conflict_level.items():
        temporary_panda = pd.DataFrame(data=value,index=[key] * len(value),columns=['conflict'])
        conflict_panda = conflict_panda.append(temporary_panda)

    all_data = pd.concat([data,conflict_panda], axis=1)
    all_data.index.set_names('participant', inplace=True)
    
    return all_data

def append_conflict_fmri(all_data=None):
    
    if all_data is None:
        all_data = import_data.import_all(data_sets=['fmri'],no_timeouts=False)
    # Again, this may not work with the calculate_fmri_conflict function. 
    # In that case, feed it directly in 
    
    
    _, conflict_level = calculate_fmri_conflict(all_data=all_data)
    
    conflict_panda = pd.DataFrame()
    
    for key, value in conflict_level.items():
        temporary_panda = pd.DataFrame(data=value,index=[key] * len(value),columns=['conflict'])
        conflict_panda = conflict_panda.append(temporary_panda)

    all_fmri_data = pd.concat([all_data,conflict_panda], axis=1)
    all_fmri_data.index.set_names('participant', inplace=True)
    
    return all_fmri_data


def calculate_conflict_neo(pandata, slope=None, center=None, inplace=False):
    """Calculates the conflict given the ['choices'] in the --pandata--.

    The resulting conflict is appended to the --pandata-- as a new column
    'conflict'; all is returned as a copy of --pandata--, so the original
    is not modified. This can be disabled by --inplace--.

    IMPORTANT NOTES:
    (1) The inferred parameters are taken from all_inferred_parameters,
        unless given as input.
    (2) Because all_inferred_parameters takes all trans_types in the
        0.75 condition as -1, --pandata-- is adapted to do the same.

    Parameters
    ----------
    pandata : pd.DataFrame
    Behavioral data from the experiment. The necessary elements are
    the index with participant and the columns 'choice', 'condition',
    and 'trans_type'.

    slope, center : float
    Parameters of the sigmoid to use. If any is None, they are both
    taken from all_inferred_parameters.pi.

    """
    if not inplace:
        pandata = pandata.copy()
    pandata.loc[pandata['condition'] == 0.75, 'trans_type'] = -1
    pandata.set_index(['condition', 'trans_type'], inplace=True, append=True)
    pandata['conflict'] = -100
    if slope is None or center is None:
        inferred = pd.read_pickle('./all_inferred_parameters.pi')
        pandata = pandata.merge(inferred, left_index=True, right_index=True)
    else:
        pandata['slope'] = slope
        pandata['center'] = center
    p_of_safe = 1 / (1 + np.exp(pandata['slope'] * (pandata['risky_points'] -
                                                    pandata['center'])))
    conflict = 1 - 2 * np.abs(p_of_safe - 0.5)
    pandata['p_of_safe'] = p_of_safe
    pandata['conflict'] = conflict
    return pandata


def calculate_conflict(filename=None):

    '''Makes the model stuffies into nicely formatted dictionaries that I have to 
    later turn into something matlab can look at for neuroimaging'''
    
    # with open('all_inferred_parameters.pi','rb') as dumpy:
    #     all_inferred_parameters = pickle.load(dumpy)    
    all_inferred_parameters = pd.read_pickle('all_inferred_parameters.pi')
    
    all_data = import_data.import_data(filename=filename, no_timeouts=False)
    all_risky_points = {}
    all_trans_types  = {}
    p_of_safe        = {}
    conflict_level   = {}
    
    participant_list = np.unique(all_data.index)
    
    
    for participant in participant_list:
        current_data = all_data.loc[participant]
        risky_points = current_data['risky_points']
        trans_type   = current_data['trans_type']
        all_risky_points[participant] = np.array(risky_points)
        all_trans_types[participant]  = np.array(trans_type)
    
    means = all_inferred_parameters.groupby(['participant','condition','trans_type']).mean()
    
    for each_participant in participant_list:
        screech          = np.ones(len(all_risky_points[each_participant]))
                                   
        for ix_trial, amt_pts in enumerate(all_risky_points[each_participant]):
            try:
                center,slope = np.array(means.loc[(each_participant,0,all_trans_types[each_participant][ix_trial])])
            except:
                screech[ix_trial] = -100
                continue
            temp_exponent = slope*(amt_pts-center)
            
            screech[ix_trial] = 1 / (1 + np.exp(temp_exponent))
            
        p_of_safe[each_participant] = screech
        conflict_level[each_participant] = 1 - 2*abs(p_of_safe[each_participant]-0.5)
        
    
    return p_of_safe, conflict_level

def calculate_fmri_conflict(all_data=None):

    '''Makes the model stuffies into nicely formatted dictionaries that I have to 
    later turn into something matlab can look at for neuroimaging'''
    
    # with open('all_inferred_parameters.pi','rb') as dumpy:
    #     all_inferred_parameters = pickle.load(dumpy)    
    all_inferred_parameters = pd.read_pickle('all_inferred_parameters.pi')
    
    if all_data is None:
        filename = None
        all_data = import_data.import_data(filename=filename, no_timeouts=False)
      # This may not work, trying a workaround....
      
      
    all_risky_points = {}
    all_trans_types  = {}
    p_of_safe        = {}
    conflict_level   = {}
    all_conditions   = {}
    
    participant_list = np.unique(all_data.index)
    
    
    for participant in participant_list:
        current_data = all_data.loc[participant]
        risky_points = current_data['risky_points']
        trans_type   = current_data['trans_type']
        conditions = current_data['condition']
        all_risky_points[participant] = np.array(risky_points)
        all_trans_types[participant]  = np.array(trans_type)
        all_conditions[participant] = np.array(conditions)
    
    means = all_inferred_parameters.groupby(['participant','condition','trans_type']).mean()
    
    for each_participant in participant_list:
        screech          = np.ones(len(all_risky_points[each_participant]))
                                   
        for ix_trial, amt_pts in enumerate(all_risky_points[each_participant]):
            try:
                condition = all_conditions[each_participant][ix_trial]
                if condition == 0.75:
                    trans_type = -1
                else:
                    trans_type = all_trans_types[each_participant][ix_trial]
                    center,slope = np.array(means.loc[(each_participant, condition, trans_type)])
            except:
                # ipdb.set_trace()
                screech[ix_trial] = -100
                continue
            temp_exponent = slope*(amt_pts-center)
            
            screech[ix_trial] = 1 / (1 + np.exp(temp_exponent))
            
        p_of_safe[each_participant] = screech
        conflict_level[each_participant] = 1 - 2*abs(p_of_safe[each_participant]-0.5)
        
    
    return p_of_safe, conflict_level

def loop_over_parms_conditions(parameter,averaged=True):
    '''
    Inputs: 
        Takes as input either 'slope' or 'center.'
        Also either averaged or not averaged
    Loops over participants over 
    both conditions and plots the average paramaeter in question for each transition
    type, or av of both conditions'''

    plt.figure(666, clear=True)
    
    all_data = import_data.import_all(data_sets=['behavioural'])
    list_of_participants = np.unique(all_data.index)
    
    if parameter=='slope': 
        for idx, participant in enumerate(list_of_participants):
            plt.subplot(4, 5, idx + 1)
            compare_slopes_conditions(participant,averaged)
    else:
        for idx, participant in enumerate(list_of_participants):
            plt.subplot(4, 5, idx + 1)
            compare_centers_conditions(participant,averaged)

def closest_parms_conditions():
    '''Calculates the difference in a given parameter between the nofuture condition
    and each of the transition types seperately, does this individually for a particular participant,
    Goal is to see if there is one transition type that most closely resembles the nofuture condition.'''


    all_pars = pd.read_pickle('all_inferred_parameters.pi')
    all_pars.reset_index(inplace=True)
    
    all_data = import_data.import_all(data_sets=['behavioural'])
    list_of_participants = np.unique(all_data.index)
    
    to_print = all_pars.groupby(['participant', 'trans_type']).mean()
    avg_tts = to_print.query('trans_type != -1').sum()
    avg_nf = to_print.query('trans_type == -1')
    
    for idp, part in enumerate(list_of_participants):
        plt.subplot(5, 4, idp + 1)
        plt.bar([0, 1], [avg_nf.query('participant == @part')['center'], 
                         avg_tts.query('participant == @part')['center']])
        
def plot_parm_differences():
    all_pars = pd.read_pickle('all_inferred_parameters.pi')
    all_pars.reset_index('condition', drop=True, inplace=True)
    
    nofuture = all_pars.query('trans_type == -1').reset_index('trans_type',
                                                              inplace=False,
                                                              drop=True)
    future = all_pars.query('trans_type != -1').reset_index('trans_type',
                                                            inplace=False)
    future['diff_center'] = nofuture['center'] - future['center']
    future.reset_index('participant', inplace=True)
    ax = sns.barplot(data=future, x='trans_type', y='diff_center', hue='participant')
    ax.get_legend().remove()
        
def plot_average_parms():
    '''Plots the average in the center parameter for all participants individually
    comparing both conditions no future and forecast'''
    
    all_pars = pd.read_pickle('all_inferred_parameters.pi')
    all_pars.reset_index(inplace=True)
    
    bins = pd.cut(all_pars['trans_type'], [-np.inf, 0, np.inf], labels=['Low-Uncertainty Condition', 'High-Uncertainty Condition'])
    all_pars['bins'] = bins
    
    sns.barplot(data=all_pars,x='participant', y='slope', hue='bins')
    

def compare_slopes_conditions(participant,averaged=True):
    all_inferred_parameters = pd.read_pickle('all_inferred_parameters.pi')
    all_inferred_parameters.reset_index(inplace=True)
    
    all_inferred_parameters = all_inferred_parameters.query('participant == @participant')
    #all_inferred_parameters.groupby(['participant'])
    
    slope_cond2_av = all_inferred_parameters.query('trans_type ==-1').mean()['slope']
    
    if not averaged:
        slope_tt0_av = all_inferred_parameters.query('trans_type==0').mean()['slope']
        slope_tt1_av = all_inferred_parameters.query('trans_type==1').mean()['slope']
        slope_tt2_av = all_inferred_parameters.query('trans_type==2').mean()['slope']
        slope_tt3_av = all_inferred_parameters.query('trans_type==3').mean()['slope']
        slope_tt4_av = all_inferred_parameters.query('trans_type==4').mean()['slope']
        slope_tt5_av = all_inferred_parameters.query('trans_type==5').mean()['slope']
        slope_tt6_av = all_inferred_parameters.query('trans_type==6').mean()['slope']
        slope_tt7_av = all_inferred_parameters.query('trans_type==7').mean()['slope']
    
        plot = plt.bar([1,2,3,4,5,6,7,8,9],[slope_cond2_av,slope_tt0_av,slope_tt1_av,slope_tt2_av,slope_tt3_av,
                                        slope_tt4_av,slope_tt5_av,slope_tt6_av,slope_tt7_av])
        
    else:
        slope_all_tt_av = all_inferred_parameters.query('trans_type != -1').mean()['slope']
        plot = plt.bar([1,2,],[slope_cond2_av,slope_all_tt_av])
    

def compare_centers_conditions(participant, averaged=True):
    all_inferred_parameters = pd.read_pickle('all_inferred_parameters.pi')
    all_inferred_parameters.reset_index(inplace=True)
    
    
    all_inferred_parameters = all_inferred_parameters.query('participant == @participant')
    #all_inferred_parameters.groupby(['participant'])
    
    center_cond2_av = all_inferred_parameters.query('trans_type ==-1').mean()['center']
    
    if not averaged:
        slope_tt0_av = all_inferred_parameters.query('trans_type==0').mean()['center']
        slope_tt1_av = all_inferred_parameters.query('trans_type==1').mean()['center']
        slope_tt2_av = all_inferred_parameters.query('trans_type==2').mean()['center']
        slope_tt3_av = all_inferred_parameters.query('trans_type==3').mean()['center']
        slope_tt4_av = all_inferred_parameters.query('trans_type==4').mean()['center']
        slope_tt5_av = all_inferred_parameters.query('trans_type==5').mean()['center']
        slope_tt6_av = all_inferred_parameters.query('trans_type==6').mean()['center']
        slope_tt7_av = all_inferred_parameters.query('trans_type==7').mean()['center']
    
        plot = plt.bar([1,2,3,4,5,6,7,8,9],[center_cond2_av,slope_tt0_av,slope_tt1_av,slope_tt2_av,slope_tt3_av,
                                        slope_tt4_av,slope_tt5_av,slope_tt6_av,slope_tt7_av])
        
    else:
        center_all_tt_av = all_inferred_parameters.query('trans_type != -1').mean()['center']
        print(averaged)
        plot = plt.bar([1,2,],[center_cond2_av,center_all_tt_av])
        
def bin_conflict(bins=None, labels=None):
    """Classifies rows into the bins in --bins--. --pandata-- must already
    have the 'conflict' added to it or this will cry.
    Adds the column 'conflict_cat' to the panda and returns a copy of it.
    """
    pandata = append_conflict_panda()
    
    pandata = pandata.copy()
    if bins is None:
        bins = [0, 0.3, 0.7, 1]
    if labels is None:
        labels = ['low', 'mid', 'high']
    if len(labels) != len(bins) - 1:
        raise ValueError('Too many (or too few) labels provided for the'
                         'number of bins.')
    pandata['conflict_cat'] = pd.cut(pandata['conflict'],
                                     bins=bins, labels=labels)
    
    sns.pointplot(data=pandata, x="conflict_cat", y="condition",hue="trans_type", legend=False)
    
    return pandata

def fake_bold(data, beta=(1, 1.2), sigmoid_slope=None, sigmoid_center=None):
    """Fakes a bold signal as y = beta[0] + beta[1] * conflict,
    where conflict should be a column in --data--.

    Parameters
    ----------
    data : DataFrame
    Behavioral data (from some session). Assumed to be in the format of
    import_data().

    beta : array-like, size=(2, )
    Betas for the generation of the fake bold signal.

    sigmoid_slope/center : float
    Parameters of the sigmoid to use. Sent to calculate_conflict_neo.

    Returns
    -------
    conflicted : DataFrame
    A copy of the original data with the extra columns 'conflict',
    'center', 'slope' and 'bold'.

    """
    conflicted = calculate_conflict_neo(data, slope=sigmoid_slope,
                                        center=sigmoid_center,
                                        inplace=False)
    conflicted['bold'] = beta[0] + conflicted['conflict'] * beta[1]
    return conflicted


def test_behavioral_bold_model():
    """Generates data with the model, infers the model with the data.

    The perfect outcome of this test is that the posteriors over all the
    parameters match the default values from
    infy.define_behavioral_bold_model(), which can be seen by running
    pm.plot_trace() on the output of this function.
    
    """
    data = import_data.import_all()
    beha = data.query('participant == 3010 and '
                      'condition == 0 and '
                      'trans_type == 6 and '
                      'phase == "behavioural"')
    fmri = data.query('participant == 3010 and '
                      'condition == 0 and '
                      'trans_type == 6 and '
                      'phase == "fmri"')
    model = infy.define_behavioral_bold_model(beha, fmri, for_data=True)
    bold = pm.sample_prior_predictive(model=model, samples=1)['bold'][0, :]
    fmri['bold'] = bold

    model = infy.define_behavioral_bold_model(beha, fmri)
    samples = pm.sampling.sample(5000, model=model)
    return samples


def categorize_conflict(pandata, bins=(0, 0.5, 1), return_copy=False):
    """Takes the behavioral data in --pandata-- and adds a column
    'conflict_cat' which represents a categorical variable for
    conflict.

    If --pandata-- does not have a column 'conflict', calculate_conflict_neo
    will be called.

    Parameters
    ----------
    pandata : pd.DataFrame
    Behavioural data (from any phase, really).

    bins : array-like
    Iterable with the edges of the bins to use. The first and last
    edges must be included, so size=(N + 1, ), where N is the number
    of desired bins.

    return_copy : bool
    Whether to create a copy of --pandata-- and return it (True), or to work
    in-place (False).

    Returns
    -------
    cat_conflicted_panda : pd.DataFrame
    Only if --return_copy-- is True. Copy of --pandata-- with the added column.

    """
    if return_copy:
        pandata = pandata.copy()
    if 'conflict' not in pandata:
        calculate_conflict_neo(pandata, inplace=True)
    pandata['conflict_cat'] = pd.cut(pandata['conflict'], bins=bins,
                                     labels=np.arange(len(bins) - 1))
    return pandata


def conflict_to_csv(pandata=None):
    """Takes the conflict in --pandata-- and outputs to files separated by
    participant number. The order of the data points is
    determined via 'decision_onset_epoch', the first decision on top.

    If --pandata-- is None, import_data.import_all(data_sets=['fmri'],
    align_onsets=True) is called, then calculate_conflict_neo() is called with
    no arguments. If --pandata-- does not contain 'conflict',
    calculate_conflict_neo() is called with no arguments.

    tl:dr call this function with no inputs to create the csv files for all
    participants for which there is data.

    """
    filenames = './regressors/conflict_{}.pi'
    if pandata is None:
        pandata = import_data.import_all(data_sets=['fmri'],
                                         align_onsets=True)
        flag_inplace=True
    else:
        flag_inplace=False
    participants = np.unique(pandata.index)
    if 'conflict' not in pandata:
        pandata = calculate_conflict_neo(pandata, inplace=flag_inplace)
    of_interest = pandata[['conflict', 'decision_onset_epoch']]
    of_interest.sort_values(by=['participant', 'condition', 'decision_onset_epoch'],
                            inplace=True)
    of_interest.reset_index(['condition', 'trans_type'], inplace=True)
    for participant in participants:
        filename = filenames.format(participant)
        datum = of_interest.loc[participant,
                                ['condition', 'conflict', 'decision_onset_epoch']]
        datum.reset_index('participant', drop=True, inplace=True)
        datum.to_csv(filename)
